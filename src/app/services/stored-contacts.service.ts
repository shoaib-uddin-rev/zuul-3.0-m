import { Injectable } from '@angular/core';
import { AddPage } from '../pages/contacts/groups/add/add.page';
import { AddContactsPage } from '../pages/contacts/contacts/add/add.page';
import { SyncContactsPage } from '../pages/sync-contacts/sync-contacts.page';
import { AlertsService } from './basic/alerts.service';
import { ModalService } from './basic/modal.service';
import { PopoverService } from './basic/popover.service';
import { EventsService } from './events.service';
import { NetworkService } from './network.service';
import { SqliteService } from './sqlite.service';
import { UserService } from './user.service';
import { UtilityService } from './utility.service';
import { NewPassComponent } from '../components/new-pass/new-pass.component';

@Injectable({
  providedIn: 'root'
})
export class StoredContactsService {

  group_contacts: any;
  sync_contacts: any;
  syncoffset: any;
  phone_contacts: any;
  fav_contacts: any;

  is_groupp_loaded: boolean = false;
  constructor(public network: NetworkService,
    public atrCtrl: AlertsService,
    public events: EventsService,
    public sqlite: SqliteService,
    public userService: UserService,
    public utilityProvider: UtilityService,
    public modals: ModalService,
    public popoverCtrl: PopoverService) {
  }


  // ------------------------------- Contacts ------------------------------- //

  getMyContacts(search = null, page = 1, is_favorite = 0): Promise<any[]> {

    return new Promise(async resolve => {

      // get contacts count
      // if zero then fetch from network
      let obj = {
        search,
        page,
        is_favorite,
      };

      // if (is_favorite !== 0) {
      //   obj[is_favorite];
      // }



      console.log(obj);

      this.network.getUserContacts(obj).then(v => {
        console.log(v);
        if (is_favorite !== 0) {
          this.fav_contacts = v.list.filter((x) => x.is_favourite === "1");
        } else {
          this.phone_contacts = v.list;
        }
        resolve(v.list);
      }, err => resolve([]));
    });

  }

  syncMyContact() {

    return new Promise(async resolve => {
      const res = await this.modals.present(SyncContactsPage);
      resolve(res);
    });



    // const res = await this.modals.present(SyncContactsPage);
    // const data = res.data;

    // console.log(data);
    // if (data['data'] != 'A') {
    //   const data2 = await this.modals.present(SyncContactsPage);
    //   this.getMyContacts("", 1);
    // }

  }

  async getSyncNetworkContacts(search = null, is_concat = true, loader = false) {


    return new Promise(resolve => {
      let offset = this.syncoffset;
      if (is_concat == false) { offset = 0 }

      this.sqlite.getSyncContacts(search, offset, loader).then((res: any) => {

        console.log("Response: ", res);

        if (is_concat) {
          this.syncoffset = res['offset'] as number;
          this.sync_contacts = this.sync_contacts.concat(res['contact_list']);
        } else {
          this.syncoffset = res['offset'] as number;
          this.sync_contacts = res['contact_list'];
        }

        resolve(this.sync_contacts);

      }, error => {
        resolve(this.sync_contacts);

      });
    });

  }

  async syncContactsViaNetworkApi(formdata) {

    return new Promise(resolve => {
      this.network.syncContacts(formdata).then(async (response: any) => {
        console.log(response);
        // this.utilityProvider.presentSuccessToast(response['message']);
        await this.sqlite.setContactListInDatabase(response['list'])
        resolve(response['list']);
        // loading.dismiss();
      }, err => {
        console.error(err['list']);
        resolve(err['list']);
      });
    })

  }

  async fetchEventsToSendPassTo(item) {

    const flag = await this.utilityProvider.presentConfirm('Create pass', 'Cancel', 'Create a Pass',
      "Would you like to create a pass and sent it to " + item.contact_name);

    if (flag) {
      const data = await this.modals.present(
        NewPassComponent,
        {
          rap: false,
          group: true,
          item: {
            from_contact: true,
            selected_contacts: [item.id],
            pass_start_date_with_time: this.utilityProvider.formatDateTime(new Date())
          }
        }
      );
    }

  }

  async fetchEventsToSendPassToGroup(group) {

    const flag = await this.utilityProvider.presentConfirm('Create pass', 'Cancel', 'Create a Pass', 'Would you like to create a pass and sent it to all contacts in' + group.group_name);
    if (flag) {

      const data = await this.modals.present(
        NewPassComponent,
        {
          rap: false,
          group: true,
          item: {
            group_id: group.id,
            pass_start_date_with_time: this.utilityProvider.formatDateTime(new Date())
          }
        }
      );
    }

  }

  async SyncContacts(resync = false) {

    return new Promise(async resolve => {
      this.utilityProvider.showLoader();
      this.utilityProvider.getPhoneContacts().then(async contacts => {
        console.log('b');
        console.log(contacts);

        this.sqlite.setSyncContactListInDatabase(contacts, resync).then(v => {
          this.getSyncNetworkContacts(null, false, false);
          this.utilityProvider.hideLoader();
        });
      })

    })

  }

  async checkIfPhoneSyncAlready() {
    return this.sqlite.checkIfPhoneSyncAlready();
  }

  async sendPassRequestToContact(item) {

    const userid = await this.sqlite.getActiveUserId();

    const data = await this.atrCtrl.presentRadioSelections('Request a pass',
      'Enter some details or reason for pass request as the recipient may not know who you are',
      [{
        type: 'textarea',
        name: 'comments',
        placeholder: ''
      }]);

    console.log(data);

    if (data) {
      var f = {};
      // requested_user_id: t.user_id,
      //   description: this.comments
      f['contact_id'] = item['id'];
      // f['sent_user_id'] = userid;
      // f['display_name'] = item['display_name'];
      f['description'] = data.comments;
      // f['phone_number'] = item['phone_number'];

      console.log(f);
      this.network.sendRequestForAPassForContact(f).then((res: any) => {
        console.log(res);
        // if (res['bool'] == false) {
        //   this.utilityProvider.presentFailureToast(res['message']);
        // } else {
        //   this.utilityProvider.presentSuccessToast(res['message']);
        // }
        this.utilityProvider.presentFailureToast('Request Sent');

      }, err => {
        this.utilityProvider.presentFailureToast('Person not allowed to send passes');
      });
    }




  }

  async deleteContact(item) {

    let flag = await this.utilityProvider.presentConfirm('Agree', 'Disagree', 'Delete Contact?', 'Passes sent to this contact will not be effected');
    if (!flag) return;

    console.log(item);
    var elementPos = this.phone_contacts.map(function (x) { return x.id; }).indexOf(item.id);
    this.phone_contacts.splice(elementPos, 1);

    if (parseInt(item.is_favourite) == 1) {
      var elementPos2 = this.fav_contacts.map(function (x) { return x.id; }).indexOf(item.id);
      this.fav_contacts.splice(elementPos2, 1);
    }
    const contactArray = { contactIds: [item.id] }
    console.log(contactArray);

    this.network.bulkDeleteContact(contactArray).then(async res => {
    });
  }

  editContact(c, is_favorite = 0) {

    return new Promise(async resolve => {

      let id = c ? c.id : null;
      let res = { contact_id: id, show_relation: false };

      const data = await this.modals.present(AddContactsPage, res);
      resolve(true);
      // if (data) {
      //   // this.getMyContacts(null, 1);

      // }

    });

  }

  // ----------------------------- Groups --------------------------------- //

  getMyGroups(search = null, page = 1, refresh = false): Promise<any[]> {

    return new Promise(async resolve => {

      const obj = {
        search,
        page
      };
      // call network for groups
      this.network.getUserGroups(obj).then(v => {
        console.log(v);
        resolve(v.list);
      }, err => resolve([]));
    });

  }


  async addContactsToGroup(group_id, list, loader = false) {
    return new Promise(async resolve => {
      const dataObj = {
        group_id: group_id,
        contact_list: list
      }

      console.log("Inside Add Contacts To Group", list)
      console.log("Inside Add Contacts To Group", dataObj)

      this.network.addContactsToGroup(dataObj, loader).then(async (data: any) => {
        let _list = data['list'];
        resolve(_list);
      });

    })

  }

  addContactsToSelectedGroupByItem(group_id, loader = true) {

    return new Promise(async resolve => {
      var self = this;

      const { data } = await this.modals.present(SyncContactsPage, { fromContacts: true });

      console.log("Here is the Groups Conbtact", data);

      if (data['data'] != 'A') {

        const imported_list = data['list'];

        console.log(imported_list);

        let list = imported_list.map(x => {
          return {
            contact_id: x
          }
        })

        self.addContactsToGroup(group_id, list, loader).then(v => {
          resolve(v);
        });

      }
    })
  }

  async createContactGroup() {
    // CreateGroupPage as modal
    let _data = { phone_contacts: this.phone_contacts };

    const data = await this.modals.present(
      AddPage,
      _data
    );

    console.log(" Here is the groups", data);

    // add to sqlite
    if (data['data'] != 'A') {
      let _group = data['group'];
      // await this.sqlite.setGroupListInDatabase([_group]);
      this.getMyGroups(true);
    }

  }

  editContactGroup(item) {

    // CreateGroupPage as modal
    return new Promise(async resolve => {
      console.log(item);
      const params = { phone_contacts: this.phone_contacts, it: item };

      const { data } = await this.modals.present(AddPage, params);

      console.log(data);
      if (data['group']) {
        let _group = data['group'];
        this.getMyGroups(true);
        resolve(data);
      } else {
        resolve(null);
      }

    });

  }

  deleteContactGroup(group) {
    return new Promise<boolean>(async resolve => {

      let ids = { ids: [group.id] };

      console.log(ids);

      this.network.deleteContactGroup(ids).then((res: any) => {
        // this.getMyGroups(true).then(v => {
        resolve(true);
        // });
      });
    })

  }

  async createSelectedInContactGroupByItem(items) {

    return new Promise(async resolve => {

      if (items.length <= 0) { resolve(null); }

      this.getGroupSelection().then(async id => {
        if (!id) { resolve(null); return; }

        resolve(await this.addContactsToGroup(id, items, false));
      });

    })

  }

  getGroupSelection() {

    return new Promise(async resolve => {

      let group_contacts = [];

      if (this.is_groupp_loaded == false) {
        group_contacts = await this.getMyGroups(true)
      }

      if (group_contacts.length == 0) {
        this.utilityProvider.presentFailureToast('No Group Present');
        resolve(null)
        return;
      }

      var radio_inputs = [];
      group_contacts.forEach(item => {
        radio_inputs.push({
          type: 'radio',
          label: item.group_name,
          value: item.id,
          checked: false
        });
      });


      let option = await this.atrCtrl.presentRadioSelections('Select Group', '', radio_inputs);
      resolve(option);


    })
  }

  // ----------------------------- Favorites --------------------------------- //

  async removeFavoritesFromContacts(item) {

    var elementPos = this.fav_contacts.map(function (x) { return x.id; }).indexOf(item.id);
    this.fav_contacts.splice(elementPos, 1);

    let ids = {
      contactIds: [item.id]
    }

    this.network.removeFromFavorites(ids).then(data => {
      console.log(data);
    }, err => { });
  }

  async syncContactsToFavourities() {

    const { data } = await this.modals.present(
      SyncContactsPage,
      { fromContacts: true }
    );

    console.log("Here are synced contacts", data);

    if (data != 'A') {
      const imported_list = data['list'];

      console.log(imported_list);

      await this.addSelectedContactsToFavorite(data["list"], true);
    }

  }


  async addSelectedContactsToFavorite(items, id_only = false) {

    var _items = items;

    if (_items.length <= 0) {
      return;
    }

    let ids = {};
    if (!id_only) {
      ids = {
        contactIds: _items.map(x => x.id)
      };
    } else if (id_only) {
      ids = {
        contactIds: items
      }
    }


    this.network.addToFavorites(ids).then((res) => {
      this.events.publish("contacts:favouritesUpdated");
      this.utilityProvider.presentSuccessToast("Added");
    });

  }


  // ------------------------------ Passes ---------------------------------- //
  async returnContactSelection(fromContacts = true, singleSelection = true, isFromRequestPassScreen = false, contact_ids = []) {

    return new Promise(async resolve => {

      // const _data = await this.modals.present(
      //   VSyncContactsPageComponent,
      //   {
      //     fromContacts: fromContacts,
      //     singleSelection: singleSelection,
      //     isFromRequestPassScreen: isFromRequestPassScreen,
      //     contact_ids: contact_ids
      //   }
      // );

      // const data = _data.data;

      // console.log(data);

      // if (data['data'] != 'A') {
      //   // add these contacts to temporary contact list
      //   var imported_list = data['list'];

      //   resolve(imported_list);

      // } else {
      resolve([]);
      // }

    })

  }

  async getContactsByArrayOfIds(ids) {
    var _ids = ids;
    _ids = '( ' + _ids.join(',') + ' )';
    console.log(ids)
    let _data = await this.sqlite.getContactsByArrayOfIds(_ids);
    console.log(_data)
    return _data['contact_list'];
  }
}

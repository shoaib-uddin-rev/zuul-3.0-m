import { Injectable, ErrorHandler } from '@angular/core';
import * as stacktrace from 'stacktrace-js';


@Injectable({
  providedIn: 'root'
})
export class AppErrorHandlerService {

  public tag = 'AppErrorHandler';

  constructor() {}

  handleError(error: any): void {

      // do something with the error
      console.log(error);

      stacktrace.get().then( trace =>
        console.error(trace)
          // trace => this.firebase.logError(this.tag + ' Error =>' + error + 'Trace =>' + trace)
      );
  }
}

import { Injectable, Injector } from '@angular/core';
import { SQLite, SQLiteDatabaseConfig, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { browserDBInstance } from './browser-db-instance';

declare let window: any;
const SQL_DB_NAME = '__zuul.db';

@Injectable({
  providedIn: 'root'
})
export abstract class SqliteCrudService {

  db: any;
  config: SQLiteDatabaseConfig = {
    name: 'zuul_systems.db',
    location: 'default'
  };

  msg = 'Sync In Progress ...';

  public platform: Platform;
  public sqlite: SQLite;

  constructor(injector: Injector){
    this.sqlite = injector.get(SQLite);
    this.platform = injector.get(Platform);
  }




}

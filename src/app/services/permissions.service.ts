import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  rolekeys = {
    superAdmin: 'Super Administrator / Owner',
    subAdmin: 'Sub-Administrators (Community Admins)',
    employees: 'Employees',
    familyHead: 'Head of the Family',
    familyMember: 'Family Member',
    guestsOutsidersOneTime: 'Guests / Outsiders One Time',
    guestsOutsidersDaily: 'Guests / Outsiders Daily'
  };

  constructor(public network: NetworkService) { }



  isUserEmailPendingVerification(user){

    // fetch if any email verification is pending
    return new Promise( resolve => {
      // this.network.checkIfDuplicateEmailVerificationPending(user.id, user).then( res => {
      //   // console.log(res);
      //   resolve(res['count']);
      // })
    })

  }

  canSendPasses(user) {

    // console.log("P", user);
    if(!user){
      return false;
    }

    if(user.suspand == '1'){
      return false;
    }
    if(user.head_of_family == null){
      return false;
    }

    if(user.head_of_family != null){

      if( user.head_of_family == '1'){
        return true;
      }

      if( user.head_of_family == '0'){

        if(user.can_send_passes == null){
          return false;
        }

        if(user.can_send_passes != null){

          if( user.can_send_passes == '0'){
            return false
          }

          if( user.can_send_passes == '1'){
            return true;
          }

        }

      }

    }

    return false;
  }

  canUserBecomeResident(user) {
    let canResident = false;
    const roles = Object.keys(this.rolekeys);

    // console.log(user.roles);

    switch (user.roles[0].name) {
      case roles[0]: // super_admin
        break;
      case roles[1]: // sub_admin
        break;
      case roles[2]: // employees
        break;
      case roles[3]: // family_head
        canResident = true;
        break;
      case roles[4]: // family_member
        canResident = true;
        break;
      case roles[5]: // guests_outsiders_one_time
        break;
      case roles[6]: // guests_outsiders_daily
        canResident = true;
        break;
      default:
        break;
    }

    return canResident;
  }

  validateProfile(user): Promise<any> {

    return new Promise( async resolve => {

      console.log(user);

      if (
        user['first_name'] == '' ||
        !user['first_name'] ||
        user['last_name'] == '' ||
        !user['last_name'] ||
        user['date_of_birth'] == '' ||
        !user['date_of_birth'] ||
        user['phone_number'] == '' ||
        !user['phone_number'] ||
        user['dial_code'] == '' ||
        !user['dial_code'] ||
        user['email'] == '' ||
        !user['email'] ||
        user['street_address'] == '' ||
        !user['street_address'] ||
        user['city'] == '' ||
        !user['city'] ||
        user['state'] == '' ||
        !user['state'] ||
        user['zip_code'] == '' ||
        !user['zip_code']
      ) {
        resolve(false);
      }
      resolve(true);
    });

  }

}

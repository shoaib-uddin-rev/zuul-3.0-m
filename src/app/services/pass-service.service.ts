import { ModalService } from './basic/modal.service';
import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class PassServiceService {

  house_member_id = null;
  house_member_name = null;

  constructor(public utility: UtilityService, public sqlite: SqliteService, public network: NetworkService, public modals: ModalService) { }

  setDefaultEvent() {

    return {
      event_id: 14,
      event_name: 'Event Name'
    };

  }

  async getSelectedUserEvents(userId) {
    return await this.sqlite.getActiveUserEvent(userId);
  }

  getFutureDate(date, validity) {

    return new Promise(resolve => {
      const dataObj = { date, validity };

      this.network.getAddedHourDate(dataObj).then(res => {
        resolve(this.utility.customMDYHMformatDateMDYHM(res.date));
      });
    });


  }

  getFutureDateISO(date, validity) {

    return new Promise(resolve => {

      const dataObj = { date, validity };
      console.log(dataObj);
      this.network.getAddedHourDate(dataObj).then(res => {
        console.log(res);
        resolve(res.date);
      });
    });


  }

  createPass(formdata) {

    return new Promise(resolve => {
      console.log(formdata);
      this.network.createNewPass(formdata).then((res: any) => {
        // console.log(res);
        resolve(true);
      }, error => { resolve(false); });
    });

  }

  editPass(formdata) {

    return new Promise(resolve => {
      console.log(formdata);
      this.network.editPass(formdata).then((res: any) => {
        // console.log(res);
        resolve(true);
      }, error => { resolve(false); });
    });

  }


}

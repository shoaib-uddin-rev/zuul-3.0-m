import { AlertsService } from './basic/alerts.service';
import { StringsService } from './basic/strings.service';
import { Injectable } from '@angular/core';
import { Contacts } from '@ionic-native/contacts/ngx';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(private contacts: Contacts, public strings: StringsService, public alerts: AlertsService) { }

  getPhoneContacts() {

    return new Promise(async resolve => {
      const contacts = await this.fetchLocalCOntacts();
      resolve(contacts);
    });

  }



  fetchLocalCOntacts() {
    return new Promise((resolve, reject) => {
      this.contacts.find(['displayName', 'phoneNumbers', 'emails'],
        { desiredFields: ['displayName', 'phoneNumbers', 'emails'], filter: '', hasPhoneNumber: true, multiple: true })
        .then(data => {

          console.log("Here the Mobile Contacts", data);

          const tcontacts = [];

          for (const item of data) {

            console.log("here is Item", item);

            let f: any = {};

            if (!item.displayName || item.displayName == '') {
              continue;
            }

            if (!item.phoneNumbers || item.phoneNumbers.length <= 0) {
              continue;
            }

            f.display_name = item.displayName;
            f.email = (item.emails) ? item.emails[0].value : null;

            if (item.phoneNumbers) {

              //f["phone_number"] = ["4576543265", "5578563534", "6585756373"];
              if (item.phoneNumbers.length > 0) {

                // var _ccontacts = [];
                item.phoneNumbers.forEach(element => {

                  // trim phone number for last 10 digits

                  if (!element.value || element.value == '') {
                    return;
                  }

                  if (!this.strings.isPhoneNumberValid(element.value)) {
                    return;
                  }

                  f.phone_number = element.value;
                  f.type = element.type;

                  tcontacts.push(Object.assign({}, f));
                });



              }


            }
          }

          console.log("Tcontacts: ", tcontacts);

          resolve(tcontacts);

        })
        .catch(err => {
          console.error(err);
          resolve(null);
        });
    });
  }

  getSinglePhoneContact() {

    return new Promise((resolve) => {
      this.contacts.pickContact()
        .then(data => {
          if (!data.phoneNumbers) { resolve(null); return; }
          if (data.phoneNumbers.length > 0) {

            this.selectContactNumberAlertOptions('Select Number', data.phoneNumbers).then(num => {
              resolve(num);
            });

          } else if (data.phoneNumbers.length == 1) {
            const tnumber = (data.phoneNumbers) ? data.phoneNumbers[0].value : null;
            resolve(tnumber);
          }
          else {
            resolve(null);
          }


        });
    });
  }

  selectContactNumberAlertOptions(title, values) {
    return new Promise(async resolve => {

      const radioOptions = [];
      values.forEach((element, index) => {
        const ph = this.onkeyupFormatPhoneNumberRuntime(element.value);
        radioOptions.push({
          type: 'radio',
          label: ph + '-' + element.type,
          value: ph,
          checked: (index == 0) ? true : false
        });
      });

      const data = await this.alerts.presentRadioSelections(title, '', radioOptions);
      resolve(data);

    });
  }

  getOnlyDigits(phoneNumber) {
    return this.strings.getOnlyDigits(phoneNumber);
  }

  onkeyupFormatPhoneNumberRuntime(phoneNumber, last = true) {
    if (phoneNumber == null || phoneNumber == '') { return phoneNumber; }

    phoneNumber = this.getOnlyDigits(phoneNumber);
    // phoneNumber = phoneNumber.substring(phoneNumber.length - 1,-11);//keep only 10 digit Number
    // phoneNumber = phoneNumber.substring(phoneNumber.length - 10, -11);//keep only 10 digit Number
    phoneNumber = last ? phoneNumber.substring(phoneNumber.length - 10, phoneNumber.length) : phoneNumber.substring(0, 10);

    const cleaned = ('' + phoneNumber).replace(/\D/g, '');

    function numDigits(x: number) {
      return Math.log(x) * Math.LOG10E + 1 | 0;
    }

    // only keep number and +
    const p1 = cleaned.match(/\d+/g);
    if (p1 == null) { return cleaned; }
    const p2 = phoneNumber.match(/\d+/g).map(Number);
    const len = numDigits(p2);
    // document.write(len + " " );
    switch (len) {
      case 1:
      case 2:
        return '(' + phoneNumber;
      case 3:
        return '(' + phoneNumber + ')';
      case 4:
      case 5:
      case 6:
        var f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
        var f2 = phoneNumber.toString().substring(len, 3);
        return f1 + ' ' + f2;
      default:
        f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
        f2 = phoneNumber.toString().substring(3, 6);
        var f3 = phoneNumber.toString().substring(6, 10);

        console.log(phoneNumber, f3);
        return f1 + ' ' + f2 + '-' + f3;
    }
  }

}

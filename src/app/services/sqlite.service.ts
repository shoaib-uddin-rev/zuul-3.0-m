import { Injectable, Injector } from '@angular/core';
import { SQLite, SQLiteDatabaseConfig, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { NetworkService } from './network.service';
import { UtilityService } from './utility.service';
import { StorageService } from './basic/storage.service';
import { browserDBInstance } from './browser-db-instance';
const countries = require('./../data/countries.json');

declare let window: any;
const SQL_DB_NAME = '__zuul.db';

@Injectable({
  providedIn: 'root'
})
export class SqliteService {

  db: any;
  config: SQLiteDatabaseConfig = {
    name: 'zuul_systems.db',
    location: 'default'
  };

  msg = 'Sync In Progress ...';

  constructor(
    injector: Injector,
    public sqlite: SQLite,
    public platform: Platform,
    private storage: StorageService,
    private network: NetworkService,
    private utility: UtilityService) {

  }

  initialize() {

    return new Promise(resolve => {

      this.storage.get('is_database_initialized').then(async v => {
        if (!v) {
          await this.initializeDatabase();
          resolve(true);
        } else {
          resolve(true);
        }
      });
    });

  }

  async initializeDatabase() {

    return new Promise(async resolve => {
      await this.platform.ready();
      // initialize database object
      await this.createDatabase();
      // initialize all tables

      // initialize users table
      await this.initializeUsersTable();
      // initialize users table
      await this.initializeUserRolesTable();
      // initialize the user flags table for screens
      await this.initializeFlagsTable();
      // initialize users table
      // await this.initializeProfileTable();
      // initialize contacts table
      await this.initializeContactsTable();
      // initialize group table
      // await this.initializeGroupsTable();
      // // initialize contact group pivot table
      // await this.initializeContactCollectionTable();
      // // initialize sync contact local table
      await this.initializeSyncContactsTable();
      // // initialize events local table
      await this.initializeEventsTable();
      // // initialize vendors local table
      // await this.initializeVendorsTable();

      await this.initializeCountriesTable();
      await this.setCountryListInDatabase(countries);






      this.storage.set('is_database_initialized', true);
      resolve(true);
    });


  }

  async initializeCountriesTable() {
    return new Promise(resolve => {
      // create statement
      let sql = 'CREATE TABLE IF NOT EXISTS user_countries(';
      sql += 'code TEXT PRIMARY KEY, ';
      sql += 'dial_code TEXT, ';
      sql += 'name TEXT, ';
      sql += 'image TEXT )';

      this.msg = 'Initializing countries ...';
      resolve(this.execute(sql, []));
    });
  }

  async setCountryListInDatabase(countryList) {
    return new Promise(async resolve => {

      const insertRows = [];

      for (const row of countryList) {

        let sql = 'INSERT OR REPLACE into user_countries(';
        sql += 'code, ';
        sql += 'dial_code, ';
        sql += 'name, ';
        sql += 'image )';

        sql += ' VALUES ';

        const values = [];

        // dump data into sqlite in each loop
        sql += '( ';
        sql += '?, ';
        values.push(row.code);
        sql += '?, ';
        values.push(row.dial_code);
        sql += '?, ';
        values.push(row.name);
        sql += '? ';
        values.push(row.image);
        sql += ') ';

        // await this.execute(sql, values);
        insertRows.push([
          sql,
          values
        ]);

      }

      await this.prepareBatch(insertRows);

      resolve(true);

    });
  }

  async initializeFlagsTable() {

    return new Promise(resolve => {
      // create statement
      let sql = 'CREATE TABLE IF NOT EXISTS user_flags(';
      sql += 'id INTEGER PRIMARY KEY, ';
      sql += 'dashboard BOOLEAN DEFAULT false, ';
      sql += 'sync_contact_from_phone BOOLEAN DEFAULT false, ';
      sql += 'sync_contact_from_zuul BOOLEAN DEFAULT false, ';
      sql += 'google_map BOOLEAN DEFAULT false, ';
      sql += 'active_pass_list BOOLEAN DEFAULT false, ';
      sql += 'archive_pass_list_archive BOOLEAN DEFAULT false, ';
      sql += 'archive_pass_list_sent BOOLEAN DEFAULT false, ';
      sql += 'archive_pass_list_scanned BOOLEAN DEFAULT false, ';
      sql += 'sent_pass_list BOOLEAN DEFAULT false, ';
      sql += 'pass_details BOOLEAN DEFAULT false, ';
      sql += 'notifications BOOLEAN DEFAULT false, ';
      sql += 'request_a_pass BOOLEAN DEFAULT false, ';
      sql += 'create_new_pass BOOLEAN DEFAULT false )';

      this.msg = 'Initializing User Flags ...';
      resolve(this.execute(sql, []));
    });

  }

  async initializeUserRolesTable() {

    return new Promise(resolve => {
      // create statement
      let sql = 'CREATE TABLE IF NOT EXISTS user_roles(';
      sql += 'user_id INTEGER PRIMARY KEY, ';
      sql += 'role_id INTEGER, ';
      sql += 'name TEXT, ';
      sql += 'slug TEXT ';
      sql += ')';

      this.msg = 'Initializing Roles ...';
      resolve(this.execute(sql, []));
    });

  }

  async initializeUsersTable() {

    return new Promise(resolve => {
      // create statement
      let sql = 'CREATE TABLE IF NOT EXISTS users(';
      sql += 'id INTEGER PRIMARY KEY, ';
      sql += 'name TEXT, ';
      sql += 'first_name TEXT, ';
      sql += 'last_name TEXT, ';
      sql += 'email TEXT, ';
      sql += 'date_of_birth TEXT, ';
      sql += 'phone_number TEXT, ';
      sql += 'profile_image TEXT, ';

      sql += 'street_address TEXT, ';
      sql += 'apartment TEXT, ';
      sql += 'city TEXT, ';
      sql += 'state TEXT, ';
      sql += 'zip_code TEXT, ';
      sql += 'license_image TEXT, ';
      sql += 'is_license_locked INTEGER DEFAULT 0, ';

      sql += 'community TEXT, ';
      sql += 'house TEXT, ';
      sql += 'head_of_family INTEGER DEFAULT 0, ';
      sql += 'can_manage_family INTEGER DEFAULT 0, ';
      sql += 'can_send_passes INTEGER DEFAULT 0, ';
      sql += 'can_retract_sent_passes INTEGER DEFAULT 0, ';
      sql += 'fcm_token TEXT, ';
      sql += 'token TEXT, ';
      sql += 'dial_code TEXT DEFAULT \'+1\', ';
      sql += 'suspand INTEGER DEFAULT 0, ';
      sql += 'allow_parental_control INTEGER DEFAULT 0, ';
      sql += 'email_verification_code INTEGER DEFAULT 0, ';
      sql += 'is_guard INTEGER DEFAULT 0, ';
      sql += 'can_user_become_resident INTEGER DEFAULT 0, ';
      sql += 'can_show_settings INTEGER DEFAULT 0, ';
      sql += 'role_id INTEGER DEFAULT 0, ';
      sql += 'active INTEGER DEFAULT 0, ';
      sql += 'is_reset_password INTEGER DEFAULT 0, ';
      sql += 'licence_number TEXT )';

      this.msg = 'Initializing Users ...';
      resolve(this.execute(sql, []));
    });

  }

  async initializeContactsTable() {
    return new Promise(resolve => {
      // create statement
      let sql = 'CREATE TABLE IF NOT EXISTS contact_list(';
      sql += 'id INTEGER PRIMARY KEY, ';
      sql += 'created_by INTEGER NOT NULL, ';
      sql += 'user_id INTEGER NOT NULL, ';
      sql += 'community_id INTEGER, ';
      sql += 'display_name TEXT, ';
      sql += 'phone_number TEXT, ';
      sql += 'email TEXT, ';
      sql += 'profile_image TEXT, ';
      sql += 'is_favourite INTEGER DEFAULT 0, ';
      sql += 'is_assigned_temporary INTEGER DEFAULT 0, ';
      sql += 'dial_code TEXT DEFAULT \'+1\' )';
      this.msg = 'Initializing Contacts ...';
      resolve(this.execute(sql, []));
    });

  }

  async initializeSyncContactsTable() {
    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS sync_contact_list(";
      sql += "id INTEGER PRIMARY KEY, "
      sql += "display_name TEXT, ";
      sql += "phone_number TEXT, ";
      sql += "type TEXT, ";
      sql += "email TEXT, ";
      sql += "dial_code TEXT DEFAULT '+1' )";

      this.msg = 'Initializing Contacts ...';
      resolve(this.execute(sql, []));
    })

  }

  async initializeEventsTable() {

    return new Promise(resolve => {
      // create statement
      let sql = 'CREATE TABLE IF NOT EXISTS events(';
      sql += 'id INTEGER PRIMARY KEY, ';
      sql += 'name TEXT, ';
      sql += 'description TEXT,';
      sql += 'active INTEGER DEFAULT 0,';
      sql += 'created_by INTEGER )';

      this.msg = 'Initializing Events ...';
      resolve(this.execute(sql, []));
    });
  }

  public async getActiveUserEvent(userId) {

    return new Promise(async resolve => {

      const sql = 'SELECT * FROM events where created_by = ? and active = ? ';
      const values = [userId, 1];

      const d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        const event = data[0];
        resolve(event);
      } else {
        resolve(null);
      }

    });
  }

  public async setActiveUserEvent(userId, event) {

    return new Promise(async resolve => {


      const sql2 = 'UPDATE set events active = ? where created_by = ?';
      const values2 = [0, userId];
      await this.execute(sql2, values2);

      // check if user record exxist
      const sql = 'INSERT OR REPLACE INTO events ( id, name, description, active, created_by ) values ( ?, ?, ?, ?, ? )';
      const values = [event.id, event.name, event.description, 1, userId];
      await this.execute(sql, values);
      resolve(true);
    });

  }

  async setUserInDatabase(_user) {

    return new Promise(async resolve => {


      // set user role in database
      await this.setUserRolesInDatabase(_user);



      // check if user is already present in our local database, if not, create and fetch his data
      // check if user exist in database, if not create it else update it
      let sql = 'INSERT OR REPLACE INTO users(';
      sql += 'id, ';
      sql += 'name, ';
      sql += 'first_name, ';
      sql += 'last_name, ';
      sql += 'email, ';
      sql += 'date_of_birth, ';
      sql += 'phone_number, ';
      sql += 'profile_image, ';

      sql += 'street_address, ';
      sql += 'apartment, ';
      sql += 'city, ';
      sql += 'state, ';
      sql += 'zip_code, ';
      sql += 'license_image, ';
      sql += 'is_license_locked, ';

      sql += 'can_send_passes, ';
      sql += 'head_of_family, ';
      sql += 'can_manage_family, ';
      sql += 'allow_parental_control, ';
      sql += 'can_show_settings, ';
      sql += 'can_user_become_resident '
      sql += ')';

      sql += 'VALUES (';

      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';

      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';

      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '? '
      sql += ')';

      const values = [
        _user.id,
        _user.full_name,
        _user.first_name,
        _user.last_name,
        _user.email,
        _user.date_of_birth,
        _user.formatted_phone,
        _user.profile_image_url,

        _user.street_address,
        _user.apartment,
        _user.city,
        _user.state,
        _user.zip_code,
        _user.license_image_url,
        _user.is_license_locked == true ? 1 : 0,

        _user.can_send_passes == true ? 1 : 0,
        _user.role_id == 4 ? 1 : 0,
        _user.can_manage_family == true ? 1 : 0,
        _user.allow_parental_control == true ? 1 : 0,
        _user.can_show_settings == true ? 1: 0,
        _user.can_user_become_resident == true ? 1 : 0
      ];

      await this.execute(sql, values);

      if (_user.token) {

        const sql3 = 'UPDATE users SET active = ?';
        const values3 = [0];
        await this.execute(sql3, values3);

        const sql2 = 'UPDATE users SET token = ?, active = ? where id = ?';
        const values2 = [_user.token, 1, _user.id];

        await this.execute(sql2, values2);

      }

      await this.setUserFlagsInDatabase(_user.id);
      resolve(await this.getActiveUser());

    });
  }

  async setUserRolesInDatabase(user) {

    return new Promise(async resolve => {
      // check if user record exxist
      const sql = 'INSERT OR REPLACE INTO user_roles values ( ?, ?, ?, ? )';
      const values = [user.id, user.role.id, user.role.name, user.role.slug];
      await this.execute(sql, values);
      resolve(true);
    });

  }

  public async getActiveUserId() {

    return new Promise(async resolve => {
      const sql = 'SELECT id FROM users where active = ?';
      const values = [1];

      const d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        const id = data[0].id;
        resolve(id);
      } else {
        resolve(null);
      }

    });

  }

  public async getActiveUser() {
    return new Promise(async resolve => {
      const sql = 'SELECT * FROM users where active = ?';
      const values = [1];

      const d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        const user = data[0];
        resolve(user);
      } else {
        resolve(null);
      }

    });
  }


  async setUserFlagsInDatabase(id) {

    return new Promise<void>(async resolve => {
      // check if user record exxist
      const sql = 'INSERT OR IGNORE INTO user_flags(id) values ( ? )';
      const values = [id];
      await this.execute(sql, values);
      resolve();
    });

  }

  async setContactListInDatabase(contact_list) {
    return new Promise<void>(async resolve => {

      for (let i = 0; i < contact_list.length; i++) {

        let sql = 'INSERT OR REPLACE into contact_list(';
        sql += 'id, ';
        sql += 'created_by, ';
        sql += 'user_id, ';
        sql += 'community_id, ';
        sql += 'display_name, ';
        sql += 'phone_number, ';
        sql += 'email, ';
        sql += 'profile_image, ';
        sql += 'is_favourite, ';
        sql += 'dial_code )';

        sql += ' VALUES ';

        const values = [];

        // dump data into sqlite in each loop
        sql += '( ';
        sql += '?, ';
        values.push(contact_list[i].id);
        sql += '?, ';
        values.push(contact_list[i].created_by);
        sql += '?, ';
        values.push('');
        sql += '?, ';
        values.push('');
        sql += '?, ';
        values.push(contact_list[i].contact_name);
        sql += '?, ';
        values.push(contact_list[i].formatted_phone_number);
        sql += '?, ';
        values.push(contact_list[i].email);
        sql += '?, ';
        values.push('');
        sql += '?, ';

        values.push(parseInt(contact_list[i].is_favourite));
        sql += '? ';
        values.push(contact_list[i].dial_code);
        sql += ') ';

        await this.execute(sql, values);

      }

      resolve();

    });
  }


  public async getCurrentUserAuthorizationToken() {
    return new Promise(async resolve => {
      const user_id = await this.getActiveUserId();
      const sql = 'SELECT token FROM users where id = ? limit 1';
      const values = [user_id];

      const d = await this.execute(sql, values);
      // this.utility.presentToast(d);
      if (!d) {
        resolve(null);
        return;
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data[0].token);
      } else {
        resolve(null);
      }

    });
  }

  public async getCurrentUserContactsCount(userId) {

    return new Promise(async resolve => {
      const sql = 'SELECT COUNT(*) FROM contact_list where user_id = ?';
      const values = [userId];

      const d = await this.execute(sql, values);
      if (!d) { resolve(0); return; }
      const data = this.getRows(d);
      if (data.length == 0) { resolve(0); return; }
      resolve(data[0]['COUNT(*)']);
    });

  }

  public async getCountriesInDatabase(search = null, offset = 0, loader) {

    // is_assigned_temporary is the key here

    return new Promise(async resolve => {

      let sql = 'SELECT * FROM user_countries ';
      const values = [];

      if (search) {
        sql += 'where code like ? or dial_code like ? or name like ? ';
        values.push('%' + search + '%', '%' + search + '%', '%' + search + '%');
      }

      sql += ' ORDER BY name ASC limit ? OFFSET ?';
      values.push(30, offset);

      const d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        const obj = {
          offset: -1,
          countries_list: []
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);

      if (data.length > 0) {

        offset = (data.length < 30) ? -1 : (offset + 30);

        const obj = {
          offset,
          countries_list: data
        };
        resolve(obj);

      } else {

        const obj = {
          offset: -1,
          countries_list: []
        };
        resolve(obj);
      }

    });

  }

  public async getTemporaryContactsInDatabase(search = null, offset = 0, loader) {

    // is_assigned_temporary is the key here

    return new Promise(async resolve => {

      const id = await this.getActiveUserId();

      let sql = 'SELECT * FROM contact_list where created_by = ? and is_assigned_temporary = ?';
      const values = [id, 1];

      if (search) {
        sql += ' and ( display_name like ? or phone_number like ? ) ';
        values.push('%' + search + '%', '%' + search + '%');
      }

      sql += ' ORDER BY display_name ASC limit ? OFFSET ?';
      values.push(30, offset);

      const d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        const obj = {
          offset: -1,
          contact_list: []
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);

      if (data.length > 0) {

        offset = (data.length < 30) ? -1 : (offset + 30);

        const obj = {
          offset,
          contact_list: data
        };
        resolve(obj);

      } else {

        const obj = {
          offset: -1,
          contact_list: []
        };
        resolve(obj);
      }

    });

  }

  public async getActiveEventInEvents() {

    return new Promise(async resolve => {

      const sql = 'SELECT * FROM events where active = ? limit ?';
      const values = [1, 1];
      const d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        resolve(null);
        return;
      }
      const data = this.getRows(d);

      if (data.length > 0) {
        resolve(data[0]);
      } else {
        resolve(null);
      }
    });

  }

  public async getContacts(search = null, offset = 0, is_favorite, loader, contact_ids = []) {

    return new Promise(async resolve => {
      const id = await this.getActiveUserId();


      let sql = 'SELECT * FROM contact_list where created_by = ? ';
      let values = [id];

      if (contact_ids.length > 0) {
        const contactIds = '(' + contact_ids.join(',') + ')';

        sql = 'SELECT * FROM contact_list where id in ' + contactIds + ' and  created_by = ? ';
        values = [id];
      }

      if (search) {
        sql += ' and ( display_name like ? or phone_number like ? ) ';
        values.push('' + search + '%', '%' + search + '%');
      }

      if (is_favorite == 1) {
        sql += 'and is_favourite = ? ';
        const is_favourite = (is_favorite == true) ? 1 : 0;
        values.push(is_favourite);
      }



      sql += ' ORDER BY display_name ASC limit ? OFFSET ?';
      values.push(30, offset);

      const d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        const obj = {
          offset: -1,
          contact_list: []
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);

      if (data.length > 0) {

        offset = (data.length < 30) ? -1 : (offset + 30);

        const obj = {
          offset,
          contact_list: data
        };
        resolve(obj);

      } else {

        const obj = {
          offset: -1,
          contact_list: []
        };
        resolve(obj);
      }

    });

  }

  public async checkIfPhoneSyncAlready() {
    return new Promise(async resolve => {
      const sql = 'SELECT COUNT(*) FROM sync_contact_list';
      const values = [];

      const d = await this.execute(sql, values);
      if (!d) { resolve(0); }
      const data = this.getRows(d);
      if (data.length == 0) { resolve(0); }
      resolve(data[0]['COUNT(*)']);
    });
  }

  public async getSyncContacts(search = null, offset = 0, loader) {

    return new Promise(async resolve => {
      const id = await this.getActiveUserId();



      let sql = 'SELECT id, display_name, phone_number as phone_number , GROUP_CONCAT(phone_number) as phone_numbers, GROUP_CONCAT(type) as type  FROM sync_contact_list ';
      const values = [];

      if (search) {
        sql += ' where ( display_name like ? or phone_number like ? ) ';
        values.push('' + search + '%', '%' + search + '%');
      }

      sql += ' GROUP BY display_name ORDER BY display_name ASC limit ? OFFSET ? ';
      values.push(30, offset);

      const d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        const obj = {
          offset: -1,
          contact_list: []
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);

      if (data.length > 0) {

        offset = (data.length < 30) ? -1 : offset + 30;

        const obj = {
          offset,
          contact_list: data
        };
        resolve(obj);

      } else {

        const obj = {
          offset: -1,
          contact_list: []
        };
        resolve(obj);
      }

    });

  }

  public async getContactGroupByUserId(loader) {

    return new Promise(async resolve => {
      const id = await this.getActiveUserId();

      const sql = 'SELECT * FROM contact_group where user_id = ? ORDER BY group_name ASC  ';
      const values = [id];

      const d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        const obj = {
          offset: 0,
          group: []
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);
      if (data.length > 0) {

        const obj = {
          offset: 0,
          group: data
        };
        resolve(obj);

      } else {

        const obj = {
          offset: 0,
          group: []
        };
        resolve(obj);
      }

    });


  }

  public async getGroupContactList(search, id, offset = 0, loader) {

    return new Promise(async resolve => {

      let sql = 'SELECT cc.id as collection_id, cl.* FROM contact_collection cc Inner Join contact_list cl ON cl.id = cc.contact_id where group_id = ?';
      const values = [id];

      if (search) {
        sql += ' and (cl.display_name like ? or cl.phone_number like ? )';
        values.push('%' + search + '%', '%' + search + '%');
      }

      sql += ' ORDER BY display_name ASC limit ? OFFSET ?';
      values.push(30, offset);

      const d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        const obj = {
          offset: -1,
          contact_list: []
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);
      if (data.length > 0) {

        offset = (data.length < 30) ? -1 : (offset + 30);

        const obj = {
          offset,
          contact_list: data
        };

        resolve(obj);

      } else {

        const obj = {
          offset: -1,
          contact_list: []
        };
        resolve(obj);
      }

    });

  }

  public async getUserEvents(search, id, offset = 0, loader) {
    return new Promise(async resolve => {

      let sql = 'SELECT * FROM events where ( created_by = ? or created_by = ? ) ';
      const values = [id, 0];

      if (search) {
        sql += ' and (event_name like ? or event_description like ? )';
        values.push('%' + search + '%', '%' + search + '%');
      }

      sql += ' ORDER BY event_name ASC limit ? OFFSET ?';
      values.push(30, offset);

      const d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        const obj = {
          offset: -1,
          events: []
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);
      if (data.length > 0) {

        offset = (data.length < 30) ? -1 : (offset + 30);

        const obj = {
          offset,
          events: data
        };

        resolve(obj);

      } else {

        const obj = {
          offset: -1,
          events: []
        };
        resolve(obj);
      }

    });
  }

  public async getUserVendors(search, id, offset = 0, loader) {
    return new Promise(async resolve => {

      let sql = 'SELECT * FROM vendors';
      const values = [];

      if (search) {
        sql += ' and (vendor_name like ? or address like ? or email like ? or phone like ? or place_name like ?)';
        values.push('%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%');
      }

      sql += ' ORDER BY vendor_name ASC limit ? OFFSET ?';
      values.push(30, offset);


      const d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        const obj = {
          offset: -1,
          vendors: []
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);
      if (data.length > 0) {

        offset = (data.length < 30) ? -1 : (offset + 30);

        const obj = {
          offset,
          vendors: data
        };

        resolve(obj);

      } else {

        const obj = {
          offset: -1,
          vendors: []
        };
        resolve(obj);
      }

    });
  }

  public async getContactsByArrayOfIds(ids) {

    return new Promise(async resolve => {


      const sql = 'SELECT * FROM contact_list where id in ' + ids + ' order by display_name ASC';
      const values = [];

      const d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        const obj = {
          offset: -1,
          contact_list: []
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);
      if (data.length > 0) {



        const obj = {
          offset: -1,
          contact_list: data
        };
        resolve(obj);

      } else {

        const obj = {
          offset: -1,
          contact_list: []
        };
        resolve(obj);
      }

    });

  }

  public async removeContactInFavorites(item) {

    return new Promise<void>(async resolve => {
      const sql = 'UPDATE contact_list SET is_favourite = ? where id = ?';
      const values = [0, item.id];
      await this.execute(sql, values);
      resolve();
    });

  }

  public async removeContactFromGroup(collection_id) {

    return new Promise<void>(async resolve => {
      const sql = 'DELETE FROM contact_collection where id = ?';
      const values = [collection_id];
      await this.execute(sql, values);
      resolve();
    });

  }

  public async removeAllContactFromTemporary() {

    return new Promise<void>(async resolve => {
      const id = await this.getActiveUserId();
      const sql = 'UPDATE contact_list SET is_assigned_temporary = ? where created_by = ?';
      const values = [0, id];
      await this.execute(sql, values);
      resolve();
    });

  }

  public async removeContactFromTemporary(contact_id) {

    return new Promise<void>(async resolve => {

      const sql = 'UPDATE contact_list SET is_assigned_temporary = ? where id = ?';
      const values = [0, contact_id];
      await this.execute(sql, values);
      resolve();
    });

  }

  public async removeContactInDatabase(item) {

    return new Promise<void>(async resolve => {
      const sql = 'DELETE FROM contact_list where id = ?';
      const values = [item.id];
      await this.execute(sql, values);
      resolve();
    });

  }

  public async removeGroupInDatabase(item) {

    return new Promise<void>(async resolve => {
      const sql = 'DELETE FROM contact_group where id = ?';
      const values = [item.id];
      await this.execute(sql, values);

      const sql2 = 'DELETE FROM contact_collection where group_id = ?';
      const values2 = [item.id];
      await this.execute(sql2, values2);

      resolve();
    });

  }

  public async removeEventFromDatabase(event_id) {

    return new Promise<void>(async resolve => {
      const sql = 'DELETE FROM events where id = ?';
      const values = [event_id];
      await this.execute(sql, values);
      resolve();
    });

  }

  public async removeVendorFromDatabase(vendor_id) {

    return new Promise<void>(async resolve => {
      const sql = 'DELETE FROM vendors where id = ?';
      const values = [vendor_id];
      await this.execute(sql, values);
      resolve();
    });

  }

  removeLoginFromLocal(user_id) {

    return new Promise(async resolve => {
      const sql = 'DELETE FROM users where id = ?';
      const values = [user_id];
      await this.execute(sql, values);
      const users = await this.getAllRecords('users');
      resolve(users);
    });

  }

  public async addContactsToGroup(group_id, contact_collection) {

    return new Promise<void>(async resolve => {
      for (let i = 0; i < contact_collection.length; i++) {

        let sql = 'INSERT OR REPLACE into contact_collection(';
        sql += 'id, ';
        sql += 'group_id, ';
        sql += 'contact_id )';
        sql += ' VALUES ';
        const values = [];
        // dump data into sqlite in each loop
        sql += '( ';
        sql += '?, ';
        values.push(contact_collection[i].id);
        sql += '?, ';
        values.push(group_id);
        sql += '? ';
        values.push(contact_collection[i].contact_id);
        sql += ') ';
        // sql+= " where ";
        // sql+= " group_id = ? and"
        // values.push(group_id);
        // sql+= " contact_id = ? ;"
        // values.push(contact_collection[i]["contact_id"]);

        await this.execute(sql, values);



        // let sql = "INSERT OR REPLACE into contact_collection (id, group_id, contact_id) values ( ? , ? , ? ) where group_id = ? and contact_id = ?";
        // let values = [contact_ids[i]["id"], group_id, contact_ids[i]["contact_id"], group_id, contact_ids[i]["contact_id"]  ];
        //
        // await this.execute(sql, values );
      }

      resolve();
    });

  }

  setLogout() {

    return new Promise(async resolve => {
      const user_id = await this.getActiveUserId();

      const sql = 'UPDATE users SET token = ?, active = ? where id = ?';
      const values = [null, 0, user_id];

      const d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(true);
      } else {
        resolve(false);
      }


    });

  }

  switchLogin(user_id) {

    return new Promise<void>(async (resolve) => {

      const sql3 = 'UPDATE users SET active = ?';
      const values3 = [0];
      await this.execute(sql3, values3);

      const sql2 = 'UPDATE users SET active = ? where id = ? and token is not null';
      const values2 = [1, user_id];

      await this.execute(sql2, values2);

      resolve();

    });

  }

  setFcmToken(fcm_token) {

    return new Promise(async (resolve) => {
      const id = this.getActiveUserId();
      const sql = 'UPDATE users SET fcm_token=? where id = ?';
      const values = [fcm_token, id];
      resolve(await this.execute(sql, values));

    });
  }

  getModelCount(table, query, array) {

    // sql starts from - where
    return new Promise(async resolve => {
      const sql = `SELECT COUNT(*) FROM ${table} ${query}`;
      const values = array;

      const d = await this.execute(sql, values);
      if (!d) { resolve(0); return; }
      const data = this.getRows(d);
      if (data.length == 0) { resolve(0); return; }
      resolve(data[0]['COUNT(*)']);
    });

  }

  execute(sql, params) {

    return new Promise(async resolve => {

      if (!this.db) {
        await this.platform.ready();
        // initialize database object
        await this.createDatabase();
      }
      // // if(this.platform.is('cordova')){
      this.db.executeSql(sql, params).then(response => {
        resolve(response);
      }).catch(err => {
        console.error(err);
        resolve(null);
      });

    });
  }

  prepareBatch(insertRows) {

    return new Promise(async resolve => {

      const size = 250; const arrayOfArrays = [];

      for (let i = 0; i < insertRows.length; i += size) {
        arrayOfArrays.push(insertRows.slice(i, i + size));
      }

      for (const element of arrayOfArrays) {
        await this.executeBatch(element);
        // await this.execute(s, p)
      }

      resolve(true);

    });
  }

  executeBatch(array) {

    return new Promise(async resolve => {

      if (!this.db) {
        await this.platform.ready();
        // initialize database object
        await this.createDatabase();
      }

      this.db.sqlBatch(array).then(response => {
        resolve(response);
      }).catch(err => {
        console.error(err);
        resolve(null);
      });

    });
  }

  async createDatabase() {
    return new Promise(async resolve => {
      if (this.platform.is('cordova')) {
        await this.sqlite.create(this.config).then(db => {
          this.msg = 'Database initialized';
          this.db = db;
        });
      } else {
        const db = window.openDatabase(SQL_DB_NAME, '1.0', 'DEV', 5 * 1024 * 1024);
        this.db = browserDBInstance(db);
        this.msg = 'Database initialized';

      }
      resolve(true);
    });


  }

  getRows(data) {
    const items = [];
    for (let i = 0; i < data.rows.length; i++) {
      const item = data.rows.item(i);

      items.push(item);
    }

    return items;
  }

  async getAllRecords(table) {

    return new Promise(async resolve => {
      const sql = 'SELECT * FROM ' + table;
      const values = [];

      const d = await this.execute(sql, values);

      if (!d) {
        resolve([]);
        return;
      }

      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }

    });
  }

  public async setSyncContactListInDatabase(contact_list, resync = false) {

    return new Promise<void>(async resolve => {
      const id = await this.getActiveUserId();

      if (resync) {
        const sql2 = 'DELETE FROM sync_contact_list';
        await this.execute(sql2, []);
      }


      for (let i = 0; i < contact_list.length; i++) {

        let sql = 'INSERT OR REPLACE into sync_contact_list(';
        sql += 'id, ';
        sql += 'display_name, ';
        sql += 'phone_number, ';
        sql += 'type, ';
        sql += 'email, ';
        sql += 'dial_code )';

        sql += ' VALUES ';

        const values = [];

        // dump data into sqlite in each loop
        sql += '( ';
        sql += '?, ';
        values.push(null);
        sql += '?, ';
        values.push(contact_list[i].display_name);
        sql += '?, ';
        const pn = this.utility.onkeyupFormatPhoneNumberRuntime(contact_list[i].phone_number, true);
        values.push(pn);
        sql += '?, ';
        values.push(contact_list[i].type);
        // values.push("mobile");
        sql += '?, ';
        values.push(contact_list[i].email);
        sql += '? ';
        values.push(contact_list[i].dial_code);
        sql += ') ';
        await this.execute(sql, values);

      }

      resolve();

    });
  }

}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EventsService } from './events.service';
import { ApiService } from './api.service';
import { UtilityService } from './utility.service';
import { Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(
    public utility: UtilityService,
    public api: ApiService,
    public router: Router,
    private events: EventsService,
  ) {
    // console.log('Hello NetworkProvider Provider');
  }

  login(data) {
    return this.httpPostResponse('login', data, null, true);
  }

  register(data) {
    return this.httpPostResponse('signup', data, null, true);
  }

  forgetPassword(data) {
    return this.httpPostResponse('forget-password', data, null, true);
  }

  getUser(loader = false) {
    return this.httpGetResponse('user', null, loader, false);
  }

  updateProfile(data) {
    return this.httpPostResponse('profile', data, null, true);
  }

  checkEmailAlreadyInUse(data) {
    return this.httpPostResponse('check-email-already-in-use', data, null, true);
  }

  verifyEmailAddress(data) {
    return this.httpPostResponse('verify-email-address', data, null, true);
  }

  getLicenceLockStatus() {
    return this.httpGetResponse('get-license-lock-status', null, false, true);
  }

  sendCodeToBecomeResident(data) {
    return this.httpPostResponse('resident_verification', data, null, true)
  }

  sendCodeToVerifyEmail(data) {
    return this.httpPostResponse('email_verification', data, null, true)
  }

  sendRequestToBecomeResident(data) {
    return this.httpPostResponse('send_request_to_become_resident', data, null, true)
  }

  getSoundLocation() {
    return this.httpGetResponse('get-sound-location', null, false, false);
  }

  // Contacts
  getUserContacts(params) {
    const str = this.serialize(params);
    return this.httpGetResponse('user-contacts' + '?' + str, null, false, false);
  }

  createContact(data) {
    return this.httpPostResponse('user-contacts', data, null, true);
  }

  addContactWithOnlyId(data) {
    return this.httpPostResponse('add-contact-by-created-by-id', data, null, true);
  }

  addContactWithOnlyUserId(data) {
    return this.httpPostResponse('add-contact-by-user-id', data, null, true);
  }

  syncContacts(data) {
    return this.httpPostResponse('bulk-import-user-contact', data, null, true);
  }
  // Groups
  getUserGroups(params) {
    const str = this.serialize(params);
    return this.httpGetResponse('contact-groups' + '?' + str, null, );
  }

  createContactGroup(data) {
    return this.httpPostResponse('contact-groups', data, null, true);
  }

  updateContactGroup(id, data) {
    return this.httpPutResponse('contact-groups/' + id, data, id, true);
  }

  addContactsToGroup(data, loader) {
    return this.httpPostResponse('add-contact-in-group', data, null, loader);
  }

  deleteContactGroup(data) {
    return this.httpPostResponse('bulk-delete-group', data, null, true);
  }

  removeFromGroup(data) {
    return this.httpPostResponse('bulk-remove-contact-from-group', data, null, false);
  }

  getGroupContactList(data, loader = false) {
    return this.httpPostResponse('get-user-contact-by-group', data, null, loader);
  }

  bulkDeleteContact(data) {
    return this.httpPostResponse('bulk-delete-user-contact', data, null, false);
  }

  getSingleContactById(id) {
    return this.httpGetResponse('user-contacts' + '/' + id);
  }
  //Favorites
  removeFromFavorites(data) {
    return this.httpPostResponse('remove-contact-in-favorite', data, null, false);
  }

  addToFavorites(data) {
    return this.httpPostResponse('add-contact-in-favorite', data, null, false);
  }
  // Events
  getUserEvents(params) {
    const str = this.serialize(params);
    return this.httpGetResponse('events' + '?' + str, null, true);
  }

  addEventToList(data) {
    return this.httpPostResponse('events', data, null, true);
  }

  editEventToList(id, data) {
    return this.httpPutResponse('events/' + id, data, null, true);
  }

  removeEventFromList(id) {
    return this.httpDeleteResponse('events/' + id, null, null, false);
  }

  // pass api
  getActivePassesData(id, params, loader) {
    const str = this.serialize(params);
    return this.httpGetResponse('active-passes-data/' + id + '?' + str, null, loader, false);
  }

  updatePassDirections(data) {
    return this.httpPostResponse('update_pass_directions', data, null, true)
  }

  retractSentPass(id, data) {
    return this.httpPostResponse('retract-sent-pass', data, null, true);
  }

  getActivePassesDates(id, params) {
    const str = this.serialize(params);
    return this.httpGetResponse('active-passes-dates/' + id + '?' + str, null, false, false);
  }

  removePassRecipient(id) {
    return this.httpGetResponse('remove-pass-recipient', id, true)
  }

  togglePassEnable(id, is_enabled) {
    return this.httpGetResponse('toggle_pass_enable', id + "/" + is_enabled, true)
  }

  addContactToPass(id, data) {
    return this.httpPostResponse('add-recipient-in-pass/' + id, data, null, true)
  }

  getContactToPass(id) {
    return this.httpGetResponse('get-pass-recipients/' + id, null, true);
  }

  getScannedPassLogs(id) {
    return this.httpGetResponse('get-scanned-pass-recipients', id, true);
  }

  getPasseDetails(id) {
    return this.httpGetResponse('passes-detail', id, false);
  }

  getUserReceivedPasses(id, params, loader = true) {
    const str = this.serialize(params);
    return this.httpGetResponse('active-passes-data/' + id + '?' + str, null, false);
  }

  getUserReceivedPassesDates(id, params) {
    console.log(id);
    const str = this.serialize(params);
    return this.httpGetResponse('active-passes-dates/' + id + '?' + str, null, false);
  }

  getUserSentPasses(id, params, loader = true) {
    const str = this.serialize(params);
    return this.httpGetResponse('sent-passes-data/' + id + '?' + str, null, false);
  }

  getUserSentPassesDates(id, params) {
    const str = this.serialize(params);
    return this.httpGetResponse('sent-passes-dates/' + id + '?' + str, null, false);
  }

  getUserScannedPasses(id, params, loader = true) {
    const str = this.serialize(params);
    return this.httpGetResponse('scanned-passes-data/' + id + '?' + str, null, false);
  }

  getUserScannedPassesDates(id, params) {
    const str = this.serialize(params);
    return this.httpGetResponse('scanned-passes-dates/' + id + '?' + str, null, false);
  }

  getResidentsListToRequestPass() {
    return this.httpGetResponse('get-users-can-send-pass', null, true, true);
  }

  sendRequestForAPass(data) {
    return this.httpPostResponse('send-request-for-pass', data, null, false, true);
  }

  sendRequestForAPassForContact(data) {
    return this.httpPostResponse('send-pass-request-from-contact', data, null, false, true);
  }

  getAddedHourDate(data) {
    return this.httpPostResponse('get-added-hour-date', data, null, false);
  }

  createNewPass(data) {
    return this.httpPostResponse('passes', data, null, true);
  }

  editPass(data) {
    return this.httpPatchResponse('passes/' + data.id, data, null, true);
  }

  // Vehicles

  getVehicleList() {
    return this.httpGetResponse('vehicles', null, false);
  }

  addVehicle(params) {
    return this.httpPostResponse('vehicles', params, null, false);
  }

  editVehicle(id, params) {
    return this.httpPutResponse('vehicles/' + id, params, null, false);
  }

  deleteVehicle(id) {
    return this.httpDeleteResponse('vehicles/' + id, null, null, false);
  }

  setDefaultVehicle(params) {
    return this.httpPatchResponse('set-pass-vehicle', params, false);
  }

  // Notification
  removeNotification(id) {
    return this.httpGetResponse('remove-notification', id, false);
  }

  rejectPassRequest(data) {
    return this.httpPostResponse('reject-pass-request', data, null, true);
  }

  acceptPassRequest(data) {
    return this.httpPostResponse('accept-pass-request', data, null, true);
  }

  // notifications

  saveFcmToken(data) {
    return this.httpPostResponse('save-fcm-token', data, null, false, false)
  }

  getParentalNotifications(params) {
    let str = this.serialize(params);
    return this.httpGetResponse('get-parental-notifications' + '?' + str, null, false)
  }

  getNotificationsCount() {
    return this.httpGetResponse('get-notification-count', null, false);
  }

  getOnePageNotifications(params) {
    const str = this.serialize(params);
    return this.httpGetResponse('get-zuul-notifications' + '?' + str, null, false);
  }

  markReadNotification(data) {
    return this.httpPostResponse('mark-read-push-notification', data, null, false)
  }

  markReadPCNotification(data) {
    return this.httpPostResponse('mark-read-pc-notification', data, null, false)
  }

  // Family
  getMyFamilyMembers() {
    return this.httpGetResponse('family-members', null, false);
  }

  updateUserNotificationSettings(data) {
    return this.httpPostResponse('update-user-notification-settings', data, null, false);
  }

  getListOfCommunities() {
    return this.httpGetResponse('get_list_of_communities', null, true)
  }

  setParentalControlOptions(id, data) {
    return this.httpPostResponse('set-parental-control-options', data, id);
  }

  setAllowParentalPermission(data) {
    return this.httpPostResponse('set-allow-parental-permission', data, null, false);
  }

  setManageFamilyPermission(data) {
    return this.httpPostResponse('set-manage-family-permission', data, null, false);
  }

  setSendPassesPermission(data) {
    return this.httpPostResponse('set-send-passes-permission', data, null, false);
  }

  getParentalControlOptions(id) {
    return this.httpGetResponse('get-parental-control-options/' + id, null);
  }

  setReadAnnouncements(id) {
    return this.httpGetResponse('set_read_announcements', id, true)
  }

  getUnreadAnnouncements() {
    return this.httpGetResponse('announcements', null, true)
  }

  serialize = ((obj) => {
    const str = [];
    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    }
    return str.join('&');
  });


  httpPostResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('post', key, data, id, showloader, showError, contenttype);
  }

  httpGetResponse(key, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('get', key, {}, id, showloader, showError, contenttype);
  }

  httpPutResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return new Promise((resolve, reject) => {

      id = id ? `/${id}` : '';
      const url = key + id;

      this.api.put(key, data).subscribe((res: any) => {
        if (res.bool !== true) {
          if (showError) {
            this.utility.presentSuccessToast(res.message);
          }
          reject(null);
        } else {
          resolve(res.result);
        }
      });
    });
  }

  httpPatchResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return new Promise((resolve, reject) => {

      id = id ? `/${id}` : '';
      const url = key + id;

      this.api.patch(key, data).subscribe((res: any) => {
        if (res.bool !== true) {
          if (showError) {
            this.utility.presentSuccessToast(res.message);
          }
          reject(null);
        } else {
          resolve(res.result);
        }
      });
    });
  }

  httpDeleteResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return new Promise((resolve, reject) => {
      this.api.delete(key).subscribe((res: any) => {
        console.log(res);
        if (res.bool !== true) {
          if (showError) {
            this.utility.presentSuccessToast(res.message);
          }
          reject(null);
        } else {
          resolve(res.result);
        }
      });
    });
  }

  // default 'Content-Type': 'application/json',
  httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true, contenttype = 'application/json'): Promise<any> {

    return new Promise((resolve, reject) => {

      if (showloader === true) {
        this.utility.showLoader();
      }

      id = (id) ? '/' + id : '';
      const url = key + id;

      const seq = (type === 'get') ? this.api.get(url, {}) : this.api.post(url, data);

      seq.subscribe((res: any) => {

        if (showloader === true) {
          this.utility.hideLoader();
        }

        if (res.bool !== true) {
          if (showError) {
            this.utility.presentSuccessToast(res.message);
          }
          reject(null);
        } else {
          resolve(res.result);
        }

      }, err => {

        const error = err.error;
        if (showloader === true) {
          this.utility.hideLoader();
        }

        if (showError) {
          this.utility.presentFailureToast(error.message);
        }

        console.log(err);

        // if(err.status === 401){
        //   this.router.navigate(['splash']);
        // }

        reject(null);

      });

    });

  }

  showFailure(err) {
    // console.error('ERROR', err);
    err = (err) ? err.message : 'check logs';
    this.utility.presentFailureToast(err);
  }

  getActivityLogs() {
    return this.httpGetResponse('get_activity_logs', null, true);
  }

}

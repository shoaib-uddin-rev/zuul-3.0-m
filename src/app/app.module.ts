import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
// import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppErrorHandlerService } from './services/app-error-handler.service';
import { EventsService } from './services/events.service';
import { ApiService } from './services/api.service';
import { StorageService } from './services/basic/storage.service';
import { NetworkService } from './services/network.service';
import { SqliteService } from './services/sqlite.service';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Calendar } from '@ionic-native/calendar/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Contacts } from '@ionic-native/contacts/ngx';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { InterceptorService } from './services/interceptor.service';
import { PagesModule } from './pages/pages.module';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
} from '@capacitor/push-notifications';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
      mode: 'ios'
    }),
    AppRoutingModule,
    HttpClientModule,
    PagesModule,
    Ng2SearchPipeModule,
    NgxQRCodeModule
  ],
    providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: ErrorHandler, useClass: AppErrorHandlerService },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    SQLite,
    Calendar,
    ImagePicker,
    LaunchNavigator,
    CallNumber,
    InAppBrowser,
    OpenNativeSettings,
    DatePicker,
    Geolocation,
    Contacts,
    Camera,
    StatusBar,
    NativeStorage,
    // custom providers
    NgxPubSubService,
    EventsService,
    SqliteService,
    StorageService,
    ApiService,
    NetworkService,
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    NativePageTransitions


  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}



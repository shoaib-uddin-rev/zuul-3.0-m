import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ParentalNotificationItemComponent } from './parental-notification-item.component';

@NgModule({
	declarations: [
		ParentalNotificationItemComponent
	],
	imports: [
    CommonModule, IonicModule, FormsModule, ReactiveFormsModule
	],
	exports: [
		ParentalNotificationItemComponent
	]
})
export class ParentalNotificationItemComponentModule {}

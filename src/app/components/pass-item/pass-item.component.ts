import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IonItemSliding } from '@ionic/angular';
import { PopoversService } from 'src/app/services/basic/popovers.service';
import { EventsService } from 'src/app/services/events.service';
import { PassItemOptionsComponent } from './pass-item-options/pass-item-options.component';

@Component({
  selector: 'app-pass-item',
  templateUrl: './pass-item.component.html',
  styleUrls: ['./pass-item.component.scss'],
})
export class PassItemComponent implements OnInit {

  @Input() seeingOthersPass = false;
  @Input() page = 'active';
  @Input() selection = '';
  @Output() openPass: EventEmitter<any> = new EventEmitter<any>();
  @Output() openDirection: EventEmitter<any> = new EventEmitter<any>();
  @Output() removePass: EventEmitter<any> = new EventEmitter<any>();
  @Output() editPass: EventEmitter<any> = new EventEmitter<any>();
  @Output() showHistory: EventEmitter<any> = new EventEmitter<any>();
  @Output() showCheckbox: EventEmitter<any> = new EventEmitter<any>();
  @Output() showGuestLogs: EventEmitter<any> = new EventEmitter<any>();
  titem: any;
  get item(): any {
    return this.titem;
  }

  @Input() set item(value: any) {
    this.titem = value;
    console.log(value);
  };
  bgcolor = 'green-background'; // green-background, gray-background

  constructor(public events: EventsService, public popover: PopoversService) { }

  ngOnInit() { }

  // archieved functions
  public open(itemSlide: IonItemSliding) {

    itemSlide.open('end');

    // reproduce the slide on the click
    // if(!this.seeingOthersPass &&  this.item.page != 'scanned'){
    //   itemSlide..setElementClass("active-sliding", true);
    //   itemSlide.setElementClass("active-slide", true);
    //   itemSlide.setElementClass("active-options-right", true);
    //   item.setElementStyle("transform", "translate3d(-200px, 0px, 0px)");
    // }
  }

  passDetails(item) {
    this.openPass.emit(item);
  }

  getDirection(item) {
    console.log(item.description);
    this.openDirection.emit(item.description);
  }

  deletePass(item) {
    this.removePass.emit(item);
  }

  updatePass(item) {
    this.editPass.emit(item);
  }

  btnShowHistory($event, item) {
    this.showHistory.emit({ event: $event, item });
  }

  showRecepients(item) {
    this.showCheckbox.emit(item);
  }

  showScanHistory(item) {
    this.showHistory.emit(item);
  }

  btnShowGuestLogs($event, item) {
    this.showGuestLogs.emit({ event: $event, item });
  }

  async openOptions($event) {
    const res = await this.popover.present(PassItemOptionsComponent, $event, {
      page: this.page,
      type: this.selection,
      item: this.item,
      seeingOthersPass: this.seeingOthersPass
    });

    const data = res.data;
    console.log(data);
    switch (data?.param) {
      case 'D':
        this.passDetails(this.item);
        break;
      case 'M':
        this.getDirection(this.item);
        break;
      case 'R':
        // show recepients
        this.showRecepients(this.item);
        break;
      case 'T':
        // retract pass
        this.deletePass(this.item);
        break;
      case 'H':
        // show history
        this.showScanHistory(this.item);
        break;
      case 'E':
        // edit pass
        this.updatePass(this.item);
        break;

    }

  }


}

import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PassItemComponent } from './pass-item.component';
import { PassItemOptionsComponent } from './pass-item-options/pass-item-options.component';

@NgModule({
  declarations: [PassItemComponent, PassItemOptionsComponent],
  imports: [CommonModule, IonicModule, FormsModule, ReactiveFormsModule],
  exports: [PassItemComponent],
  providers: [Location],
})
export class PassItemComponentModule {}

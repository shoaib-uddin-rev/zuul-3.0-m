import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-pass-item-options',
  templateUrl: './pass-item-options.component.html',
  styleUrls: ['./pass-item-options.component.scss'],
})
export class PassItemOptionsComponent implements OnInit {

  @Input() page: 'active';
  @Input() item: any;
  @Input() type: any;
  @Input() seeingOthersPass = false;

  constructor(public popoverCtrl: PopoverController) { }

  ngOnInit() {}

  close(param) {
    this.popoverCtrl.dismiss({item: this.item, param });
  }

}

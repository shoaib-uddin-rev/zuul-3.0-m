import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsNotificationItemComponent } from './components-notification-item.component';

@NgModule({
  declarations: [ComponentsNotificationItemComponent],
  imports: [CommonModule, IonicModule, FormsModule, ReactiveFormsModule],
  exports: [ComponentsNotificationItemComponent],
})
export class ComponentsNotificationItemComponentModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { ContactpopoverPageComponent } from '../contactpopover-page/contactpopover-page.component';
@Component({
  selector: 'app-components-notification-item',
  templateUrl: './components-notification-item.component.html',
  styleUrls: ['./components-notification-item.component.scss'],
})
export class ComponentsNotificationItemComponent extends BasePage implements OnInit {
  @Input() itm: any;
  @Output() removeNotf: EventEmitter<any> = new EventEmitter<any>();
  @Output() rejectNotf: EventEmitter<any> = new EventEmitter<any>();
  @Output() createPass: EventEmitter<any> = new EventEmitter<any>();
  @Output() showPass: EventEmitter<any> = new EventEmitter<any>();
  @Output() markasread: EventEmitter<any> = new EventEmitter<any>();
  text: string;
  public show = false;
  countdown;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    if (!this.itm.status) {
      this.itm.status = 0;
    }

    if (this.itm.expire_at != null && this.itm.anonymous == 1) {
      this.countdown = this.itm.expire_at;
      // // console.log(this.itm.expire_at);
      const intervel = setInterval(() => {
        this.countdown--;
        if (this.countdown < 1) {
          clearInterval(intervel);
          if (this.itm.type == 0) {
            this.rejectNotf.emit(this.itm);
          } else {
            this.removeNotf.emit(this.itm);
          }
        }
      }, 1000);
    }

    // setTimeout( () => {
    //   this.markasread.emit(this.itm['id']);
    // }, 2000);
  }

  // convertSecondstoTime(giventime) {

  //   var nd = giventime.replace(' ', 'T');
  //   // console.log(nd);
  //   var t1 = new Date(nd);;
  //   var t2 = new Date();
  //   // console.log(t1, t2);
  //   var dif = t1.getTime() - t2.getTime();
  //   // console.log(dif);

  //   var Seconds_from_T1_to_T2 = dif / 1000;
  //   return Math.round(Seconds_from_T1_to_T2);

  // }

  convertSecondstoTime(given_seconds) {
    const dateObj = new Date(given_seconds * 1000);
    const minutes = dateObj.getUTCMinutes();
    const seconds = dateObj.getSeconds();

    const timeString =
      minutes.toString().padStart(2, '0') +
      ':' +
      seconds.toString().padStart(2, '0');
    return timeString;
  }

  async presentPopover($event, item) {

    const res = await this.popover.present(ContactpopoverPageComponent, $event, { pid: item, flag: 'NOF' });

    const data = res['data'];

    if (data == null) {
      return;
    }
    console.log(data);
    console.log(item);

    switch (data.param) {
      case 'A':
        this.createPass.emit();
        break;
      case 'R':
        this.rejectNotf.emit(item);
        break;
      case 'D':
        this.removeNotf.emit(item);
        break;
      case 'V':
        this.showPass.emit(item);
        break;
    }

  }
}

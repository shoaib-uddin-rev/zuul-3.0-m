import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-announcments-item',
  templateUrl: './announcments-item.component.html',
  styleUrls: ['./announcments-item.component.scss'],
})
export class AnnouncmentsItemComponent extends BasePage implements OnInit {

  @Input('item') item: any;
  @Output() annCheck: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    injector: Injector
  ) {
    super(injector)
  }

  ngOnInit() { }

  emit(item) {
    this.annCheck.emit(item);
  }
  
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PassformComponent } from './passform.component';

describe('PassformComponent', () => {
  let component: PassformComponent;
  let fixture: ComponentFixture<PassformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassformComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PassformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

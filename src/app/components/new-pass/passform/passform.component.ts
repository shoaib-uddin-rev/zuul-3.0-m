import { Component, Injector, OnInit, ViewChild, Input, AfterViewInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { IonDatetime } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { PassServiceService } from 'src/app/services/pass-service.service';
import { GmapComponent } from '../../gmap/gmap.component';
import { EventsComponent } from '../events/events.component';

@Component({
  selector: 'app-passform',
  templateUrl: './passform.component.html',
  styleUrls: ['./passform.component.scss'],
})
export class PassformComponent extends BasePage implements OnInit, AfterViewInit {
  @Input() edit = false;
  @ViewChild('enddatetime') sTime: IonDatetime;
  uitem: any = {};
  @Input()
  get item(): any {
    return this.uitem;
  }
  set item(item: any) {
    this.uitem = item;
  }

  user: any;
  custom_date_lbl = 'Custom';

  aForm: FormGroup;
  submitAttempt = false;
  dateNowIsoString;
  dateFutureIsoString;
  selectedEventName: any = {
    id: 14,
    name: 'Event name'
  };
  processing = false;

  constructor(injector: Injector, public passservice: PassServiceService) {
    super(injector);
    this.setupForm();
  }


  nextAvailable() {
    return (
      !this.aForm.controls.pass_date.valid ||
      !this.aForm.controls.pass_validity.valid ||
      !this.aForm.controls.pass_type.valid ||
      !this.aForm.controls.description.valid
    );
  }

  ngOnInit() {
    this.events.subscribe('validate:firstSlide', this.validateForm.bind(this));
  }

  async setupForm() {

    // this.utility.showLoader();
    const tempdate = new Date().toISOString();
    // const myDate = new Date(
    //   tempdate.getTime() - tempdate.getTimezoneOffset() * 60000
    // ).toISOString();

    // var re = /\S+@\S+\.\S+/;
    this.aForm = this.formBuilder.group({
      pass_date: [tempdate, Validators.compose([Validators.required])],
      pass_validity: ['24', Validators.compose([Validators.required])],
      pass_type: ['one', Validators.compose([Validators.required])],
      visitor_type: ['friends_family', Validators.compose([Validators.required])],
      description: ['Default Location not found', Validators.compose([Validators.required]),],
      event_id: ['', Validators.compose([Validators.required])],
      pass_image: [''],
      event_name: [''],
      mycontacts: ['contacts'],
      selected_contacts: [[]],
      lat: ['', Validators.compose([Validators.required])],
      lng: ['', Validators.compose([Validators.required])],
      searchTerm: [''],
      vendor_id: [''],
      vendor_name: [''],
      pass_end_date: [''],
    });

    this.user = await this.sqlite.getActiveUser();

    await this.getUserEvents();
    console.log("Here is the item",this.item);
    if (this.item) {
      this.aForm.controls.pass_date.setValue(this.item.pass_start_date_with_time);
      const pvn = this.utility.getOnlyDigits(this.item.pass_validity);
      console.log(pvn);
      // alert(pvn);
      this.aForm.controls.pass_validity.setValue(pvn);
      this.aForm.controls.pass_type.setValue(this.item.pass_type);
      this.aForm.controls.visitor_type.setValue(this.item.visitor_type);
      this.aForm.controls.description.setValue(this.item.description);
      this.aForm.controls.event_id.setValue(this.item.event_id);
      this.aForm.controls.pass_image.setValue(this.item.pass_image);
      this.aForm.controls.event_name.setValue(this.item.event_name);
      this.aForm.controls.mycontacts.setValue(this.item.mycontacts);
      this.aForm.controls.selected_contacts.setValue(this.item.selected_contacts);
      this.aForm.controls.lat.setValue(this.item.lat);
      this.aForm.controls.lng.setValue(this.item.lng);
      this.aForm.controls.searchTerm.setValue(this.item.searchTerm);
      this.aForm.controls.vendor_id.setValue(this.item.vendor_id);
      this.aForm.controls.vendor_name.setValue(this.item.vendor_name);
      this.aForm.controls.pass_end_date.setValue(this.item.pass_date);

    } else {

      const address = this.utility.parseAddressFromProfile(this.user);
      this.aForm.controls.description.setValue(address);
      const coords = await this.initializeMapBeforeSetCoordinates(address);
      this.aForm.controls.lat.setValue(coords["lat"]);
      this.aForm.controls.lng.setValue(coords["lng"]);

    }

    console.log(this.user);
    await this.setFutureDate();

  }

  ngAfterViewInit() {
    console.log(this.item.selected_contacts);
    if (this.item.selected_contacts.length > 0) {
      this.aForm.controls.selected_contacts.setValue(this.item.selected_contacts);
    }
  }



  initializeMapBeforeSetCoordinates(address) {

    return new Promise(async resolve => {

      this.utility.getCoordsForGeoAddress(address)
        .then(coords => {
          console.log(coords);
          resolve(coords);
        }, err => {
          console.log(err);
          this.utility.presentFailureToast('Destination Not Found for given address');
          console.log(err);
          resolve(null);
        });

    });

  }

  getUserEvents(): Promise<any> {

    return new Promise(async resolve => {
      const data = await this.passservice.getSelectedUserEvents(this.user.id);
      if (data != null) {
        this.selectedEventName = data;
      }

      this.aForm.controls.event_id.setValue(this.selectedEventName.id);
      this.aForm.controls.event_name.setValue(this.selectedEventName.name);
      resolve(true);
    });



  }

  setFutureDate(): Promise<any> {
    const self = this;
    return new Promise(async resolve => {

      let pd = this.aForm.controls.pass_date.value;
      let v = this.aForm.controls.pass_validity.value
      // if(this.item){
      //   pd = this.item.pass_date;
      // }

      const res = await this.passservice
        .getFutureDateISO(pd, v);
      const myDate = new Date(res as string).toISOString();
      self.aForm.controls.pass_end_date.setValue(myDate);
      resolve(true);
    });
  }

  async openEventSelection() {

    const obj = { items: null };
    const res = await this.modals.present(EventsComponent, obj);
    const data = res.data;

    console.log(res);
    if (res.data !== 'A') {
      this.sqlite.setActiveUserEvent(this.user.id, data);
      this.selectedEventName = data;
      this.aForm.controls.event_id.setValue(this.selectedEventName.id);
      this.aForm.controls.event_name.setValue(this.selectedEventName.name);
    }

  }

  async openLocation() {
    console.log("Here for Locations 1st");
    const address = this.aForm.controls.description.value;
    const res = await this.modals.present(GmapComponent, { myAddress: address });
    const data = res.data;
    console.log(data);
    if (data["data"] !== "A") {
      this.aForm.controls.lat.setValue(data.lat);
      this.aForm.controls.lng.setValue(data.lng);
      this.aForm.controls.description.setValue(data.address);
    }
  }

  setEndTimeAfterValidityChange(ev) {
    const v = this.aForm.controls.pass_date.value;
    if (v == null || v == '') {
      return;
    };


    const pass_validity = this.aForm.controls.pass_validity.value;


    if (pass_validity == 'xxx') {

      const self = this;
      this.sTime.open();

    } else {
      this.setFutureDate();
    }


  }

  validateForm() {


    console.log(this.aForm);
    const obj = {
      res: this.aForm.valid,
      data: this.aForm.value
    };
    setTimeout(() => {
      this.events.publish('firstSlide:isValidated', obj);
    }, 500);
  }
}

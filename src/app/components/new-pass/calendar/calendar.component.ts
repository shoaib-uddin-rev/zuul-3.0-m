import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent extends BasePage implements OnInit {

  @Input() heading1 = 'Pass Has Been Sent';
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {

  }

  addareminder(){
    this.events.publish("newpass:addareminder", this.addareminder.bind(this));
  }

  finishregister(){
    // this.events.publish('newpass:closemodal');
  }

}

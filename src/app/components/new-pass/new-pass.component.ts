import { SelectorFlags } from '@angular/compiler/src/core';
import { AfterViewInit, Component, Injector, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { IonContent, IonSlide, IonSlides } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { PassServiceService } from 'src/app/services/pass-service.service';

@Component({
  selector: 'app-new-pass',
  templateUrl: './new-pass.component.html',
  styleUrls: ['./new-pass.component.scss'],
})
export class NewPassComponent extends BasePage implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('slides', { static: true }) slides: IonSlides;
  @ViewChild('content', { static: true }) content: IonContent;
  // @Input() item: any = null;
  aForm: FormGroup;
  step = 0;
  edit = false;

  titem: any;
  get item(): any {
    return this.titem;
  }

  @Input() set item(value: any) {
    this.titem = value;
    // console.log(value)
  };

  // user_events: any[];
  // phone_contacts: any[]
  // phone_contacts_o: any[]
  // group_contacts: any[];
  // selected_contacts: any = [];
  // date_of_pass_mdy;
  // end_date_of_pass_mdy;
  // selected_event_name: string = "";
  // rap: boolean = false;
  // rapitem: any = null;
  // isGroup: boolean = false;
  // isContact: boolean = true;
  reminderObject: any;
  loading = false;

  constructor(
    injector: Injector,
    public passService: PassServiceService
  ) {
    super(injector);
    // this.item = null;
    this.setupForm();
    this.events.subscribe('firstSlide:isValidated', this.firstSlideisValidated.bind(this));
    this.events.subscribe('secondSlide:isValidated', this.secondSlideisValidated.bind(this));
    this.events.subscribe('newpass:addareminder', this.addareminder.bind(this));
  }

  ngOnInit(): void { }

  ngAfterViewInit() {
    if (this.item?.selected_contacts.length > 0) {
      this.aForm.controls.selected_contacts.setValue(this.item.selected_contacts);
    }

  }

  ngOnDestroy(): void {
    this.events.unsubscribe('firstSlide:isValidated');
    this.events.unsubscribe('secondSlide:isValidated');
    this.events.unsubscribe('newpass:addareminder');
  }

  ionViewDidEnter() {
    this.moveSlide(0);
  }

  async setupForm() {

    this.aForm = this.formBuilder.group({
      pass_date: ['', Validators.compose([Validators.required])],
      pass_validity: ['24', Validators.compose([Validators.required])],
      pass_type: ['one', Validators.compose([Validators.required])],
      visitor_type: ['friends_family', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      event_id: ['', Validators.compose([Validators.required])],
      pass_image: ['', Validators.compose([Validators.required])],
      event_name: [''],
      mycontacts: ['contacts'],
      selected_contacts: [],
      lat: [''],
      lng: [''],
      searchTerm: ['']
    });

    // const profile = await this.users.getUser();
    // console.log(profile);
    // if(profile){
    //   const address = this.utility.parseAddressFromProfile(profile);
    //   this.aForm.controls['description'].setValue(address);

    // }


  }

  async firstSlideisValidated(data) {
    const self = this;
    console.log(data);
    if (data.res) {
      console.log(self.item);

      self.item = { ...self.item, ...data.data };
      if (self.item.pass_type === 'self' || self.item.id || self.item.group_id || self.item.from_contact) {
        this.addNewPass();
      } else {
        self.item = data.data;
        this.moveSlide(1);
      }

    } else {
      self.utility.presentFailureToast('Please Fill All Fields');
    }
  }

  async secondSlideisValidated(data) {
    var self = this;
    if (data.res) {
      self.item.selected_contacts = data.data;
      console.log(self.item);
      self.addNewPass();
    } else {
      self.utility.presentFailureToast('Please Select Contacts');
    }
  }

  async goToNextSlide(slides) {

    const self = this;
    const slideNum = await this.slides.getActiveIndex();
    console.log(slideNum);

    if (slideNum === 0) {
      this.events.publish('validate:firstSlide');
    }

    if (slideNum === 1) {
      this.events.publish('validate:secondSlide');
    }

    if (slideNum === 2) {
      this.modals.dismiss({ data: this.item });
    }



  }

  addareminder() {
    const detailObj = {
      event_name: this.item.event_name,
      description: this.item.description,
      notes: "",
      startDate: Date.parse(this.item.pass_date),
      endDate: Date.parse(this.item.pass_end_date)
    };
    console.log(detailObj);
    this.utility.addaremondertodate(detailObj);
  }

  async addNewPass() {

    console.log(this.item);
    let res = null;
    if (this.item.id) {
      res = await this.passService.editPass(this.item);
      console.log(res);
    } else {
      res = await this.passService.createPass(this.item);
      console.log(res);
    }

    if (res) {
      this.moveSlide(2);
    }


  }


  close() {
    this.modals.dismiss();
  }

  async goStep($event) {
    const num = await this.slides.getActiveIndex();
    if ($event < num && num !== 2) {
      this.step = $event;
      this.moveSlide($event);
    }


  }

  moveSlide(num) {
    this.content.scrollToTop();
    this.slides.lockSwipes(false);
    this.slides.slideTo(num, 500);
    this.slides.lockSwipes(true);
    this.step = num;
  }
}

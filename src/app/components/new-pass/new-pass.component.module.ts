import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { PassformComponent } from './passform/passform.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewPassComponent } from './new-pass.component';
import { WizardComponent } from './wizard/wizard.component';
import { EventsComponent } from './events/events.component';
import { AddComponent } from './events/add/add.component';
import { ContactsComponent } from './contacts/contacts.component';
import { CalendarComponent } from './calendar/calendar.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    NewPassComponent,
    PassformComponent,
    WizardComponent,
    EventsComponent,
    AddComponent,
    ContactsComponent,
    CalendarComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule
  ],
  exports: [NewPassComponent, PassformComponent],
  providers: [Location],
})
export class NewPassComponentModule { }

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { ContactpopoverPageComponent } from '../../contactpopover-page/contactpopover-page.component';
import { AddComponent } from './add/add.component';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
})
export class EventsComponent extends BasePage implements OnInit {

  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  page = 1;
  plist: any = [];
  user;
  search = '';

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.getUserid().then(v => {
      this.getUserEvents();
    });
  }

  async getUserid() {
    return new Promise(async resolve => {
      this.user = await this.sqlite.getActiveUser();
      resolve(true);
    });
  }

  filter($event) {
    this.getUserEvents();
  }

  async getUserEvents() {

    return new Promise(resolve => {

      if (this.page === -1) {
        resolve(this.events);
        return;
      }

      const obj = {
        search: this.search,
        page: this.page
      };
      this.network.getUserEvents(obj).then((res: any) => {

        console.log(res);

        if (res.list.length === 0) {
          this.page = -1;
          resolve(this.events);
          return;
        }

        if (this.page === 1) {
          this.plist = res.list;
        } else {
          this.plist = [...this.plist, ...res.list];
        }

        this.page = this.page + 1;

        resolve(this.events);

      });
    });


  }

  resetSearch() {
    this.getUserEvents();
  }

  doRefresh($event) {
    this.getUserEvents();
  }

  loadMore($event) {
    this.getUserEvents().then(v => {
      $event.target.complete();
    });
  }


  close(res) {
    this.modals.dismiss(res);
  }

  async plusHeaderButton($event, item) {
    // CreateGroupPage as modal
    $event.stopPropagation();
    const obj = { event: item };
    console.log(obj);
    const res = await this.modals.present(AddComponent, obj);
    const data = res.data;
    if (data.data !== 'A') {
      this.page = 1;
      this.getUserEvents().then(v => {
        $event.complete();
      });
    }

  }

  async presentPopover($event, item) {

    $event.stopPropagation();
    const res = await this.popover.present(ContactpopoverPageComponent, $event, {
      id: item,
      flag: 'E'
    });

    const data = res.data;

    if (data == null) {
      return;
    }

    console.log(item);
    switch (data['param']) {
      case 'S':
        this.setPassEvent(item);
        break;
      case 'E':
        this.plusHeaderButton($event, item);
        break;
      case 'D':
        this.deleteEventFromList(item);
        break;
    }
  }

  async setPassEvent(item) {
    console.log(item);
    this.modals.dismiss(item);
  }

  async deleteEventFromList(item) {
    const flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Are You Sure?',
      'This will hide this Event from your list but passes will not be effected');

    console.log(flag, item.id);
    if (flag === true) {
      console.log(flag, item.id);

      this.network.removeEventFromList(item.id).then((res: any) => {
        console.log(res);

        const elementPos = this.plist.map(x => x.id).indexOf(item.id);
        this.plist.splice(elementPos, 1);

      }, err => { });
    }
  }



}

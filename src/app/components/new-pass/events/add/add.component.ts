import { AfterViewInit, Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddComponent extends BasePage implements OnInit, AfterViewInit {

  @Input() event;
  aForm: FormGroup;
  submitAttempt = false;


  constructor(injector: Injector) {
    super(injector);
    this.setupForm();
  }

  ngAfterViewInit(): void {

    console.log(this.event);
    if(this.event){
      this.aForm.controls.name.setValue(this.event.name);
      this.aForm.controls.description.setValue(this.event.description);
      // this.aForm.controls.event_description.setValue(this.event.event_description);
    }
  }



  ngOnInit() {}

  setupForm(){

    this.aForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */ ],
      description: ['']
    });

  }

  create(){

    const invalidName = !this.aForm.controls.name.valid;

    if(invalidName){
      this.utility.presentFailureToast('event name is required');
      return;
    }

    this.submitAttempt = true;
    const formdata = this.aForm.value;
    this.network.addEventToList(formdata).then((res: any) => {
      //this.utility.presentSuccessToast(res['message']);
      // this.sqlite.setEventInDatabase(res.event);
      this.closeModal({data: res.result});

    }, err => {});



  }

  update(){

    const invalidName = !this.aForm.controls.name.valid;

    if(invalidName){
      this.utility.presentFailureToast('group name is required');
      return;
    }

    this.submitAttempt = true;
    const formdata = this.aForm.value;

    formdata.id = this.event.id;
    console.log(this.event);
    this.network.editEventToList(formdata.id, formdata).then((res: any) =>{
    //   //this.utility.presentSuccessToast(res['message']);
      this.closeModal({data: res.result});
    }, err => {});

  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

}

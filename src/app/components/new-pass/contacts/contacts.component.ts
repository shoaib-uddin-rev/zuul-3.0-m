import { AfterViewInit, Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss'],
})
export class ContactsComponent extends BasePage implements OnInit {

  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  @Input() singleSelection = false;
  @Output() next: EventEmitter<any> = new EventEmitter<any>();

  search = '';
  selectedContacts = [];
  page = 1;
  plist: any[] = [];

  constructor(
    injector: Injector,
    public storedContactsService: StoredContactsService
  ) {
    super(injector);
    this.loadData();
  }

  ngOnInit() {
    this.events.subscribe("validate:secondSlide", this.validate.bind(this));
  }

  async loadData() {
    this.plist = await this.storedContactsService.getMyContacts(this.search, this.page);
  }

  async loadMore($event) {
    await this.loadData();
  }

  filter($event) {
    this.loadData();

  }

  reset() {
    this.loadData();
  }

  doRefresh($event) {
    this.page = 1;
    this.loadData().then(v => {
      $event.target.complete();
    });
  }

  isSelectedContact(id) {
    return this.selectedContacts.some(el => el.id === id);
  }

  // addSelectedContact($event) {

  //   console.log($event);

  //   const check = $event.checked;
  //   const id = $event.id;
  //   console.log(check, id);
  //   if (check == true) {

  //     if (this.singleSelection) {
  //       this.selectedContacts = [];
  //       this.selectedContacts.push(id);
  //       // this.events.publish('contact-list:check-update', {id: id, includes: this.selectedContacts.includes(id) })
  //     } else {
  //       if (!this.selectedContacts.includes(id)) {
  //         this.selectedContacts.push(id);
  //       }
  //     }




  //   } else {

  //     const index = this.selectedContacts.indexOf(id);

  //     if (index > -1) {
  //       this.selectedContacts.splice(index, 1);
  //     }

  //   }

  //   console.log(this.selectedContacts);

  // }

  validate() {
    console.log(this.selectedContacts);
    this.selectedContacts = this.plist.filter( x => x.checked == true).map( x => x.id);


    let obj = {
      res: this.selectedContacts.length > 0,
      data: this.selectedContacts,
    }
    setTimeout(() => {
        this.events.publish("secondSlide:isValidated", obj)
    }, 1000);
    
    
  }

}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardComponent implements OnInit {

  @Input() step: any;
  @Output() goStep: EventEmitter<any> = new EventEmitter<any>();
  
  constructor() { }

  ngOnInit() {}

}

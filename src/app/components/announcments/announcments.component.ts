import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-announcments',
  templateUrl: './announcments.component.html',
  styleUrls: ['./announcments.component.scss'],
})
export class AnnouncmentsComponent extends BasePage implements OnInit {

  @ViewChild('slides', { static: true }) slides: IonSlides;
  getNotified: boolean = false;
  plist: any[] = [];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    console.log(this.plist);
  }

  ngAfterViewInit() {
    this.slides.lockSwipes(true);
  }

  annCheck(item) {
    console.log(item);
    this.network.markReadNotification({ notification_ids: [ item.id ] }).then(async res => {
      const flag = await this.slides.isEnd();
      console.log(flag);
      if (!flag) {
        this.slides.lockSwipes(false);
        this.slides.slideNext();
        this.slides.lockSwipes(true);
        // this.removeItem(item);
      } else {
        this.modals.dismiss();
      }
    }, err => { })


  }

  removeItem(item) {
    let index = this.plist.indexOf(item);
    if (index > -1) {
      this.plist.splice(index, 1);
    }
  }

  dismiss() {
    this.modals.dismiss();
  }
}

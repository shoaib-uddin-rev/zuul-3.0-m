import { Component } from '@angular/core';
import { SqliteService } from './services/sqlite.service';
import { UserService } from './services/user.service';
import { ModalController, Platform } from '@ionic/angular';
import { EventsService } from './services/events.service';
import { NavService } from './services/nav.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AudioService } from './services/audio.service';
import { NetworkService } from './services/network.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from './services/utility.service';
import { SplashScreen } from '@capacitor/splash-screen';

import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
} from '@capacitor/push-notifications';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  user_role_id = -1;
  canBeResident = false;
  canShowSettings = true;
  isEmailVerificationPending = false;
  isModalOpen;

  pages: any[] = [
    { title: 'Become Resident', component: 'CodeVerificationPage' },
    { title: 'Manage Family', component: 'MyfamilymembersPage' },
    { title: 'Settings', component: 'SettingsPage' },
    { title: 'Edit Profile', component: 'RegistrationPage' },
    { title: 'Logout', method: 'logout' },
    { title: 'Activity Logs', component: 'ActivityLogsPage' },
    { title: 'Zuul Key', component: 'ZKeyPage' },
    { title: 'Manage Vehicles', component: 'VManageVehiclesPage' },
  ];

  constructor(
    public sqlite: SqliteService,
    public platform: Platform,
    public events: EventsService,
    public nav: NavService,
    public statusBar: StatusBar,
    public audio: AudioService,
    public network: NetworkService,
    public users: UserService,
    public activateRoute: ActivatedRoute,
    private router: Router,
    private modalController: ModalController,
    public utility: UtilityService
  ) {
    platform.ready().then(() => {
      // menuCtrl.enable(false, 'main');
      if (platform.is('cordova') || platform.is('ios') || platform.is('android')) {
        this.initializeApp();
      }
    });


    this.platform.backButton.subscribeWithPriority(1, () => {
      console.log('hit back btn');
    });

    this.router.events.subscribe(async () => {

      // if (router.url.toString() === "/tabs/home" && isModalOpened) this.modalController.dismiss();
    });

    document.addEventListener('backbutton', (event) => {
      event.preventDefault();
      event.stopPropagation();
      const url = this.router.url;
      console.log(url);
      this.createBackRoutingLogics(url);

    }, false);
  }

  async createBackRoutingLogics(url){

    if(url.includes('login')
      || url.includes('signup')
      || url.includes('dashboard')
      || url.includes('tutorial')) {

        this.utility.hideLoader();

        const isModalOpen = await this.modalController.getTop();
        console.log(isModalOpen);
        if(isModalOpen){
          this.modalController.dismiss({data: 'A'});
        }else{
          this.exitApp();
        }

    }else{
      if(this.isModalOpen){

      }
    }




  }

  exitApp(){
    navigator['app'].exitApp();
  }

  initializeApp() {

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      SplashScreen.hide();
      this.audio.preload();
      // this.sqlite.initialize();
      this.setupFMC();
      // this.getUser();
    });

  }

  // async getUser() {
  //   // const user = await this.users.getUser();
  //   if (user) {
  //     this.user_role_id = user['role_id'];
  //     // this.canBeResident = (parseInt(user.can_user_become_resident, 10) == 1);
  //     // this.enableMenu();
  //   }
  // }

  // enableMenu() {
  //   console.log('enable menu');
  //   this.menuCtrl.enable(true, 'main');
  // }

  // setupFMC() {

  //   console.log("setup fcm");

  //   this.fcm.subscribeToTopic('all');
  //   this.fcm.onNotification().subscribe(data => {
  //     console.log(data);
  //     if (!data.wasTapped) {
  //       this.audio.play('');
  //       this.events.publish('dashboard:refreshpage');
  //       if (data.showalert != null) {
  //         this.events.publish('user:shownotificationalert', data);
  //       } else {
  //         this.events.publish('user:shownotification', data);
  //       }
  //     };
  //   });
  //   this.fcm.onTokenRefresh().subscribe(token => {
  //     this.sqlite.setFcmToken(token);
  //     this.events.publish('user:settokentoserver');
  //   });

  // }

  setupFMC(){

    // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    PushNotifications.requestPermissions().then(result => {
      if (result.receive === 'granted') {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration',
      (token: Token) => {
        // alert('Push registration success, token: ' + token.value);
        this.sqlite.setFcmToken(token);
        this.events.publish('user:settokentoserver');
      }
    );

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError',
      (error: any) => {
        console.error('Error on registration: ' + JSON.stringify(error));
      }
    );

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        // alert('Push received: ' + JSON.stringify(notification));
        this.audio.play('');
        this.events.publish('dashboard:notificationReceived');
        this.events.publish('dashboard:refreshpage');
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      (notification: ActionPerformed) => {
        // alert('Push action performed: ' + JSON.stringify(notification));
        this.events.publish('dashboard:refreshpage');
      }
    );

  }

  // openPage(page) {


  //   // Reset the content nav to have just this page
  //   // we wouldn't want the back button to show in this scenario
  //   // this.menuCtrl.close('main');
  //   if (page.hasOwnProperty('component')) {

  //     if (page.component == 'VManageVehiclesPage') {
  //       this.events.publish('dashboard:carselection');
  //       return;
  //     }

  //     if (page.component == 'RegistrationPage') {
  //       this.events.publish('dashboard:updateprofile');
  //       return;
  //     }

  //     this.nav.push(page.component, { revisit: true });
  //     return;

  //   }
  //   if (page.hasOwnProperty('method')) {
  //     this[page.method]();
  //   }


  // }




}

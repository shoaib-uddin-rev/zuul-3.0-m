import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage extends BasePage implements OnInit, OnDestroy {

  tab1Root = 'contacts';
  tab2Root = 'groups';
  tab3Root = 'favorites';

  constructor(injector: Injector) {
    super(injector);

    this.events.subscribe('contacts:popup', this.goBack.bind(this));
  }

  ngOnDestroy(): void {
    this.events.unsubscribe('contacts:popup');
  }

  ngOnInit() {
  }

  goBack(){
    // this.nav.pop();
    // this.modals.dismiss({data: 'A'});
    this.nav.push('pages/dashboard');
  }

}

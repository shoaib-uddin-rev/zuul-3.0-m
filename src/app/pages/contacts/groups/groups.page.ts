import { Component, Injector, OnInit } from '@angular/core';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../../base-page/base-page';
import { AddPage } from './add/add.page';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.page.html',
  styleUrls: ['./groups.page.scss'],
})
export class GroupsPage extends BasePage implements OnInit {

  plist: any[] = [];
  page = 1;
  search = '';

  constructor(
    injector: Injector,
    public storedContactsService: StoredContactsService
  ) {
    super(injector);
    this.events.subscribe('groups:refresh', () => {
      this.page = 1;
      this.search = '';
      this.loadData();
    });
  }

  ngOnInit() {
    this.loadData();
  }


  goBack() {
    this.events.publish('contacts:popup', {});
  }

  async loadData() {
    if (this.page == 1) {
      this.plist = await this.storedContactsService.getMyGroups(this.search, this.page);
    } else {
      let list = await this.storedContactsService.getMyGroups(this.search, this.page);
      this.plist = [... this.plist , ...list];
    }
  }

  showGroupCOntacts(item) {
    console.log(item);
    let extras = {
      queryParams: { group_id: item['id'], group_name: item['group_name'], description: item['description'] }
    }
    this.nav.navigateTo('pages/contacts/groups/group-contacts-list', extras);
  }

  async doRefresh($event) {
    this.page = 1;
    await this.loadData();
    $event.target.complete();
  }

  async loadMore($event) {
    this.page++;
    await this.loadData();
    $event.target.complete();
  }

  async filter($event) {
    console.log($event.target.value);
    this.search = $event.target.value;
    await this.loadData();
  }

  async reset() {
    this.search = "";
    await this.loadData();
  }

  async add() {
    const res = await this.modals.present(AddPage);
    console.log(res);
    this.page = 1;
    await this.loadData();
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupContactsListPage } from './group-contacts-list.page';

const routes: Routes = [
  {
    path: '',
    component: GroupContactsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupContactsListPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GroupContactsListPageRoutingModule } from './group-contacts-list-routing.module';

import { GroupContactsListPage } from './group-contacts-list.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupContactsListPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [GroupContactsListPage]
})
export class GroupContactsListPageModule {}

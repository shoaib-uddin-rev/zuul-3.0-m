import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';

@Component({
  selector: 'app-group-contacts-list',
  templateUrl: './group-contacts-list.page.html',
  styleUrls: ['./group-contacts-list.page.scss'],
})
export class GroupContactsListPage extends BasePage implements OnInit {

  group: any;
  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  search_value;
  search_on = false;

  public groupcontactsoffset = 0;
  public group_contact_list: any[] = [];

  constructor(injector: Injector, public storedcontacts: StoredContactsService) {
    super(injector);
    const id = this.nav.getQueryParams().group_id;
    const group_name = this.nav.getQueryParams().group_name;
    const description = this.nav.getQueryParams().description;
    const contact_list = this.nav.getQueryParams().contact_list;
    this.group = {
      id,
      group_name,
      description,
      contact_list
    };
  }

  ngOnInit() {
    this.getContactListByGroupId(null, 1, true, false);
  }

  setFocusOnSearch() {
    const self = this;
    this.search_on = true;
    setTimeout(() => {
      self.searchbar.setFocus();
    }, 500);

  }


  filterGlobal($event) {
    const val = this.search_value;
    this.getContactListByGroupId(val, this.groupcontactsoffset, false, false);
  }

  ionViewDidLoad() { }

  resetSearch() {
    this.getContactListByGroupId(null, 0, true, false);
  }

  async deleteContactFromGroup(item) {
    const flag = await this.removeContactFromGroup();
    if (flag) {
      const elementPos = this.group_contact_list.map(function (x) { return x.id; }).indexOf(item.id);
      this.group_contact_list.splice(elementPos, 1);
      const obj = {
        group_id: this.group.id,
        contactIds: [item.id]
      };
      this.network.removeFromGroup(obj).then();
    }

  }

  async removeContactFromGroup() {
    return this.utility.presentConfirm('Agree', 'Disagree', 'Confirm Delete', 'Do you want to delete this contact from group?');
  }

  async getContactListByGroupId(search = null, page, loader = true, is_concat = true) {
    return new Promise(resolve => {
      const obj = {
        search,
        page,
        contact_group_id: this.group.id
      }

      this.network.getGroupContactList(obj).then(res => {

        if (is_concat) {
          this.group_contact_list = this.group_contact_list.concat(res.list);
        } else {
          this.group_contact_list = res.list;
        }

        resolve(res);
      });

    });
  }

  doRefresh($event) {
    this.groupcontactsoffset = 0;
    this.getContactListByGroupId(this.search_value, this.groupcontactsoffset, false, false).then(v => {
      $event.target.complete();
    });
  }

  loadMoreContacts($event) {
    if (this.groupcontactsoffset == -1) {
      $event.complete();
    } else {
      this.getContactListByGroupId(this.search_value, this.groupcontactsoffset, false).then(v => {
        $event.complete();
      });
    }
  }

  async presentGroupPopover($event, item) {
    // var r = item;
    var self = this;
    const res = await this.popover.present(ContactpopoverPageComponent, $event, {
      pid: item, flag: 'G', count: this.group_contact_list.length
    });

    const data = res.data;

    if (data == null) {
      return;
    }

    switch (data.param) {

      case 'G':

        this.storedcontacts.addContactsToSelectedGroupByItem(this.group.id, false).then(() => {
          this.groupcontactsoffset = 0;
          this.getContactListByGroupId('', this.groupcontactsoffset, false, false);
        });
        break;
      case 'SP':
        if (!this.group_contact_list || this.group_contact_list.length == 0) {
          this.utility.showAlert('At least one member should be in group');
        } else {
          this.storedcontacts.fetchEventsToSendPassToGroup(this.group);
        }

        break;
      case 'E':
        this.storedcontacts.editContactGroup(this.group).then(v => {
          if (v) {
            if (v['group']) {
              this.group = v['group'];
            }
          }
        });
        break;
      case 'D':

        const flag = await this.utility.presentConfirm('Sure', 'Cancel', 'Delete ' + this.group.group_name + '?');
        if (flag) {
          this.storedcontacts.deleteContactGroup(this.group).then(v => {
            this.events.publish('groups:refresh', {});
            this.nav.navigateTo('/contacts/groups');
          });
        }

        break;
    }

  }

}


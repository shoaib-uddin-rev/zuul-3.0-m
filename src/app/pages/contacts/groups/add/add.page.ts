import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  phone_contacts: any;
  return_data: any;
  storedData;
  it;
  it_group;
  user: any;
  page = 1;
  search = '';

  constructor(
    injector: Injector,
    public storedContactsService: StoredContactsService
  ) {
    super(injector);
    this.setupForm();
  }

  ngOnInit() {
  }

  close(res) {
    this.modals.dismiss(res);
  }

  ngAfterViewInit() {
    this.initialize();
    this.sqlite.getActiveUser().then(u => {
      this.user = u;
      // this.getMyContacts();
    });

  }

  initialize() {

    console.log(this.it);

    if (this.it) {

      this.it_group = this.it;
      this.aForm.controls.group_name.setValue(this.it_group.group_name);
      this.aForm.controls.description.setValue(this.it_group.description);

    }

  }
  async getMyContacts() {

    this.utility.getPhoneContacts()
      .then(async data => {
        // console.log(data);
        this.phone_contacts = await this.storedContactsService.getMyContacts(this.search, this.page);
      });
  }


  setupForm() {

    this.aForm = this.formBuilder.group({
      group_name: ['', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */],
      description: ['']
    });

  }

  create() {

    const in_name = !this.aForm.controls.group_name.valid;

    if (in_name) {
      this.utility.presentFailureToast('group name is required');
      return;
    }

    this.submitAttempt = true;
    const formdata = this.aForm.value;

    this.network.createContactGroup(formdata).then((res: any) => {
      // this.utility.presentSuccessToast(res['message'])
      this.close(res);

    }, err => { });

  }

  update() {

    const in_name = !this.aForm.controls.group_name.valid;

    if (in_name) {
      this.utility.presentFailureToast('group name is required');
      return;
    }

    this.submitAttempt = true;
    const formdata = this.aForm.value;

    console.log(formdata);

    // formdata["description"] = "description";

    this.network.updateContactGroup(this.it.id, formdata).then((res: any) => {
      this.close({ group: formdata });
    }, err => { });

  }

}

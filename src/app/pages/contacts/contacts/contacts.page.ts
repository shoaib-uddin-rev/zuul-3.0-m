import { Component, Injector, OnInit } from '@angular/core';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../../base-page/base-page';
import { AddContactsPage } from './add/add.page';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage extends BasePage implements OnInit {

  plist: any[] = [];
  page = 1;
  search = '';

  constructor(
    injector: Injector,
    public storedContactsService: StoredContactsService
  ) {
    super(injector);
  }

  ngOnInit() {
    this.loadData();
  }

  async loadData() {
    this.plist = await this.storedContactsService.getMyContacts(this.search, this.page);
  }

  async doRefresh($event) {
    this.page = 1;
    await this.loadData();
    $event.target.complete();
  }

  async loadMore($event) {
    await this.loadData();
    $event.target.complete();
  }

  goBack() {
    this.events.publish('contacts:popup', {});
  }

  async filter($event) {
    this.search = $event.target.value;
    await this.loadData();
  }

  reset() { }

  async add() {
    const res = await this.modals.present(AddContactsPage);
    console.log(res.data);
    const data = res.data;

    if (data.data !== 'A') {
      // refresh here
      this.page = 1;
      await this.loadData();
    }
  }

  async presentPopover($event, item) {
    const res = await this.popover.present(
      ContactpopoverPageComponent,
      $event,
      {
        pid: item,
        flag: 'C',
      },
      'interface-style'
    );

    const data = res.data;

    if (data == null) {
      return;
    }
    console.log(data);
    // var item = data.pid;

    switch (data.param) {
      case 'C':
        this.utility.dialMyPhone(item.formatted_phone_number);
        break;
      case 'G':
        item.contact_id = item.id;
        this.storedContactsService.createSelectedInContactGroupByItem([item]);
        break;
      case 'E':
        await this.storedContactsService.editContact(item);
        this.page = 1;
        this.loadData();
        break;
      case 'F':
        this.storedContactsService.addSelectedContactsToFavorite([item]);
        break;
      case 'D':
        this.storedContactsService.deleteContact(item);
        break;
      case 'SP':
        this.storedContactsService.fetchEventsToSendPassTo(item)
        break;
      case 'RP':
        this.storedContactsService.sendPassRequestToContact(item);
        break;
    }
  }

  async synContacts() {
    const res = await this.storedContactsService.syncMyContact();
    const data = res["data"];
    console.log(data);
    this.plist = [...data['list'], ...this.plist];
  }

  // async syncContactsFromApi() {
  //   this.utility.showLoader();
  //   await this.storedContactsService.syncContactsFromApi(false);
  //   this.utility.hideLoader();
  // }
}

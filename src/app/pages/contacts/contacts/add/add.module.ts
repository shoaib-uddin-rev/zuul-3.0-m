import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPageRoutingModule } from './add-routing.module';

import { AddContactsPage } from './add.page';
import { CountryCodeComponent } from 'src/app/components/country-code/country-code.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AddPageRoutingModule
  ],
  declarations: [AddContactsPage, CountryCodeComponent]
})
export class AddPageModule {}

import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { CountryCodeComponent } from 'src/app/components/country-code/country-code.component';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddContactsPage extends BasePage implements OnInit {
  @Input() showRelation = false;
  @Input() isUpdate = false;
  @Input() dial_code = {
    name: 'United States',
    dial_code: '+1',
    code: 'US',
    image: 'assets/imgs/flags/us.png',
  };
  aForm: FormGroup;
  submitAttempt = false;
  // canModify = false;
  // user: any;
  //phoneContacts;
  // @Input() phoneNumber;
  // @Input() name;

  btnText = 'Create';
  // user_id;
  contact_id: any;
  // formattedNumber;

  // has_house: boolean = false;
  // @Input() isFromRequestPassScreen: boolean = false;
  // temporary_duration: number = 6;

  constructor(injector: Injector) {
    super(injector);

    this.setupForm();
  }

  ngOnInit() {
    console.log(this.contact_id);
    if (this.contact_id) {
      this.isUpdate = true;
      this.btnText = "Update";
      this.getContactInfo(this.contact_id);
    } 

  }

  setupForm() {
    this.aForm = this.formBuilder.group({
      contact_name: [
        '',
        Validators.compose([
          Validators.required,
        ]) /*, VemailValidator.checkEmail */,
      ],
      // relationship: ['none', Validators.compose([Validators.required])],
      email: [''],
      phone_number: ['', Validators.compose([Validators.required])],
      // can_manage_family: [false, Validators.compose([Validators.required])],
      // can_send_passes: [false, Validators.compose([Validators.required])],
      // allow_parental_control: [false, Validators.compose([Validators.required])],
      is_temporary: [false, Validators.compose([Validators.required])],
      temporary_duration: ['6'],
      dial_code: ['+1'],
    });
  }

  getContactInfo(contact_id) {

    var formdata = { contact_id: contact_id };
    console.log("Here is the contact Id", contact_id);
    this.network.getSingleContactById(contact_id).then((res: any) => {

      console.log(res);
      this.contact_id = res['id'];

      var contact_profile = res;
      // this.user_id = contact_profile['user_id'];

      this.aForm.controls['contact_name'].setValue(contact_profile["contact_name"]);
      this.aForm.controls['email'].setValue(contact_profile["email"]);
      var _tel = this.utility.onkeyupFormatPhoneNumberRuntime(contact_profile["formatted_phone_number"]);
      this.aForm.controls['phone_number'].setValue(_tel);

      this.aForm.controls['dial_code'].setValue(contact_profile["dial_code"]);

      // this.setDialCode(contact_profile["dial_code"])

    });

  }

  setDialCode(code) {
    this.sqlite.getCountriesInDatabase(null, 0, true).then((res: any) => {
      var n: any = res;
      console.log(n);
      this.dial_code = n.find(x => x.dial_code == code)
      console.log(res);
    }, error => {
      console.log(error);
    })
  }
  close(res) {
    this.modals.dismiss(res);
  }

  create() {
    // add validations
    const invalidName =
      !this.aForm.controls.contact_name.valid ||
      !this.utility.isLastNameExist(this.aForm.controls.contact_name.value);
    let invalidPhone = !this.aForm.controls.phone_number.valid;
    const validPhoneNumber = this.utility.getOnlyDigits(
      this.aForm.controls.phone_number.value
    );
    // console.log(validPhoneNumber);
    if (validPhoneNumber.toString().length < 10) {
      invalidPhone = true;
    } else {
      invalidPhone = false;
    }

    if (invalidName) {
      this.utility.presentFailureToast('Please Enter Full Name');
      return;
    }

    if (invalidPhone) {
      this.utility.presentFailureToast('Please Enter Valid Phone Number');
      return;
    }

    this.createContact().then(
      (data) => {
        this.close(data);
      },
      (err) => {
        // console.log(err);
      }
    );
  }

  createContact(){

    return new Promise((resolve, reject) => {

      this.submitAttempt = true;
      let formdata = this.aForm.value;

      // if (this.isUpdate) {
      //   formdata['contact_id'] = this.contact_id;
      //   formdata['user_id'] = this.user_id;
      // }

      // console.log(formdata);

      this.network.createContact(formdata).then((res: any) => {
        // this.utility.presentSuccessToast(res.message);
        resolve(res);

      }, err => {
        reject(err);
      });


    });

  }

  async openCounryCode() {
    // CreateGroupPage as modal
    const res = await this.modals.present(CountryCodeComponent, {
      dc: this.dial_code,
    });
    const data = res.data;
    if (data.data != 'A') {
      this.dial_code = data;
      this.aForm.controls.dial_code.setValue(this.dial_code.dial_code);
    }
  }

  onTelephoneChange(ev) {

    if (ev.inputType !== 'deleteContentBackward') {
      const utel = this.utility.onkeyupFormatPhoneNumberRuntime(ev.target.value, false);
      console.log(utel);
      ev.target.value = utel;
      this.aForm.controls['phone_number'].patchValue(utel);
      // ev.target.value = utel;
    }

  }
}

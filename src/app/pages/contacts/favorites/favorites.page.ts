import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage extends BasePage implements OnInit {

  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  search_value = '';
  search_on = false;
  page = 1;

  constructor(injector: Injector, public storedcontacts: StoredContactsService) {
    super(injector)
    this.search_on = false;
    this.storedcontacts.getMyContacts("", 1, 1);
    this.events.subscribe("contacts:favouritesUpdated", this.doRefresh.bind(this));
  }

  ngOnInit() { }

  setFocusOnSearch() {
    var self = this;
    this.search_on = true;
    setTimeout(() => {
      self.searchbar.setFocus();
    }, 500);
  }

  filterGlobal($event) {
    let val = this.search_value;
    this.storedcontacts.getMyContacts(val, 1, 1).then(v => {
      console.log(v);
    });
  }

  resetSearch() {
    this.search_value = '';
    this.storedcontacts.getMyContacts(this.search_value, this.page, 1);
  }

  doRefresh($event) {
    this.page = 1;
    this.storedcontacts.getMyContacts(this.search_value, this.page, 1).then(v => {
      if ($event.target) {
        $event.target.complete();
      }
    });
  }

  loadMoreContacts($event) {
    this.page++;
    this.storedcontacts.getMyContacts(this.search_value, this.page, 1).then(v => {
      $event.target.complete();
    });
  }

  goBack() {
    this.events.publish('contacts:popup', {});
  }

  async presentPopover($event, item) {
    const res = await this.popover.present(ContactpopoverPageComponent, $event,
      {
        pid: item,
        flag: "F"
      }, 'interface-style');

    let data = res.data;

    if (data == null) {
      return;
    }

    item = data["pid"];

    switch (data["param"]) {
      case "R":
        this.storedcontacts.removeFavoritesFromContacts(item);
        break;
      case "G":
        item['contact_id'] = item['id'];
        this.storedcontacts.createSelectedInContactGroupByItem([item]);
        break;
      case "E":
        // this.storedcontacts.editFamily(item, 1);
        await this.storedcontacts.editContact(item);
        this.resetSearch();
        break;
      case "C":
        this.utility.dialMyPhone(item.formatted_phone_number);
        break;
      case "SP":
        this.storedcontacts.fetchEventsToSendPassTo(item)
        break;
      case "RP":
        this.storedcontacts.sendPassRequestToContact(item);
        break;
    }

  }

  plusHeaderButton(param) {
    this.storedcontacts.syncContactsToFavourities();
  }

}

import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  user: any;
  socialImage;

  constructor(injector: Injector) {
    super(injector);
    this.setupForm();
  }

  ngOnInit() {}

  setupForm() {

    const re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      name: ['shoaib uddin', Validators.compose([
        Validators.minLength(3),
        Validators.maxLength(30),
        Validators.pattern('[a-zA-Z ]*'),
        Validators.required
      ])],
      phone_number: ['3432322100', Validators.compose([ Validators.required]) ],
      password: ['Hotm@!l1VU', Validators.compose([
        Validators.minLength(9),
        Validators.maxLength(30),
        Validators.required,
        Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/)
      ])],
      password_confirm: ['Hotm@!l1VU', Validators.compose([Validators.minLength(6), Validators.maxLength(30), Validators.required])],
    }, { validator: this.utility.checkIfMatchingPasswords('password', 'password_confirm') });


  }

  login() {
    this.nav.setRoot('pages/login');
  }

  singUp(){

    this.submitAttempt = true;

    const inName = !this.aForm.controls.name.valid || !this.utility.isLastNameExist(this.aForm.controls.name.value);
    let inPhone = !this.aForm.controls.phone_number.valid;
    inPhone = !this.utility.isPhoneNumberValid(this.aForm.controls.phone_number.value);
    const inPassword = !this.aForm.controls.password.valid;
    const inCpassword = !this.aForm.controls.password_confirm.valid;

    if(inName) {
      this.utility.presentFailureToast('Name/Full Name is required');
      return;
    }

    if(inPhone){
      this.utility.presentFailureToast('Phone Number required');
      return;
    }

    if(inPassword){
      this.utility.presentFailureToast('Valid Password Required');
      return;
    }

    const rp = this.aForm.controls.password.value;
    const rcp = this.aForm.controls.password_confirm.value;


    if(rp !== rcp){
      this.utility.presentFailureToast('confirm password must match password field');
      return;
    }

    const formdata = this.aForm.value;
    formdata.profile_image = null;
    this.doSignup(formdata);

  }

  doSignup(formdata) {

    formdata.register_with_phonenumber = true;
    this.network.register(formdata).then(() => {
      formdata.showelcome = true;
      this.users.login(formdata);
      // this.events.publish('user:login', formdata);
    }, err => {
      console.log(err.message);
    });

  }

  onTelephoneChange(ev, tel) {
    if (ev.inputType !== 'deleteContentBackward') {
      const ptel = this.utility.onkeyupFormatPhoneNumberRuntime(tel, false);
      // console.log(_tel);
      this.aForm.controls.phone_number.setValue(ptel);
    }
  }


}


import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, Location } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PagesRoutingModule } from './pages-routing.module';
import { NewPassComponent } from '../components/new-pass/new-pass.component';
import { NewPassComponentModule } from '../components/new-pass/new-pass.component.module';
import { GmapComponent } from '../components/gmap/gmap.component';
import { ContactpopoverPageComponent } from '../components/contactpopover-page/contactpopover-page.component';
import { ContactItemComponent } from '../components/contact-item/contact-item.component';
import { CodeVerficationComponent } from '../components/code-verfication/code-verfication.component';
import { RequestBecomeResidentComponent } from '../components/request-become-resident/request-become-resident.component';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';


@NgModule({
    declarations: [
      GmapComponent,
      ContactpopoverPageComponent,
      ContactItemComponent,
      CodeVerficationComponent,
      RequestBecomeResidentComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        PagesRoutingModule,
        NewPassComponentModule,
        NgxQRCodeModule
    ],
    exports: [

    ],
    providers: [
        Location,
    ]

})
export class PagesModule{}

import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage extends BasePage implements OnInit {
  aForm: FormGroup;
  tempaccesstoken: any;
  user: any;
  loading = false;

  constructor(injector: Injector) {
    super(injector);

    this.setupForm();
    this.fetchProfileValues();
  }

  ngOnInit() {}

  setupForm() {
    this.aForm = this.formBuilder.group({
      push_notification: [true, Validators.required],
      email_notification: [true, Validators.required],
      vacation_mode: [false, Validators.required],
    });
  }

  async fetchProfileValues() {
    this.loading = true;
    this.network.getUser(false).then(
      (user: any) => {
        this.loading = false;
        this.user = user.user;
        console.log(this.user);

        this.aForm.controls.push_notification.setValue(
          !!parseInt(this.user.push_notification, 10)
        );
        this.aForm.controls.email_notification.setValue(
          !!parseInt(this.user.email_notification, 10)
        );
        this.aForm.controls.vacation_mode.setValue(
          !!parseInt(this.user.vacation_mode, 10)
        );
      },
      (err) => {
        this.modals.dismiss();
      }
    );
  }

  updateFlags() {
    const formdata = this.aForm.value;
    console.log(formdata);

    // formdata.push_notification = formdata.push_notification ? 'true' : 'false';
    // formdata.email_notification = formdata.email_notification ? 'true' : 'false';
    // formdata.vacation_mode = formdata.vacation_mode ? 'true' : 'false';
    this.loading = true;
    this.network.updateUserNotificationSettings(formdata).then(
      (res) => {
        this.loading = false;
        // this.utility.presentSuccessToast(res.message);
        this.close();
      },
      (err) => {}
    );
  }

  close() {
    this.modals.dismiss();
  }
}

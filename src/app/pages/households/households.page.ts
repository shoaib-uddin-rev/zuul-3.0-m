import { Component, Injector, OnInit } from '@angular/core';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';
import { BasePage } from '../base-page/base-page';
import { ParentalControlPage } from '../parental-control/parental-control.page';
import { EditMembersPermissionComponent } from './edit-members-permission/edit-members-permission.component';

@Component({
  selector: 'app-households',
  templateUrl: './households.page.html',
  styleUrls: ['./households.page.scss'],
})
export class HouseholdsPage extends BasePage implements OnInit {

  user: any;
  plist = [];
  search = "";
  isLoading = false;

  constructor(injector: Injector) {
    super(injector);
    this.initialize();
  }

  ngOnInit() {
  }

  async initialize() {
    this.user = await this.sqlite.getActiveUser();
    this.getMyFamily();
    // this.events.subscribe('permissions:update', this.permissionsUpdate.bind(this))
  }

  doRefresh($event) {

  }

  getMyFamily() {
    this.isLoading = true;
    this.plist = [];
    this.network.getMyFamilyMembers().then((response: any) => {
      console.log(response);
      this.plist = response;
      // this.selectedItem = null;
      // this.noselection = true;
      this.plist.forEach((eachObj) => {
        eachObj.dark = false;
      });
      this.isLoading = false;

    }, err => { this.closeModal({ data: 'A' }); });
  }

  closeModal(res) {
    // this.modals.dismiss(res);
    this.nav.pop();
  }

  async presentEditFamilyPopover($event, item) {
    const { data } = await this.popover.present(ContactpopoverPageComponent, $event, {
      pid: item, flag: 'MHP',
    });

    if (data == null) {
      return;
    }

    let _item = data.pid;

    switch (data.param) {
      case 'SP':
        this.showPasses(_item);
        break;
      case 'PC':
        this.showParentalControls(_item);
        break;
      case 'EP':
        this.editPermissions(_item);
        break;
    }

  }

  showParentalControls(item) {
    this.nav.push('pages/parental-control', { user_id: item.id, })
  }

  editPermissions(item) {
    console.log(item);
    let obj = {
      user_id: item.id,
      can_manage_family: item.can_manage_family,
      allow_parental_control: item.allow_parental_control,
      can_send_passes: item.can_send_passes
    }
    this.modals.present(EditMembersPermissionComponent, obj)
  }

  showPasses(user) {
    this.nav.push('pages/passes', { user_id: user.id, display_name: user.full_name });
  }
}

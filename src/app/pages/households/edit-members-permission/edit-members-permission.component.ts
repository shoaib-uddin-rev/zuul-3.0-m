import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-edit-members-permission',
  templateUrl: './edit-members-permission.component.html',
  styleUrls: ['./edit-members-permission.component.scss'],
})
export class EditMembersPermissionComponent extends BasePage implements OnInit {

  can_manage_family = 0;
  can_send_passes = 0;
  allow_parental_control = 0;
  user_id;

  constructor(
    injector: Injector
  ) {
    super(injector)
  }

  ngOnInit() { }

  closeModal() {
    this.modals.dismiss();
  }

  updatePermissions(flag) {

    let obj = {
      member_id: this.user_id
    }

    if (flag == "can_manage_family") {
      obj[flag] = this.can_manage_family;
      this.network.setManageFamilyPermission(obj).then((response: any) => {
        console.log(response);
      }, err => { });
    }

    if (flag == "can_send_passes") {
      obj[flag] = this.can_send_passes;
      this.network.setSendPassesPermission(obj).then((response: any) => {
        console.log(response);
      }, err => { });
    }

    if (flag == "allow_parental_control") {
      obj[flag] = this.allow_parental_control;
      this.network.setAllowParentalPermission(obj).then((response: any) => {
        console.log(response);
      }, err => { });
    }
  }

  save() {
    console.log(this.can_manage_family);
    console.log(this.allow_parental_control)
    console.log(this.can_send_passes);

  }
}

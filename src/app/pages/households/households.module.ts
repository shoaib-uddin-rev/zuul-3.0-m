import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HouseholdsPageRoutingModule } from './households-routing.module';

import { HouseholdsPage } from './households.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { EditMembersPermissionComponent } from './edit-members-permission/edit-members-permission.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HouseholdsPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [
    HouseholdsPage,
    EditMembersPermissionComponent
  ]
})
export class HouseholdsPageModule {}

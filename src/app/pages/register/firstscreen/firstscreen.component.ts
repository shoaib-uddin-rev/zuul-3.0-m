import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { CountryCodeComponent } from 'src/app/components/country-code/country-code.component';
import { BasePage } from '../../base-page/base-page';

const countries = require('src/app/data/countries.json');
const userData = require('src/app/data/user.json')

@Component({
  selector: 'app-firstscreen',
  templateUrl: './firstscreen.component.html',
  styleUrls: ['./firstscreen.component.scss'],
})
export class FirstscreenComponent extends BasePage implements OnInit {

  @Input() isUpdate = false;
  @Input() isNew = false;
  aForm: FormGroup;
  submitAttempt = false;
  profile: any;
  tuser: any;
  get user(): any {
    return this.tuser;
  }
  @Input() set user(value: any) {
    this.tuser = value;
    // console.log(value);
    this.fetchProfileValues();
  };

  piImageBase64 = this.users.avatar;

  dial_code = {
    name: 'United States',
    dial_code: '+1',
    code: 'US',
    image: 'assets/imgs/flags/us.png',
  };

  constructor(injector: Injector, public domSanitizer: DomSanitizer) {
    super(injector);
    this.setupForm();

    this.events.subscribe('registration:firstScreenValidation', this.firstScreenValidation.bind(this) );
  }



  setupForm() {
    const re = /\S+@\S+\.\S+/;

    const tempdate = new Date();
    // let myDate: String = new Date(tempdate.getTime() - tempdate.getTimezoneOffset()*60000).toISOString();


    this.aForm = this.formBuilder.group({
      name: [
        '',
        Validators.compose([
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required,
        ]),
      ],
      middle_name: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*') ])],
      last_name: [
        '',
        Validators.compose([
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required,
        ]),
      ],
      date_of_birth: [null, Validators.compose([Validators.required])],
      phone_number: [
        {value: ''},
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.pattern('[0-9]*'),
        ]),
      ],
      email: [
        '',
        Validators.compose([
          Validators.pattern(re),
          Validators.required,
        ]) /*, VemailValidator.checkEmail */,
      ],
      dial_code: ['+1'],
      profile_image: [''],
    });
  }

  async fetchProfileValues() {

    // this.user = user;
    // get user data
    // redirect to welcomeid
    if(!this.user){
      return;
    }

    console.log('user', this.user);
    this.profile = this.user;
    this.aForm.controls.name.setValue(this.profile.first_name);
    this.aForm.controls.middle_name.setValue(
      this.profile.middle_name
    );
    this.aForm.controls.last_name.setValue(this.profile.last_name);
    const dob = this.profile.date_of_birth;

    if (dob) {
      const date = Date.parse(dob.replace(' ', 'T'));
      const iso = new Date(date).toISOString();
      this.aForm.controls.date_of_birth.setValue(iso);

      // this.aForm.controls["date_of_birth"].setValue(dob);
      // console.log(dob);
      //this.date_of_birth_mdy = this.utility.formatDateMDY(dob);
      // this.date_of_birth_mdy = this.profile["dateOfBirthMDY"];
      // this.selectedDate = new Date(dob);
    }

    const ttel = this.utility.onkeyupFormatPhoneNumberRuntime(
      this.user.phone_number
    );
    this.aForm.controls.phone_number.setValue(ttel);

    // if (!this.user.dial_code) {
    //   this.user.dial_code = '+1';
    // }else{
    //   this.user.dial_code = '+' + this.user.dial_code;
    // }
    this.aForm.controls.dial_code.setValue(this.user.dial_code);

    this.aForm.controls.email.setValue(this.user.email);

    console.log(this.user.profile_image);
    this.aForm.controls.profile_image.setValue(
      this.user.profile_image
    );

    // this.aForm.controls["licence_number"].setValue(this.user["licence_number"]);
    this.piImageBase64 = this.user.profile_image;

    //
    // do code search
    this.setDialCode();

  }

  ngOnInit() { }

  getprofileimage() {

    if(!this.platform.is('cordova')){
      this.piImageBase64 = userData.avatar;
      this.aForm.controls.profile_image.setValue(this.piImageBase64);
    }else{
      this.utility.snapImage('profile').then((image) => {
        this.piImageBase64 = image as string;
        this.aForm.controls.profile_image.setValue(this.piImageBase64);
      });
    }

  }

  async openCounryCode() {
    // CreateGroupPage as modal

    const res = await this.modals.present(CountryCodeComponent);
    const data = res.data;
    if (data.data != 'A') {
      this.dial_code = data;
      this.aForm.controls.dial_code.setValue(this.dial_code.dial_code);
    }
  }

  setDialCode() {
    const n: any[] = countries;
    this.dial_code = n.find((x) => x.dial_code == this.user.dial_code);
    this.dial_code.image =
      'assets/imgs/flags/' + this.dial_code.code.toLowerCase() + '.png';
  }

  onTelephoneChange(ev) {

    if (ev.inputType != 'deleteContentBackward') {
      const ttel = this.utility.onkeyupFormatPhoneNumberRuntime(ev.target.value, false);
      // console.log(ttel);
      this.aForm.controls.phone_number.setValue(ttel);
    }

  }

  validateDate() {

    const dob = this.aForm.controls.date_of_birth.value;
    const date = new Date(dob);
    if (!this.utility.isOverThirteen(date)) {
      this.utility.presentFailureToast(
        'Age must be over 13 years old'
      );
      this.aForm.controls.date_of_birth.setValue(null);
    }
  }

  async goToNext(num) {

    const flag = await this.validateForm();
    // console.log(flag);
    if (!flag) {
      return;
    }

    let formData = this.aForm.value;
    formData["phone_number"] = this.utility.getOnlyDigits(
      this.aForm.controls.phone_number.value
    );

    this.events.publish('registration:setdata', formData);
    this.events.publish('registration:slidetoNext', num);
  }

  async firstScreenValidation(){
    const flag = await this.validateForm();
    // console.log(flag);
    if(flag === true){
      this.events.publish('registration:firstScreenValidated', this.aForm.value);
    }
  }

  validateForm(): Promise<boolean> {

    const eror = 'Please Enter Valid';
    return new Promise(resolve => {

      const invalidName = !this.aForm.controls.name.valid;
      const invalidMname = !this.aForm.controls.middle_name.valid;

      const invalidDialcode = !this.aForm.controls.dial_code.valid;
      const invalidLname = !this.aForm.controls.last_name.valid;
      const invalidBirth = !this.aForm.controls.date_of_birth.valid;

      let invalidPhone = !this.aForm.controls.phone_number.valid;
      invalidPhone = !this.utility.isPhoneNumberValid(
        this.aForm.controls.phone_number.value
      );

      const invalidEmail = !this.aForm.controls.email.valid;

      if (invalidName) {
        this.utility.presentFailureToast(
          eror + ' Name, can not be null, only alphabets allowed'
        );
        resolve(false);
        return;
      }

      if(invalidMname){
        this.utility.presentFailureToast(
          eror + ' Middle name, can not be null, only alphabets allowed'
        );
        resolve(false);
        return;
      }

      if (invalidDialcode) {
        this.utility.presentFailureToast(eror + ' Dial Code');
        resolve(false);
        return;
      }
      if (invalidLname) {
        this.utility.presentFailureToast(
          eror + ' Last Name, can not be null, only alphabets allowed'
        );
        resolve(false);
        return;
      }
      if (invalidBirth) {
        this.utility.presentFailureToast(eror + ' Date Of Birth');
        resolve(false);
        return;
      }
      if (invalidEmail) {
        this.utility.presentFailureToast(eror + ' Email');
        resolve(false);
        return;
      }
      if (invalidPhone) {
        this.utility.presentFailureToast(
          eror + ' Phone Number (Minimum 10 Digits)'
        );
        resolve(false);
        return;
      }

      // CHeck if Email already exist with another user
      const emailObj = { email: this.aForm.controls.email.value };
      this.network.checkEmailAlreadyInUse(emailObj).then(
        (res) => {
          const userCount = res.user_count;
          if (parseInt(userCount, 10) > 0) {
            this.promptUserToUseEmail(this.aForm.controls.email.value).then(() => {
                resolve(true);
                return;
            });
          } else {
            resolve(true);
            return;
          }
        },
        (err) => { }
      );

    });




  }

  promptUserToUseEmail(email): Promise<boolean> {
    return new Promise(async (resolve) => {

      const flag = await this.utility.presentConfirm('Get Permission', 'Cancel', 'Email ALert', email +
        ' is already in use, try other email or get permission to use this email address');

      if (flag) {
        this.sendVerifyEmailAddress(email).then((res) => {
          this.user.suspand = '1';
          resolve(res);
        });
      }



    });
  }

  sendVerifyEmailAddress(email): Promise<boolean> {
    return new Promise((resolve) => {
      const emailObj = { email: this.aForm.controls.email.value };
      this.network.verifyEmailAddress(emailObj).then(
        (res) => {
          // this.utility.presentSuccessToast(res.message);
          resolve(true);
        },
        (err) => {
          resolve(false);
        }
      );
    });
  }


}

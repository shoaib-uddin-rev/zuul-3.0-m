import { AfterViewInit, Component, Injector, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { IonContent, IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { WelcomescreenComponent } from './welcomescreen/welcomescreen.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage extends BasePage implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('slides', { static: true }) slides: IonSlides;
  @ViewChild('content', { static: true }) content: IonContent;
  @Input() new = false;
  @Input() user: any;
  public formdata: any;
  revisit = false;
  step = 0;

  heading1 = 'ALL DONE! Click The Button Below To Start Using The App';
  start = 'Start';
  heading = this.step === 1 ? 'Profile' : ( this.step === 2 ? 'Contact Information' : 'Verification');
  firstScreenData;
  secondScreenData;
  dial_code = {
    name: 'United States',
    dial_code: '+1',
    code: 'US',
    image: 'assets/imgs/flags/us.png',
  };


  constructor(injector: Injector) {
    super(injector);



    this.events.subscribe('registration:slidetoNext', this.goToNext.bind(this));
    this.events.subscribe('registration:setdata', this.setData.bind(this));
    this.events.subscribe('registration:finish', this.dashboardopen.bind(this) );

    // screen validations
    this.events.subscribe('registration:firstScreenValidated', this.firstScreenValidated.bind(this) );
    this.events.subscribe('registration:secondScreenValidated', this.secondScreenValidated.bind(this) );
    this.events.subscribe('registration:thirdScreenValidated', this.thirdScreenValidated.bind(this) );

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.events.unsubscribe('registration:slidetoNext');
    this.events.unsubscribe('registration:setdata');
    this.events.unsubscribe('registration:finish' );

    // screen validations
    this.events.unsubscribe('registration:firstScreenValidated' );
    this.events.unsubscribe('registration:secondScreenValidated' );
    this.events.unsubscribe('registration:thirdScreenValidated');

    // first screen
    this.events.unsubscribe('registration:firstScreenValidation' );

    // second screen
    this.events.unsubscribe('registration:secondScreenValidation');

    // third screen
    this.events.unsubscribe('registration:thirdScreenValidation');





  }

  ngAfterViewInit(): void {

    // if (this.new === true) {
    //   const self = this;
    //   setTimeout(() => {
    //     self.modals.present(WelcomescreenComponent);
    //   }, 1000);

    // }

    // this.fetchProfileValues();

    this.slides.update();
    this.moveSlide(0);

  }

  ngOnInit() { }


  // fire event into first screen
  firstScreenValidated(data){
    this.user = {...this.user, ...data};
    console.log('firstScreenData', this.user);
    this.dial_code.dial_code = data.dial_code;
    this.step = 1;
    this.moveSlide(this.step);
  }

  secondScreenValidated(data) {

    this.user = {...this.user, ...data};
    console.log('secondScreenData', this.user);

    this.step = 2;
    this.moveSlide(this.step);
  }

  async thirdScreenValidated(data) {

    console.log(data);

    // this.user = {...data, ...this.user};
    this.user['license_image'] = data['license_image'];
    this.user['license_type'] = data['license_type'];
    this.user['first_name'] = this.user.name;
    console.log('thirdScreenData', this.user);


    const res = await this.updateProfileAsIs(this.user);
    if(res){
      this.step = 3;
      this.moveSlide(this.step);
    }


  }

  async goToNext() {

    const num = await this.slides.getActiveIndex();
    console.log(num);

    if(num === 0){
      this.events.publish('registration:firstScreenValidation');
    }

    if(num === 1){
      this.events.publish('registration:secondScreenValidation');
    }

    if(num === 2){
      this.events.publish('registration:thirdScreenValidation');
    }




    // if(num === 4){

    //   const formdata = this.user;
    //   formdata.id = this.user.id;

    //   if (!this.revisit) {
    //     this.revisit = false;
    //   }
    //   this.updateProfileAsIs(formdata);

    // }else{

    //   this.step = num + 1;

    //   this.slides.lockSwipes(false);
    //   this.slides.slideTo(this.step, 500);
    //   this.slides.lockSwipes(true);
    // }

  }

  moveSlide(num){
    this.content.scrollToTop();
    this.slides.lockSwipes(false);
    this.slides.slideTo(num, 500);
    this.slides.lockSwipes(true);
  }

  // async fetchProfileValues() {
  //   // get user data
  //   this.network.getUser().then((user: any) => {
  //     console.log(user);
  //     this.user = user.user;
  //   });
  // }

  setData(data){
    this.user = { ...this.user, ...data};
  }

  seProfileImageInData(formdata) {
    console.log('here-', formdata.profile_image);
    return new Promise((resolve) => {
      this.utility
        .convertImageUrltoBase64(formdata.profile_image)
        .then((image) => {
          resolve(image);
        });
    });
  }

  seLicenceImageInData(formdata) {
    console.log('here1-', formdata.profile_image);
    return new Promise((resolve) => {
      this.utility
        .convertImageUrltoBase64(formdata.license_image)
        .then((image) => {
          resolve(image);
        });
    });
  }

  updateProfileAsIs(formdata) {

    return new Promise( resolve => {

      const self = this;
      const promise = Promise.all([
        this.seProfileImageInData(formdata),
        this.seLicenceImageInData(formdata),
      ]);
      promise.then((images) => {
        formdata.profile_image = images[0];
        formdata.license_image = images[1];
        // console.log(formdata);

        if (!this.revisit) {
          this.revisit = false;
        }

        this.network.updateProfile(formdata).then( async (res) => {

          await this.users.getUser();
          this.events.publish('dashboard:refreshpage');
          resolve(true);
        },(err) => { resolve(false); }
        );
      });

    });

  }

  dashboardopen() {
    this.new = false;
    this.events.publish('user:get');
  }

  async goStep($event){
    const num = await this.slides.getActiveIndex();
    if($event < num){
      this.step = $event;
      this.moveSlide($event);
    }


  }




}

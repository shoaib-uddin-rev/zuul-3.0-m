import { FormGroup, Validators } from '@angular/forms';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';
import { DomSanitizer } from '@angular/platform-browser';

const userdata = require('src/app/data/user.json');

@Component({
  selector: 'app-thirdscreen',
  templateUrl: './thirdscreen.component.html',
  styleUrls: ['./thirdscreen.component.scss'],
})
export class ThirdscreenComponent extends BasePage implements OnInit {

  aForm: FormGroup;
  is_licence_locked = 0;

  profile: any;

  _user: any;
  get user(): any {
    return this._user;
  }
  @Input() set user(value: any) {
    this._user = value;

    // console.log(value);
    this.fetchProfileValues();
  };


  constructor(injector: Injector, public domSanitizer: DomSanitizer) {
    super(injector);
    this.setupForm();

    this.events.subscribe('registration:thirdScreenValidation', this.thirdScreenValidation.bind(this));
  }

  liImageBase64 = '';

  ngOnInit() { }

  setupForm() {
    const re = /\S+@\S+\.\S+/;
    const tempdate = new Date();
    // let myDate: String = new Date(tempdate.getTime() - tempdate.getTimezoneOffset()*60000).toISOString();
    this.aForm = this.formBuilder.group({
      license_type: ['driving_license'],
      license_image: ['', Validators.compose([Validators.required])],
    });
  }

  async fetchProfileValues() {
    // get user data

    if (!this.user) {
      return;
    }

    // redirect to welcomeid
    // this.profile = this.user;
    console.log(this.user);

    this.aForm.controls.license_type.setValue(
      this.user.license_type
    );

    this.liImageBase64 = this.user.license_image;
    this.aForm.controls.license_image.setValue(this.liImageBase64);


    // const iv = this.profile.license_image_url;

    // if(iv){
    //   if (iv.indexOf('licence') != -1) {
    //     this.profile.license_image_url = null;
    //   }
    // }

    // if(!iv){
    //   this.aForm.controls.license_image.setValue(
    //     userdata.license_image
    //   );

    //   this.liImageBase64 = userdata.license_image;
    // }else{

    //   this.aForm.controls.license_image.setValue(
    //     this.profile.license_image_url
    //   );

    //   this.liImageBase64 = this.profile.license_image_url;

    // }



  }

  async thirdScreenValidation(){
    const flag = await this.validateForm();
    // console.log(flag);

    if(flag === true){
      this.events.publish('registration:thirdScreenValidated', this.aForm.value);
    }
  }

  validateForm(): Promise<boolean> {

    const eror = 'Please Enter Valid';
    return new Promise(resolve => {

      const in_license_type = !this.aForm.controls.license_type.valid;
      const in_license_image = !this.aForm.controls.license_image.valid;

      if (in_license_type) {
        this.utility.presentFailureToast(eror + ' licence format');
        resolve(false);
      }
      // if(in_address){ this.showAlert(eror + " address"); return }
      if (in_license_image) {
        this.utility.presentFailureToast(eror + ' city');
        resolve(false);
      }

      resolve(true);

    });

  }

  getlicenceimage() {

    this.network.getLicenceLockStatus().then((v) => {
      // console.log('locked', v.locked == false );
      if (v.locked == false) {
        if(!this.platform.is('cordova')){
          this.liImageBase64 = userdata.small_image as string;
          this.aForm.controls.license_image.setValue(userdata.small_image);
        }else{
          this.utility.snapImage('licence').then((image) => {
            this.liImageBase64 = image as string;
            this.aForm.controls.license_image.setValue(this.liImageBase64);
          });
        }
      } else {
        this.promptLicenceUploadLock();
      }
    });
  }

  promptLicenceUploadLock() {
    this.utility.showAlert(
      'Licence details are Locked, please contact admins to unlock the feature.'
    );
  }

  async finishregister(num) {

    const flag = await this.validateForm();
    // console.log(flag);
    if (!flag) {
      return;
    }

    let res = this.aForm.value;
    res['license_type'] = 'driving_license';

    this.events.publish('registration:setdata', res);
    this.events.publish('registration:slidetoNext', num);

  }

}

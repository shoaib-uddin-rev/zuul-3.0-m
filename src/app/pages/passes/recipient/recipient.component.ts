import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar, IonItemSliding } from '@ionic/angular';
import { BasePage } from '../../base-page/base-page';
import { SyncContactsPage } from '../../sync-contacts/sync-contacts.page';

@Component({
  selector: 'app-recipient',
  templateUrl: './recipient.component.html',
  styleUrls: ['./recipient.component.scss'],
})
export class RecipientComponent extends BasePage implements OnInit {

  searchTerm: string = '';
  original: any;
  contacts: any;
  items: any = [];
  noselection: boolean = false;

  item: any;
  isHeadOfFamily: number = 0;
  canRetractRecipients: number = 0;
  allowParentalControl: number = 0;

  // new variables added
  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  search_value = "";
  search_on = false;
  event_name = "";

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  initialize() {
    let c = this.item;
    console.log("sent item", c);
    this.event_name = c.event_name;
    const hof = this.isHeadOfFamily;
    const crp = this.allowParentalControl;

    if (hof == 1) {
      this.canRetractRecipients = 1;
    } else if (crp == 1) {
      this.canRetractRecipients = 1;
    } else {
      this.canRetractRecipients = 0;
    }

    this.original = c;

    this.network.getContactToPass(this.original['id']).then(response => {
      console.log(response);
      if (response['list'].length == 0) {
        return;
      }
      this.items = response['list'];
      this.contacts = response['list'];
      this.checkItems()

    }, error => { })
  }

  setFocusOnSearch() {
    var self = this;
    this.search_on = true;
    setTimeout(() => {
      self.searchbar.setFocus();
    }, 500);
  }

  doRefresh($event) {
    // this.groupcontactsoffset = 0;
    // this.getContactListByGroupId(this.search_value, this.group.id, this.groupcontactsoffset, false, false).then( v => {
    //   $event.complete();
    // });
  }

  closeModal() {
    this.modals.dismiss();
  }

  async showConfirm(item) {

    const flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Are You Sure?', 'I want to retract this recipient’s pass and make all associated QR codes null and void');

    if (flag) {
      this.network.removePassRecipient(item['id']).then((res: any) => {

        var recepient_count = res['recepient_count'];
        if (recepient_count == 0) {
          this.events.publish('pass.deleted.id', { 'pass_id': res['pass_id'] })
        }

        this.spliceItem(item);
        if (this.items.length == 0) {
          this.events.publish('passes:refreshsent')
          this.modals.dismiss({ data: 'A' });
        }

      }, err => {
        this.utility.presentToast(err.error.message);
      });
    }

  }

  spliceItem(item) {

    const index: number = this.items.indexOf(item);
    if (index !== -1) {
      this.items.splice(index, 1);
    }
  }

  checkItems() {
    this.items.forEach((item) => {
      // console.log(item['is_enabled'], parseInt(item['is_enabled']) == 1)
      if (parseInt(item['is_enabled']) == 1) {
        item['selected'] = true;
      }
    });
  }

  setFilteredItems(flag) {

    var self = this;

    this.items = this.contacts.filter((item) => {
      return (item.added_user.name && item.added_user.name.toLowerCase().indexOf(self.searchTerm.toLowerCase()) > -1)
        || (item.added_user.email && item.added_user.email.toLowerCase().indexOf(self.searchTerm.toLowerCase()) > -1)
        || (item.added_user.phone_number && item.added_user.phone_number.toLowerCase().indexOf(self.searchTerm.toLowerCase()) > -1);
    });

  }

  returnSelected() {

    this.items.forEach((item) => {
      item['is_enabled'] = item['selected'] ? "1" : "0"
    });
    this.original['get_qrs'] = this.items;
    this.modals.dismiss(this.original);
  }

  fireChange(item) {
    item.is_enabled = (item.selected) ? 1 : 0;
    this.network.togglePassEnable(item.id, item.is_enabled).then((res: any) => {
      console.log(res);
    }, err => { })
  }

  async plusHeaderButton() {

    const { data } = await this.modals.present(
      SyncContactsPage,
      { fromContacts: true }
    );

    console.log("Here are selected contacts", data);

    if (data == null) { return }

    if (data.list.length > 0) {
      console.log(this.original);
      let passid = this.original.id;
      let d = {
        contactIds: data.list
      }
      console.log(d);

      this.network.addContactToPass(passid, d).then(response => {
        console.log(response);
        this.utility.presentSuccessToast(response['message']);
        this.initialize();
      }, err => { })
    }


  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArchievePageRoutingModule } from './archieve-routing.module';

import { ArchievePage } from './archieve.page';
import { PassItemComponentModule } from 'src/app/components/pass-item/pass-item.component.module';
import { EmptyviewComponentModule } from 'src/app/components/emptyview/emptyview.component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArchievePageRoutingModule,
    PassItemComponentModule,
    EmptyviewComponentModule
  ],
  declarations: [ArchievePage]
})
export class ArchievePageModule {}

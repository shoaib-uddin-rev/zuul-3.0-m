import { Component, Injector, OnInit } from '@angular/core';
import { GmapComponent } from 'src/app/components/gmap/gmap.component';
import { NewPassComponent } from 'src/app/components/new-pass/new-pass.component';
import { PassServiceService } from 'src/app/services/pass-service.service';
import { BasePage } from '../../base-page/base-page';
import { DetailsPage } from '../details/details.page';

@Component({
  selector: 'app-archieve',
  templateUrl: './archieve.page.html',
  styleUrls: ['./archieve.page.scss'],
})
export class ArchievePage extends BasePage implements OnInit {

  avatar = this.users.avatar;
  storedData;
  plist: any[] = [];
  offset = 1;
  canBeResident = false;
  isHeadOfFamily = 0;
  canSendPasses = 0;
  user: any;
  title = 'My';
  alphalist: any[];
  datelist: any[];
  dcount = 65 * 3;
  searchTerm = '';
  seeingOthersPass = false;
  c_selection = 'archieve';
  user_id;
  filter;


  constructor(injector: Injector, public passService: PassServiceService) {
    super(injector);

    this.alphalist = this.utility.alphalist;
    this.user_id = this.passService.house_member_id;
    const display_name = this.nav.getQueryParams().display_name;

    console.log(this.user_id);

    if (display_name) {
      this.title = display_name + '\'s';
    }

    this.segmentChanged(this.c_selection);
    this.events.subscribe('pass.deleted.id', this.deleteLocallyByPassId.bind(this));
    this.events.subscribe('passes:searched', this.searchArchive.bind(this));
    this.events.subscribe('passes:searchClear', this.doRefresh());
    this.events.subscribe("passes:filterUpdated", (filter) => {
      this.filter = filter;
    });
  }

  ngOnInit() {
  }

  deleteLocallyByPassId(obj) {

    const pass_id = obj.pass_id;
    if (pass_id) {
      alert('Here');
      const elementPos = this.plist.map(x => x.id).indexOf(pass_id);
      this.plist.splice(elementPos, 1);
    }
  }

  async searchArchive() {
    this.offset = 1;
    console.log("Searching Archive Passes");
    this.storedData = { search: this.searchTerm.toLowerCase() };
    await this.getPasses(this.storedData);
  }

  async segmentChanged(choice) {

    this.c_selection = choice;
    this.user = await this.sqlite.getActiveUser();
    if (!this.user_id) {
      this.user_id = this.user.id;
    }
    switch (choice) {
      case 'archieve':
        this.doRefresh();
        break;
      case 'sent':
        this.doRefresh();
        break;
      case 'scanned':
        this.doRefresh();
        break;
    }

    this.getPassDates();

  }

  getPassDates(data = {}) {

    // according to c selection
    let req: Promise<any>;
    data['expired'] = 1;
    console.log(this.user_id);
    switch (this.c_selection) {
      case 'archieve':
        req = this.network.getUserReceivedPassesDates(this.user_id, data);
        break;
      case 'sent':
        req = this.network.getUserSentPassesDates(this.user_id, data);
        break;
      case 'scanned':
        req = this.network.getUserScannedPassesDates(this.user_id, data);
        break;
    }

    if (req) {
      req.then((res: any) => {
        this.datelist = res.list;
        this.dcount = 65 * this.datelist.length;
      });
    }

  }

  async doRefresh(refresher = null) {

    // this.user = await this.sqlite.getActiveUser();
    if (this.user_id) {
      // // console.log(user_id);
      this.canBeResident = false;
      this.seeingOthersPass = true;
    } else {

      this.user_id = this.user.id;
      this.canBeResident = this.user.canBeResident;
      this.isHeadOfFamily = this.user.head_of_family;
    }


    this.offset = 1;
    this.plist = [];
    this.storedData = {};
    this.getPasses(this.storedData);
    if (refresher) {
      refresher.complete();
    }


  }

  getPasses(data = {}, loader = true) {

    return new Promise(resolve => {

      // according to c selection
      let req: Promise<any>;
      data['expired'] = 1;
      data['page'] = this.offset;

      switch (this.c_selection) {
        case 'archieve':
          req = this.network.getUserReceivedPasses(this.user_id, data, loader);
          break;
        case 'sent':
          req = this.network.getUserSentPasses(this.user_id, data, loader);
          break;
        case 'scanned':
          req = this.network.getUserScannedPasses(this.user_id, data, loader);
          break;
      }

      if (req) {
        req.then((res: any) => {

          console.log("Here is Archive Result", res);
          if (this.offset == 1) {
            this.plist = res.list;
          } else {
            this.plist = [...this.plist, ...res.list];
          }

          this.offset = this.offset + 1;
          resolve(true);

        });
      } else {
        resolve(true);
      }

    });

  }

  goBack() {
    this.events.publish('passes:close');
  }

  async deletePass(item) {

    let flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Are You Sure?', 'This will remove your pass and all associated sent passes along with it ...')

    if (flag) {

      var data = { pass_id: item['pass_id'] }
      this.network.retractSentPass(this.user.id, data).then((res: any) => {
        this.spliceItem(item)
      }, err => { });

    }

  }

  spliceItem(item) {
    const index: number = this.plist.indexOf(item);
    if (index !== -1) {
      this.plist.splice(index, 1);
    }
  }

  async editPass(item) {

    console.log(item);
    const data = await this.modals.present(NewPassComponent, { item: item, edit: true });

  }

  async editDirections(item) {

    const _data = await this.modals.present(GmapComponent, { myAddress: item.description });

    let data = _data.data;
    if (data['data'] != 'A') {
      item['lat'] = data.lat;
      item['lng'] = data.lng;
      item['description'] = data.address;
      item['id'] = item['pass_id'];

      this.network.updatePassDirections(item).then(res => { }, err => { })
    }

  }

  async getDirection(address) {

    var addr = address.trim();
    const data = await this.modals.present(GmapComponent, { myAddress: addr, newAddress: true, isDirections: true });

  }


  loadMore($event) {

    this.getPasses(this.storedData, false).then(v => {
      $event.target.complete();
    });


  }


  async showGuestLogs(obj, type = 'pass') {

    let event = obj.event;
    let item = obj.item;

    event.stopPropagation();

    let id = type == 'pass' ? item.pass_id : item.qrcode_id;
    let _data = { pass_id: id, type: type }
    // const data = await this.modals.present(PassGuestLogsPageComponent, _data);

  }


  async showCheckbox(obj) {

    let event = obj.event;
    let item = obj.item;

    event.stopPropagation();

    let _data = { item: item, isHeadOfFamily: this.isHeadOfFamily, allowParentalControl: false };
    // const data = await this.modals.present(SentContactsPageComponent, _data);
    // let d = data.data;
    // if (d['data'] != 'A') {
    //   item = d;
    // }

  }

  async passDetails(sel) {
    console.log(sel);
    const data = await this.modals.present(DetailsPage, { item: sel.id, flag: "active" });
  }

  filterByAlpha(a, b) {

    this.alphalist.forEach((item, index) => {
      if (item == a) {
        a.enable = !a.enable;
      } else {
        item.enable = false;
      }
    });

    this.offset = 1;
    if (a.enable == false) {
      this.storedData = {};
    } else {
      const param = b == 'sname' ? 'sender_last_name' : b == 'scommunity' ? 'community' : 'event_name';
      this.storedData = { filter_type: param, filter_search: a.alpha.toLowerCase() };
    }

    this.getPasses(this.storedData);

  }

  filterByDate(a, b) {
    this.datelist.forEach((item, index) => {
      if (item == a) {
        a.enable = !a.enable;
      } else {
        item.enable = false;
      }
    });

    this.offset = 1;
    if (a.enable == false) {
      this.storedData = {};
    } else {
      this.storedData = { filter_type: 'date', filter_search: a.pass_start_date };
    }

    this.getPasses(this.storedData);
  }

}

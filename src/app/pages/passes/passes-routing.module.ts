import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PassesPage } from './passes.page';

const routes: Routes = [
  {
    path: '',
    component: PassesPage,
    // children: [
    //   {
    //     path: 'active',
    //     loadChildren: () => import('./active/active.module').then( m => m.ActivePageModule)
    //   },
    //   {
    //     path: 'archieve',
    //     loadChildren: () => import('./archieve/archieve.module').then( m => m.ArchievePageModule)
    //   },
    //   {
    //     path: 'sent',
    //     loadChildren: () => import('./sent/sent.module').then( m => m.SentPageModule)
    //   },
    //   {
    //     path: '',
    //     redirectTo: 'active',
    //     pathMatch: 'full'
    //   }
    // ]
  },
  {
    path: 'details',
    loadChildren: () => import('./details/details.module').then( m => m.DetailsPageModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PassesPageRoutingModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { NewPassComponent } from 'src/app/components/new-pass/new-pass.component';
import { PassServiceService } from 'src/app/services/pass-service.service';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-sent',
  templateUrl: './sent.page.html',
  styleUrls: ['./sent.page.scss'],
})
export class SentPage extends BasePage implements OnInit {

  plist: any[] = [];
  storedData;
  offset = 1;
  canBeResident = false;
  isHeadOfFamily = 0;
  user: any;
  title = 'My';
  alphalist: any[];
  searchTerm = '';
  seeingOthersPass = false;
  isOtherIsHeadOfFamily = 0;
  datelist: any[];
  dcount = 60 * 3;
  allowParentalControl = 0;
  user_id;
  display_name;
  filter;

  constructor(
    injector: Injector,
    public passService: PassServiceService
  ) {
    super(injector);

    const display_name = this.nav.getQueryParams().display_name;
    this.isOtherIsHeadOfFamily = this.nav.getQueryParams().isOtherIsHeadOfFamily;

    if (display_name) {
      this.title = display_name + '\'s';
    }

    this.initialize();

    this.events.subscribe('passes:refreshsent', this.doRefresh.bind(this));
    this.events.subscribe('pass.deleted.id', this.deleteLocallyByPassId.bind(this));
    this.events.subscribe("passes:filterUpdated", (filter) => {
      this.filter = filter;
    });

    this.alphalist = this.utility.alphalist;

  }

  ngOnInit() {
  }

  async initialize() {
    await this.doRefresh();
    this.getPassDates();
  }

  deleteLocallyByPassId(obj) {

    const pass_id = obj.pass_id;
    if (pass_id) {
      const elementPos = this.plist.map(x => x.id).indexOf(pass_id);
      this.plist.splice(elementPos, 1);

    }
  }

  doRefresh(refresher = null) {

    return new Promise(async resolve => {

      this.user_id = this.passService.house_member_id;
      this.user = await this.sqlite.getActiveUser();

      if (!this.user_id) {
        this.user_id = this.user.id;
      }

      if (this.user_id != this.user.id) {

        this.canBeResident = false;
        this.allowParentalControl = parseInt(this.users._user.allow_parental_control, 10);

      }
      else {

        this.user = await this.sqlite.getActiveUser();
        this.user_id = this.user.id;
        this.canBeResident = this.user.canBeResident;
        this.isHeadOfFamily = this.user.head_of_family;

      }

      this.offset = 1;
      this.plist = [];
      this.storedData = {};

      await this.getPasses(this.storedData);
      resolve(true);

      if (refresher) {
        refresher.complete();
      }
    });

  }

  getPasses(data = {}, loader = true) {

    return new Promise(resolve => {
      data['expired'] = 0;
      data['page'] = this.offset;
      this.network.getUserSentPasses(this.user_id, data, loader).then((res: any) => {
        // console.log(res);
        if (this.offset == 1) {
          this.plist = res.list;
        } else {
          this.plist = [...this.plist, ...res.list];
        }

        this.offset = this.offset + 1;
        resolve(true);
      });
    })


  }

  getPassDates() {
    this.network.getUserSentPassesDates(this.user_id, { expired: 0 }).then((res: any) => {
      console.log("datelist", res);
      this.datelist = res.list;
      this.dcount = 65 * this.datelist.length;
    })
  }

  closePassTabs() {
    this.events.publish('pass:allTabClose', Date.now());
  }

  filterByAlpha(a, b) {

    this.alphalist.forEach((item, index) => {
      if (item == a) {
        a.enable = !a.enable;
      } else {
        item.enable = false;
      }
    });

    this.offset = 1;
    if (a.enable == false) {
      this.storedData = {};
    } else {
      const param = b == 'sname' ? 'sender_last_name' : b == 'scommunity' ? 'community' : 'event_name';
      this.storedData = { filter_type: param, filter_search: a.alpha.toLowerCase() };
    }

    this.getPasses(this.storedData);

  }

  filterByDate(a, b) {
    this.datelist.forEach((item, index) => {
      if (item == a) {
        a.enable = !a.enable;
      } else {
        item.enable = false;
      }
    });

    this.offset = 1;
    if (a.enable == false) {
      this.storedData = {};
    } else {
      this.storedData = { filter_type: 'date', filter_search: a.pass_start_date };
    }

    this.getPasses(this.storedData);
  }

  async editPass(item) {

    console.log(item);
    const res = await this.modals.present(NewPassComponent, {item, edit: true});

  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SentPageRoutingModule } from './sent-routing.module';
import { SentPage } from './sent.page';
import { PassItemComponentModule } from 'src/app/components/pass-item/pass-item.component.module';
import { EmptyviewComponentModule } from 'src/app/components/emptyview/emptyview.component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SentPageRoutingModule,
    PassItemComponentModule,
    EmptyviewComponentModule
  ],
  declarations: [SentPage]
})
export class SentPageModule {}

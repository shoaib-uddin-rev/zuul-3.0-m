import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PassesPageRoutingModule } from './passes-routing.module';

import { PassesPage } from './passes.page';
import { EmptyviewComponentModule } from 'src/app/components/emptyview/emptyview.component.module';
import { PassItemComponentModule } from 'src/app/components/pass-item/pass-item.component.module';
import { RecipientComponent } from './recipient/recipient.component';
import { Ng2SearchPipe, Ng2SearchPipeModule } from 'ng2-search-filter';
import { PassGuestLogComponent } from './pass-guest-log/pass-guest-log.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PassesPageRoutingModule,
    EmptyviewComponentModule,
    PassItemComponentModule,
    Ng2SearchPipeModule
  ],
  declarations: [
    PassesPage,
    RecipientComponent,
    PassGuestLogComponent
  ]
})
export class PassesPageModule { }

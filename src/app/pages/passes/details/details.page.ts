import { Component, Injector, Input, OnInit } from '@angular/core';
import { GmapComponent } from 'src/app/components/gmap/gmap.component';
import { BasePage } from '../../base-page/base-page';
import { CreateVehicleComponent } from '../../vehicles/create-vehicle/create-vehicle.component';
import { VehiclesPage } from '../../vehicles/vehicles.page';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage extends BasePage implements OnInit {

  vehicle: any;
  is_vehicle = false;
  pass: any;
  isImageLoaded = false;

  titem;
  @Input() set item(value: any) {

    this.isImageLoaded = false;
    this.network.getPasseDetails(value).then(res => {
      console.log(res);

      this.titem = res;
      // this.getPassVehicles();
    }, err => { });

  }

  get item(): any {
    return this.titem;
  }

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

  closeModal() {
    this.modals.dismiss();
  }

  callMe(num) {
    this.utility.dialMyPhone(num);
  }

  imageLoaded(){
    console.log("it happends");
    this.isImageLoaded = true;
  }

  async getDirection(address) {
    const data = this.modals.present(GmapComponent, { myAddress: address, newAddress: true, isDirections: true });
  }

  async addToContact(item) {

    console.log(item);
    const flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Add to Contacts?', 'I Would  Like to Add <strong>' + item.sender_name + '</strong> to ZUUL Contacts');

    if (flag) {
      this.network.addContactWithOnlyId({
        created_by_id:item.sender_id,
        contact_id:item.contact_id
      }).then(res => {
        item.contact_id = res.id;
        // this.utility.presentSuccessToast(res['message'])
        // if (res['bool'] == true) {
        //   this.item.contact_id = 0;
        // }
      }, err => { });
    }

  }

  addareminder(item) {

    const detailObj = {
      event_name: item.event_name,
      description: item.description,
      notes: item.phone ? "Call at: " + item.phone : "",
      startDate: Date.parse(item.to_date + ' ' + item.to_date_time),
      endDate: Date.parse(item.for_date + ' ' + item.for_date_time)
    }
    this.utility.addaremondertodate(detailObj);
  }

  async setPassVehicles() {

    // open vehicle modal with CRUD
    const res = await this.modals.present(VehiclesPage, { from_new_pass: true });
    const data = res.data;


    console.log(data);
    if (data['data'] != 'A') {
      this.vehicle = data['data'];

      this.item.color = this.vehicle.color;
      this.item.license_plate = this.vehicle.license_plate;
      this.item.make = this.vehicle.make;
      this.item.model = this.vehicle.model;
      this.item.year = this.vehicle.year;

      let obj = {
        pass_id: this.item['pass_id'],
        qrcode_id: this.item['qrcode_id'],
        vehicle_id: this.vehicle['id'],
        pass_user_id: this.item['id']
      }

      if (this.item['visitor_type'] == 2) {
        obj['vendor_id'] = this.item['vendor_id'];
      }


      // console.log(obj);
      this.network.setDefaultVehicle(obj).then(res => {
        // console.log(res);
        this.is_vehicle = true;
      })

    }

  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivePageRoutingModule } from './active-routing.module';

import { ActivePage } from './active.page';
import { EmptyviewComponentModule } from 'src/app/components/emptyview/emptyview.component.module';
import { PassItemComponentModule } from 'src/app/components/pass-item/pass-item.component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActivePageRoutingModule,
    EmptyviewComponentModule,
    PassItemComponentModule
  ],
  declarations: [ActivePage]
})
export class ActivePageModule {}

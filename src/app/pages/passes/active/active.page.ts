import { Component, Injector, OnInit } from '@angular/core';
import { GmapComponent } from 'src/app/components/gmap/gmap.component';
import { PassServiceService } from 'src/app/services/pass-service.service';
import { BasePage } from '../../base-page/base-page';
import { DetailsPage } from '../details/details.page';

@Component({
  selector: 'app-active',
  templateUrl: './active.page.html',
  styleUrls: ['./active.page.scss'],
})
export class ActivePage extends BasePage implements OnInit {

  plist = [];
  storedData;
  offset = 1;
  canBeResident = false;
  isHeadOfFamily = 0;
  user: any;
  title = 'My';
  alphalist: any[];
  datelist: any[];
  dcount = 160 * 3;
  ccount = 160 * 3;
  communitylist: any[];
  seeingOthersPass = false;
  user_id;
  loading = false;
  filter;

  constructor(
    injector: Injector,
    public passService: PassServiceService
  ) {
    super(injector);
    this.initialize();
    this.events.subscribe("passes:searched", this.setFilteredItems.bind(this));
    this.events.subscribe("passes:searchClear", this.doRefresh());
    this.events.subscribe("passes:filterUpdated", (filter) => {
      this.filter = filter;
    });

    this.alphalist = this.utility.alphalist;
  }

  ionViewWillEnter() {
    // this.initialize();
  }

  ngOnInit() { }

  async initialize() {
    await this.doRefresh();
    this.getPassDates();
  }

  async doRefresh(refresher = null) {

    return new Promise(async resolve => {

      this.user_id = this.nav.getQueryParams().user_id;
      this.user = await this.sqlite.getActiveUser();
      if (!this.user_id) {
        this.user_id = await this.sqlite.getActiveUserId();
      } else {
        this.passService.house_member_id = this.user_id;
      }

      const display_name = this.nav.getQueryParams().display_name;

      if (display_name) {
        this.title = display_name + '\'s';
        this.passService.house_member_name = display_name;
      }

      if (this.user_id != this.user.id) {
        // // console.log(user_id);
        this.canBeResident = false;
        this.seeingOthersPass = true;

      } else {

        this.user_id = this.user.id;
        this.canBeResident = this.user.can_user_become_resident;
        this.isHeadOfFamily = this.user.head_of_family;

      }

      this.offset = 1;
      this.plist = [];
      this.storedData = {};

      await this.getPasses(this.storedData);
      resolve(true);

      if (refresher) {
        refresher.complete();
      }
    });

  }

  getPasses(data: any, loader = false) {

    data.expired = 0;
    data.page = this.offset;
    return new Promise(resolve => {

      console.log(this.user_id);
      if (this.loading == true) {
        resolve(true);
        return;
      }

      this.loading = true;
      this.network.getActivePassesData(this.user_id, data, loader).then((res: any) => {

        console.log('of', this.offset);
        if (this.offset == 1) {
          this.plist = res.list;
        } else {
          this.plist = [...this.plist, ...res.list];
        }

        if (res.list.length > 0) {
          this.offset = this.offset + 1;
        }

        this.loading = false;
        resolve(true);

      }, err => { });

    });

  }

  getPassDates() {
    this.network.getActivePassesDates(this.user_id, { expired: 0 }).then((res: any) => {
      console.log(res.list);
      this.datelist = res.list;
      this.dcount = 65 * this.datelist.length;
    });
  }

  filterByAlpha(a, b) {

    this.alphalist.forEach((item, index) => {
      if (item == a) {
        a.enable = !a.enable;
      } else {
        item.enable = false;
      }
    });

    this.offset = 1;
    if (a.enable == false) {
      this.storedData = {};
    } else {
      const param = b == 'sname' ? 'sender_last_name' : b == 'scommunity' ? 'community' : 'event_name';
      this.storedData = { filter_type: param, filter_search: a.alpha.toLowerCase() };
    }

    this.getPasses(this.storedData);

  }

  filterByDate(a, b) {
    this.datelist.forEach((item, index) => {
      if (item == a) {
        a.enable = !a.enable;
      } else {
        item.enable = false;
      }
    });

    this.offset = 1;
    if (a.enable == false) {
      this.storedData = {};
    } else {
      this.storedData = { filter_type: 'date', filter_search: a.pass_start_date };
    }

    this.getPasses(this.storedData);

  }

  closePassTabs() {
    this.events.publish('pass:allTabClose', Date.now());
  }

  async getDirection(address) {

    console.log(address);
    const addr = address.trim();
    const data = await this.modals.present(GmapComponent, { myAddress: addr, newAddress: true, isDirections: true });

  }

  passDetails(item) {
    console.log(item);
    const data = this.modals.present(DetailsPage, { item: item.pass_id, flag: 'active' });
  }

  setFilteredItems(searchTerm) {
    this.offset = 1;
    this.storedData = { search: searchTerm.toLowerCase() };
    this.getPasses(this.storedData);
  }

  loadMore($event) {

    this.getPasses(this.storedData, false).then(v => {
      $event.target.complete();
    });


  }



  goBack() {
    this.events.publish('passes:close');
  }





}
function DetailPage(DetailPage: any, arg1: { item: any; flag: string; }) {
  throw new Error('Function not implemented.');
}


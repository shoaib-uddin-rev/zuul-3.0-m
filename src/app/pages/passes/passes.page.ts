import { Component, Injector, OnInit } from '@angular/core';
import { GmapComponent } from 'src/app/components/gmap/gmap.component';
import { NewPassComponent } from 'src/app/components/new-pass/new-pass.component';
import { PassServiceService } from 'src/app/services/pass-service.service';
import { BasePage } from '../base-page/base-page';
import { DetailsPage } from './details/details.page';
import { PassGuestLogComponent } from './pass-guest-log/pass-guest-log.component';
import { RecipientComponent } from './recipient/recipient.component';

@Component({
  selector: 'app-passes',
  templateUrl: './passes.page.html',
  styleUrls: ['./passes.page.scss'],
})
export class PassesPage extends BasePage implements OnInit {
  plist = [];
  storedData;
  offset = 1;
  canBeResident = false;
  searchTerm = '';
  filter = '';
  title = 'My';
  loading = false;
  alphalist: any[];
  datelist: any[];
  user;
  user_id;
  dcount = 160 * 3;
  ccount = 160 * 3;
  seeingOthersPass = false;
  isHeadOfFamily = 0;
  timer: any;
  c_selection = 'c_archieve';

  // new flag system introduced
  type = 'active';
  activeDates = [];
  archieveArchieveDates = [];
  archieveSentDates = [];
  archieveScannedDates = [];
  sentDates = [];

  constructor(injector: Injector, public passService: PassServiceService) {
    super(injector);
    this.alphalist = this.utility.alphalist;
    this.preinitialize();

    // this.events.subscribe('passes:close', this.close.bind(this));
  }

  async preinitialize() {
    this.user_id = this.nav.getQueryParams().user_id;
    this.user = await this.sqlite.getActiveUser();
    if (!this.user_id) {
      this.user_id = await this.sqlite.getActiveUserId();
    } else {
      this.passService.house_member_id = this.user_id;
    }

    const display_name = this.nav.getQueryParams().display_name;

    if (display_name) {
      this.title = display_name + "'s";
      this.passService.house_member_name = display_name;
    }

    if (this.user_id != this.user.id) {
      // // console.log(user_id);
      this.canBeResident = false;
      this.seeingOthersPass = true;
    } else {
      this.user_id = this.user.id;
      this.canBeResident = this.user.can_user_become_resident;
      this.isHeadOfFamily = this.user.head_of_family;
    }

    this.initialize();
  }

  ngOnInit() {
    const display_name = this.nav.getQueryParams().display_name;

    if (display_name) {
      this.title = display_name + "'s";
    }
  }

  close() {
    this.nav.pop();
    // this.modals.dismiss();
  }



  searchItems(ev) {

    var self = this;
    clearTimeout(this.timer);
    this.timer = setTimeout( () => {
      console.log(ev.target.value);
      this.searchTerm = ev.target.value ? ev.target.value : '';
      this.offset = 1;
      this.getPasses(this.storedData);
    }, 600);
    // this.events.publish('passes:searched', this.searchTerm);
  }

  async doRefresh(refresher = null) {
    return new Promise(async (resolve) => {
      this.searchTerm = '';
      this.offset = 1;
      this.plist = [];
      this.storedData = {};

      await this.getPasses(this.storedData);
      resolve(true);

      if (refresher) {
        refresher.target.complete();
      }
    });
  }

  onFilterChange() {
    console.log(this.filter);
    // this.events.publish('passes:filterUpdated', this.filter);
  }

  filterByAlpha(a, b) {
    this.alphalist.forEach((item, index) => {
      if (item == a) {
        a.enable = !a.enable;
      } else {
        item.enable = false;
      }
    });

    this.offset = 1;
    if (a.enable == false) {
      this.storedData = {};
    } else {
      const param =
        b == 'sname'
          ? 'sender_last_name'
          : b == 'scommunity'
          ? 'community'
          : 'event_name';
      this.storedData = {
        filter_type: param,
        filter_search: a.alpha.toLowerCase(),
      };
    }

    this.getPasses(this.storedData);
  }

  filterByDate(a, b) {
    this.datelist.forEach((item, index) => {
      if (item == a) {
        a.enable = !a.enable;
      } else {
        item.enable = false;
      }
    });

    this.offset = 1;
    if (a.enable == false) {
      this.storedData = {};
    } else {
      this.storedData = {
        filter_type: 'date',
        filter_search: a.pass_start_date,
      };
    }

    this.getPasses(this.storedData);
  }

  async segmentChanged(choice) {
    this.c_selection = choice;
    this.initialize();
  }

  getPasses(data: any, loader = false) {
    data.expired = (this.type == 'active' || this.type == 'sent') ? 0 : 1;
    data.page = this.offset;
    data.search = this.searchTerm;
    return new Promise(async (resolve) => {
      console.log(this.user_id);
      if (this.loading == true) {
        resolve(true);
        return;
      }

      this.loading = true;
      let res: any;

      if (this.type == 'active') {
        res = await this.network.getActivePassesData(
          this.user_id,
          data,
          loader
        );
      }

      if (this.type == 'archieve') {

        switch (this.c_selection) {
          case 'c_archieve':
            res = await this.network.getUserReceivedPasses(
              this.user_id,
              data,
              loader
            );
            break;
          case 'c_sent':
            res = await this.network.getUserSentPasses(
              this.user_id,
              data,
              loader
            );
            break;
          case 'c_scanned':
            res = await this.network.getUserScannedPasses(
              this.user_id,
              data,
              loader
            );
            break;
        }
      }

      if (this.type == 'sent') {
        res = await this.network.getUserSentPasses(this.user_id, data, loader);
      }

      console.log('of', this.offset);
      if (this.offset == 1) {
        this.plist = res.list;
      } else {
        this.plist = [...this.plist, ...res.list];
      }

      if (res.list.length > 0) {
        this.offset = this.offset + 1;
      }

      this.loading = false;
      resolve(true);
    });
  }

  async getPassDates() {
    let res: any;

    if (this.type == 'active' ) {
      res = this.activeDates.length > 0 ? { list: this.activeDates } :
        await this.network.getActivePassesDates(this.user_id, {
          expired: 0,
        });

        this.activeDates = res.list;
    }

    if (this.type == 'archieve') {
      const data = {
        expired : 1
      };

      console.log(this.user_id);
      switch (this.c_selection) {
        case 'c_archieve':

          res = this.archieveArchieveDates.length > 0 ? { list : this.archieveArchieveDates } :
            await this.network.getUserReceivedPassesDates(this.user_id, data);
          this.archieveArchieveDates = res.list;

          break;
        case 'c_sent':
          res = this.archieveSentDates.length > 0 ? { list : this.archieveSentDates } :
            await this.network.getUserSentPassesDates(this.user_id, data);
          this.archieveSentDates = res.list;

          break;
        case 'scanned':
          res = this.archieveScannedDates.length > 0 ? { list : this.archieveScannedDates } :
            await this.network.getUserScannedPassesDates(this.user_id, data);
          this.archieveScannedDates = res.list;

          break;
      }
    }

    if (this.type == 'sent') {
      res = this.sentDates.length > 0 ? { list: this.sentDates } : await this.network.getUserSentPassesDates(this.user_id, { expired: 0 });
      this.sentDates = res.list;
    }

    console.log(res.list);
    this.datelist = res.list;
    this.dcount = 65 * this.datelist.length;
  }

  updateTypeInitialize(type) {
    this.type = type;
    this.initialize();
  }

  async initialize() {


    this.filter = '';
    this.alphalist = this.utility.alphalist;
    this.datelist = [];
    this.storedData = {};


    await this.doRefresh();
    this.getPassDates();
  }

  loadMore($event) {
    this.getPasses(this.storedData, false).then((v) => {
      $event.target.complete();
    });
  }

  passDetails(item) {
    console.log(item);
    const data = this.modals.present(DetailsPage, {
      item: item.pass_id,
      flag: 'active',
    });
  }

  async getDirection(address) {
    console.log(address);
    const addr = address.trim();
    const data = await this.modals.present(GmapComponent, {
      myAddress: addr,
      newAddress: true,
      isDirections: true,
    });
  }

  async deletePass(item) {

    const flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Are You Sure?',
    'This will remove your pass and all associated sent passes along with it ...');

    if (flag) {

      const data = { pass_id: item['id'] }
      this.network.retractSentPass(this.user.id, data).then((res: any) => {
        this.spliceItem(item);
      }, err => { });

    }

  }

  spliceItem(item) {
    const index: number = this.plist.indexOf(item);
    if (index !== -1) {
      this.plist.splice(index, 1);
    }
  }

  async editPass(item) {

    console.log(item);
    const data = await this.modals.present(NewPassComponent, { item: item, edit: true });

  }

  async editDirections(item) {

    const res = await this.modals.present(GmapComponent, { myAddress: item.description });

    const data = res.data;
    if (data['data'] != 'A') {
      item['lat'] = data.lat;
      item['lng'] = data.lng;
      item['description'] = data.address;
      item['id'] = item['pass_id'];

      this.network.updatePassDirections(item).then(res => { }, err => { })
    }

  }

  async showGuestLogs(item) {

    let res = { pass_id: item.id }
    const data = await this.modals.present(PassGuestLogComponent, res);

  }

  async showRecepients(item) {
    console.log(item);

    let _data = { item: item, isHeadOfFamily: this.isHeadOfFamily, allowParentalControl: false };
    const data = await this.modals.present(RecipientComponent , _data);
    let d = data.data;
    if (d['data'] != 'A') {
      item = d;
    }

  }


}

import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-pass-guest-log',
  templateUrl: './pass-guest-log.component.html',
  styleUrls: ['./pass-guest-log.component.scss'],
})
export class PassGuestLogComponent extends BasePage implements OnInit {

  items: any = [];
  searchTerm: string = "";
  pass_id: any;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.network.getScannedPassLogs(this.pass_id).then(v => {
      console.log(v);
      this.items= v["list"];
      // if (v['bool'] == true) {
      //   this.items = v['scanlogs']
      // }
    })
  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

}

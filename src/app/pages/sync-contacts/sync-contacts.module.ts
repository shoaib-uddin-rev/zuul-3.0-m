import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SyncContactsPageRoutingModule } from './sync-contacts-routing.module';

import { SyncContactsPage } from './sync-contacts.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SyncContactsPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [SyncContactsPage]
})
export class SyncContactsPageModule {}

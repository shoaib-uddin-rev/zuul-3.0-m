import { Component, Injector, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage extends BasePage implements OnInit {
  tapCount = 0;
  skipintro = false;
  stimeout;
  isSync = false;
  aftertimeout = true;

  constructor(injector: Injector) {
    super(injector);
    // this.users.assignEvents();
    this.platform.ready().then(() => {
      // this.waitTillInitialize();
    });
    // this.menu.enable(false);
    this.init();
  }

  ngOnInit() {}

  async waitTillInitialize() {
    this.sqlite.initialize().then((v) => {
      this.callRedirect();
    });
  }

  tap() {
    this.tapCount = this.tapCount + 1;
    setTimeout(function() {
      this.tapCount = 0;
    }, 500);
    if (this.tapCount == 2) {
      if (!this.skipintro) {
        this.skipintro = true;
        this.tapCount = 0;
        this.utility.presentToast('Skipping Intro ...');
        clearTimeout(this.stimeout);
        // this.callRedirect();
      }
    }
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad SplashPage');
  }

  ionViewDidEnter() {

  }

  async init() {
    // get all data from local storage

    await this.sqlite.initialize();
    const self = this;
    this.stimeout = setTimeout(() => {
      // self.callRedirect();
      self.aftertimeout = false;
      self.users.getUser();
    }, 1200);
  }

  callRedirect() {
    // temporary waiting
    // let token = await this.sqlite.getCurrentUserAuthorizationToken();
    // this.users.getUser();
  }

  ionViewDidLeave() {
    // this.menu.enable(true);
  }
}

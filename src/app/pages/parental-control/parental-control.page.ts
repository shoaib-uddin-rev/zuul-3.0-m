import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-parental-control',
  templateUrl: './parental-control.page.html',
  styleUrls: ['./parental-control.page.scss'],
})
export class ParentalControlPage extends BasePage implements OnInit {

  item;
  user_id;

  aForm: FormGroup;
  akeys: any = [];

  constructor(injector: Injector) {
    super(injector);

    this.user_id = this.nav.getQueryParams().user_id;
    // console.log(this.item);
    this.setupForm();
    this.fetchParentalValues()

  }

  ngOnInit() { }

  setupForm() {

    this.aForm = this.formBuilder.group({
      notify_when_member_guest_arrive: [1, Validators.required],
      notify_when_member_send_pass_to_others: [1, Validators.required],
      notify_when_member_receive_a_pass: [1, Validators.required],
      notify_when_member_retract_a_pass: [1, Validators.required],
      notify_when_member_update_a_pass: [1, Validators.required],
      notify_when_pass_retracted_included_member: [1, Validators.required],
      notify_when_pass_updated_included_member: [1, Validators.required],
      notify_when_member_request_a_pass_with_offensive_words: [1, Validators.required],
      blacklist_member_to_receive_request_a_pass: [1, Validators.required],
      allow_parental_controls_to_member: [1, Validators.required],
    })

    const keys = Object.keys(this.aForm.controls);
    this.akeys = keys.map(x => {
      return {
        'key': x,
        'label': x.replace(/_/g, ' ')
      }
    })

  }

  fetchParentalValues() {

    this.network.getParentalControlOptions(this.user_id).then(data => {

      let options = data[0];
      console.log(options);

      for (let key in options) {
        let str = options[key];
        let val = parseInt(str);
        key = key.toLowerCase();
        if (this.aForm.controls[key]) {
          this.aForm.controls[key].setValue(val);
        }
      }

      console.log(this.akeys);


    }, err => { })
  }

  saveSelection() {
    console.log(this.aForm.value);
    this.network.setParentalControlOptions(this.user_id, this.aForm.value).then(data => {
      this.utility.presentSuccessToast("Settings Saved");
      this.nav.pop();
    }, err => { })
  }


}

import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BasePage } from '../base-page/base-page';
import { ForgetPasswordPage } from '../forget-password/forget-password.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;

  constructor(injector: Injector, public activatedRoute: ActivatedRoute,) {
    super(injector);
    this.setupForm();

    // get params if any
    let params = this.activatedRoute.snapshot.params;
    console.log(params);

    let query = this.activatedRoute.snapshot.queryParams;
    console.log(query);



  }

  ionViewWillEnter(){
    // get params if any
    let params = this.activatedRoute.snapshot.params;
    console.log(params);

    let query = this.activatedRoute.snapshot.queryParams;
    console.log(query);

    if(query.phone_number){
      this.aForm.controls.phone_number.setValue(query.phone_number);
    }

  }

  ngOnInit() { }

  setupForm() {

    // (353) 453-4534
    // 12345678

    this.aForm = this.formBuilder.group({
      phone_number: ['', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */],
      password: ['', Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.required])]
    });

  }



  async openLogUsers(event) {

    // const udata = { id: null, flag: 'SW' };
    // const data = await this.popover.present(event, udata);




    // if (data == null) {
    //   return;
    // }
    // const sw_user = data["param"];
    // this.setUserInForm(sw_user)

  }

  // setUserInForm(sw_user) {
  //   if (sw_user) {
  //     this.users.switchUserAccount(sw_user).then(data => {
  //       if (!data) {
  //         // this.navwithin = true;
  //         this.aForm.controls['phone_number'].setValue(sw_user['phone_number'])
  //       }
  //     })

  //   }
  // }

  onTelephoneChange(ev) {
    if (ev.inputType !== 'deleteContentBackward') {
      const utel = this.utility.onkeyupFormatPhoneNumberRuntime(ev.target.value, false);
      console.log(utel);
      ev.target.value = utel;
      this.aForm.controls['phone_number'].patchValue(utel);
      // ev.target.value = utel;
    }
  }

  forgetpassword() {
    // this.nav.push('forget-password');
    this.modals.present(ForgetPasswordPage);
  }

  login() {
    this.submitAttempt = true;
    const formdata = this.aForm.value;

    formdata.register_with_phonenumber = true;
    this.users.login(formdata);

  }

  signup() {
    this.nav.push('pages/signup');
  }

}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotificationsPageRoutingModule } from './notifications-routing.module';

import { NotificationsPage } from './notifications.page';
import { EmptyviewComponentModule } from 'src/app/components/emptyview/emptyview.component.module';
import { ComponentsNotificationItemComponentModule } from
'src/app/components/components-notification-item/components-notification-item.component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotificationsPageRoutingModule,
    ComponentsNotificationItemComponentModule,
    EmptyviewComponentModule
  ],
  declarations: [NotificationsPage]
})
export class NotificationsPageModule {}

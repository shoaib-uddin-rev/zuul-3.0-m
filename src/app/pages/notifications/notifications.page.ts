import { Component, Injector, OnInit } from '@angular/core';
import { NewPassComponent } from 'src/app/components/new-pass/new-pass.component';
import { BasePage } from '../base-page/base-page';
import { DetailsPage } from '../passes/details/details.page';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage extends BasePage implements OnInit {
  plist: any[] = [];
  avatar = this.users.avatar;
  user_events: any[] = [];
  offset = 1;
  public loading = true;
  public doneMarkAllRead = false;

  constructor(injector: Injector) {
    super(injector);
    this.fetchNotifications();
  }

  ngOnInit() { }

  removeNotf(item) {
    // console.log(item);
    this.spliceItem(item);
    this.network.removeNotification(item.id).then(
      (response: any) => {
        // console.log(response);
      },
      (err) => { }
    );
  }

  rejectNotf(item) {
    this.spliceItem(item);
    const dataObj = { id: item.id, pass_request_id: item.pass_request_id };
    this.network.rejectPassRequest(dataObj).then((response: any) => {
      this.removeNotf(item);
    });
  }

  acceptNotf(item, ease) {
    // console.log(item, ease);
    this.spliceItem(item);

    const it = { it_id: item, ev_id: ease };
    this.network.acceptPassRequest(it).then(async (response: any) => {
      this.removeNotf(item);
    });
  }

  async createPass(item) {


    const contact_id = await this.createUserBySenderId(item.pass_request_sent_user_id);
    // this.modals.dismiss(item).then(() => {
      // this.removeNotf(item);

      if (item.anonymous == 1) {
        // console.log(item);
        const profile = this.sqlite.getActiveUser();
        const f = {
          selected_contacts: [contact_id],
          event_id: 15,
          event_name: 'Instant Pass',
          pass_date: this.utility.formatDateTime(new Date()),
          pass_validity: '5',
          pass_type: "one",
          visitor_type: 'friends_family',
          pr_id: item.pr_id,
          description: this.utility.parseAddressFromProfile(profile),
        };

        this.network.createNewPass(f).then(
          (res: any) => {
            this.utility.presentSuccessToast('Instant pass sent successfully');
          },
          (error) => { }
        );
      } else {
        // console.log(item);
        // this.events.publish('user:redirectToCreatePass', item);
        const data = await this.modals.present(
          NewPassComponent,
          {
            rap: false,
            group: true,
            item: {
              from_contact: true,
              selected_contacts: [contact_id],
              pass_start_date_with_time: this.utility.formatDateTime( new Date() )
            }
          }
        );

      }
    // });
  }

  createUserBySenderId(senderId){

    return new Promise( resolve => {

      this.network.addContactWithOnlyUserId({
        user_id:senderId,
      }).then(res => {
        resolve(res.id);
        // this.utility.presentSuccessToast(res['message'])
        // if (res['bool'] == true) {
        //   this.item.contact_id = 0;
        // }
      }, err => { });

    });

  }

  sendAnonymousPassToUser() { }

  async showPass(item) {
    // console.log(item);
    const data = await this.modals.present(DetailsPage, {
      item: item.pass_id,
      flag: 'active',
    });
  }

  spliceItem(item) {
    const index: number = this.plist.indexOf(item);
    if (index !== -1) {
      this.plist.splice(index, 1);
    }
  }

  fetchNotifications(showloader = true) {
    return new Promise((resolve) => {
      this.loading = showloader;
      const params = { page: this.offset };

      this.network.getOnePageNotifications(params).then(
        (res: any) => {
          if (this.offset == 1) {
            this.plist = res.list;
          } else {
            this.plist = [...this.plist, ...res.list];
          }

          this.offset = this.offset + 1;
          this.loading = false;
          resolve(true);
        },
        (err) => { }
      );
    });
  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

  convertSecondstoTime(giventime) {
    const t1 = new Date(giventime);
    const t2 = new Date();
    const dif = t1.getTime() - t2.getTime();

    const Seconds_from_T1_to_T2 = dif / 1000;
    return Math.round(Seconds_from_T1_to_T2);
  }

  markasread(item) {
    this.network.markReadNotification({ notification_ids: [item.id] }).then(
      (response: any) => {
        // console.log(response);
      },
      (err) => { }
    );
  }

  markAllAsRead() {
    const ids = [];
    this.plist.forEach((element) => {
      element.status = 1;
      ids.push(element.id);
    });

    this.doneMarkAllRead = true;
    this.network.markReadNotification({ notification_ids: ids, all: true }).then(
      (response: any) => {
        // console.log(response);
      },
      (err) => { }
    );
  }

  async doRefresh(refresher = null) {
    this.offset = 1;
    this.plist = [];
    this.fetchNotifications();
    refresher.target.complete();
  }

  loadMore($event) {
    this.fetchNotifications(false).then((v) => {
      $event.complete();
    });
  }
}

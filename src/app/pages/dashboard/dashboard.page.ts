import { AfterViewInit, ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';
import { NewPassComponent } from 'src/app/components/new-pass/new-pass.component';
import { BasePage } from '../base-page/base-page';
import { RequestPassPage } from '../request-pass/request-pass.page';
import { RegisterPage } from '../register/register.page';
import { MenuController } from '@ionic/angular';
import { VehiclesPage } from '../vehicles/vehicles.page';
import { HouseholdsPage } from '../households/households.page';
import { SettingsPage } from '../settings/settings.page';
import { ParentalLogsPage } from '../parental-logs/parental-logs.page';
import { NotificationsPage } from '../notifications/notifications.page';
import { PassesPage } from '../passes/passes.page';
import { CodeVerficationComponent } from 'src/app/components/code-verfication/code-verfication.component';
import { DetailsPage } from '../passes/details/details.page';
import { ContactsPage } from '../contacts/contacts.page';
import { SelectMultipleControlValueAccessor } from '@angular/forms';
import { AnnouncmentsComponent } from 'src/app/components/announcments/announcments.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage extends BasePage implements OnInit, AfterViewInit {

  avatar = this.users.avatar;
  user: any;
  plist: any[] = [];
  page = 1;
  getNotified = false;
  rnotif = false;
  isEmailVerificationPending: boolean = false;
  isListLoading = false;
  isNewNotification: boolean = false;

  constructor(injector: Injector, private chkChange: ChangeDetectorRef) {
    super(injector);
    this.events.subscribe('dashboard:refreshpage', this.dashboardRefreshpage.bind(this));
    this.events.subscribe('dashboard:notificationReceived', () => {
      this.isNewNotification = true;
    })
  }

  async dashboardRefreshpage($event) {
    // this.doRefresh($event);
    console.log('ping here', this.plist.length);
    // this.plist = [];
    this.isListLoading = true;
    this.page = 1;
    this.chkChange.detectChanges();

    setTimeout(async () => {
      await this.getPasses();
      this.isListLoading = false;
      this.chkChange.detectChanges();
    }, 2000);

    console.log('ping here', this.plist.length);

  }

  ngAfterViewInit(): void {

  }

  async openPopover($event) {
    const res = await this.popover.present(ContactpopoverPageComponent, $event, { flag: 'MENU' });

    const data = res.data;
    if (data) {

      switch (data.param) {
        case 'MH':
          this.goToHouseholds();
          break;
        case 'S':
          this.goToSettings();
          break;
        case 'P':
          this.user = await this.sqlite.getActiveUser();
          this.modals.present(RegisterPage, { new: false, user: this.user });
          break;
        case 'M':
          // vehicle
          this.goToVechicle();
          break;
        case 'L':
          this.users.logout();
          break;
      }

    }



  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.initializeView();
  }

  async initializeView() {

    // this.menuCtrl.enable(true, 'main');

    this.isListLoading = true;
    this.page = 1;
    this.user = await this.users.getActiveUser();
    this.users.setTokenToServer();
    console.log(this.user);

    const v = await this.permissions.validateProfile(this.user);
    if (!v) {
      await this.modals.present(RegisterPage, { new: true, user: this.user });
    }

    //

    this.getPasses().then(async () => {

      this.isListLoading = false;

      // if (this.user.is_verified) {
      //   this.isEmailVerificationPending = this.user.is_verified == "0" ? true : false;
      //   console.log(this.isEmailVerificationPending);
      // }
      // else
      if (this.user["is_reset_password"] == 1) {
        // this.modals.present(UpdatePasswordPageComponent, { user: this.user })
      }
      // else if (!this.validateProfile(this.user)) {
      //   this.nav.push('1/RegistrationPage', { new: true, user: this.user })
      // }
      else {
        this.callAnnouncements();
      }

    });



  }

  getPasses(): Promise<any> {

    return new Promise((resolve) => {

      const data = {
        expired: 0,
        page: this.page
      };

      this.network.getActivePassesData(this.user.id, data, this.page == 0).then((res: any) => {
        console.log(res);
        if (this.page == 1) {
          this.plist = res.list;
        } else {
          this.plist = [...this.plist, ...res.list];
        }

        if (res.list.length > 0) {
          this.page = this.page + 1;
        }

        resolve(true);

      });

    });

  }

  doRefresh(refresher = null) {

    return new Promise(async resolve => {
      this.page = 1;
      this.plist = [];

      // this.isListLoading = true;
      await this.getPasses();
      // this.isListLoading = false;
      // await this.callAnnouncements();
      if (refresher) {
        refresher.target.complete();
      }

      resolve(true);
    });


  }

  async mycontactbutton() {
    // MyContactPage
    this.nav.push('pages/contacts');
    // this.modals.present(ContactsPage);
  }

  async pass() {
    this.nav.push('pages/passes');
    // const res = await this.modals.present(PassesPage);
  }

  async newPass() {
    const res = await this.modals.present(NewPassComponent, { item: null });
  }

  async openDoubleBell($event) {

    this.getNotified = false;
    this.rnotif = false;

    const res = await this.popover.present(ContactpopoverPageComponent, $event, { flag: 'PCN' });

    const data = res.data;

    // let tr = data;
    // console.log(data);
    if (data.data == 'A') {
      return;
    }

    switch (data.param) {
      case 'P':
        await this.modals.present(ParentalLogsPage);
        break;
      case 'N':
        await this.modals.present(NotificationsPage);
        break;
    }
  }

  async requestapass() {
    const data = await this.modals.present(RequestPassPage);
  }

  async notificationReceived(data) {
    console.log(data);
    this.getNotified = true;
    this.utility.presentToast(data.title);

    await this.getPasses();
  }

  presentPopoverParental() {
    this.getNotified = false;
    // this.users.sendNotification().subscribe((data: any) => {
    //   // console.log(data);
    // })
    this.rnotif = false;
    // let myModal = this.modals.present(ParentalLogsPage);
  }

  async goToVechicle() {
    const res = await this.modals.present(VehiclesPage);
  }

  async goToHouseholds() {
    // const res = await this.modals.present(HouseholdsPage);
    this.nav.push('pages/households');
  }

  async goToSettings() {
    const res = await this.modals.present(SettingsPage);
  }

  // becomeAResident() {
  //   this.nav.push('CodeVerificationPage');
  // }

  presentDuplicateEmailVerification() {
    this.modals.present(CodeVerficationComponent, { emv: true });
  }

  async passDetails(sel) {
    // console.log(sel);
    const data = await this.modals.present(DetailsPage, { item: sel.pass_id, flag: "active" });
  }

  async openLogUsers($event) {
    // var r = item;
    var self = this;
    const res = await this.popover.present(ContactpopoverPageComponent, $event, {
      id: null,
      flag: 'SW'
    });

    const data = res.data;

    if (data == null) {
      return;
    }
    console.log(data);
    // var item = data.pid;

    const swUser = data['param'];
    console.log(swUser);
    const user = await this.users.switchUserAccount(swUser)
    if (user) {

      await this.initializeView();
    } else {
      this.users.logout(swUser);
    }

  }

  callAnnouncements() {

    return new Promise<void>(resolve => {
      this.network.getUnreadAnnouncements().then(async data => {
        const d: any[] = data['list'];
        if (d.length > 0) {
          const data = await this.modals.present(AnnouncmentsComponent, { plist: d });
        }

        resolve();

      }, err => { });
    })

  }

  async loadMore($event) {
    await this.getPasses();
    $event.target.complete();
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';

import { DashboardPage } from './dashboard.page';
import { EmptyviewComponentModule } from 'src/app/components/emptyview/emptyview.component.module';
import { AnnouncmentsComponent } from 'src/app/components/announcments/announcments.component';
import { AnnouncmentsItemComponent } from 'src/app/components/announcments-item/announcments-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule,
    EmptyviewComponentModule
  ],
  declarations: [
    DashboardPage,
    AnnouncmentsComponent,
    AnnouncmentsItemComponent
  ]
})
export class DashboardPageModule {}

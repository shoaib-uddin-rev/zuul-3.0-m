import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';
import { VehicleServiceService } from 'src/app/services/vehicle.service';
import { BasePage } from '../base-page/base-page';
import { CreateVehicleComponent } from './create-vehicle/create-vehicle.component';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.page.html',
  styleUrls: ['./vehicles.page.scss'],
})
export class VehiclesPage extends BasePage implements OnInit {

  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  search_value;
  @Input() search_on = false;
  @Input() from_new_pass = false;
  selected_item_id = -1;
  loading = false;
  plist: any[] = [];
  _plist: any[] = [];

  constructor(injector: Injector, public vehicleService: VehicleServiceService) {
    super(injector);

  }

  ionViewWillEnter(){
    this.getVehicleList();
  }

  ngOnInit() { }

  async getVehicleList() {
    this.loading = true;
    this.plist = await this.vehicleService.getVehiclesList();
    this._plist = [...this.plist];
    this.loading = false;
    // console.log(this.plist);
  }

  async doRefresh($event) {
    this.plist = await this.vehicleService.getVehiclesList();
    $event.target.complete();
  }

  plusHeaderButton() {
    this.editVehicle(null);
  }

  async editVehicle(item) {

    const res = await this.modals.present(CreateVehicleComponent, { item });
    const data = res.data;
    this.getVehicleList();

    // if (data['data'] != 'A') {
    //   var vehicle = data;
    //   vehicle.new = true;
    //   vehicle.image = vehicle.ImageUrl;
    //   const _index = this._plist.findIndex(x => x.id == vehicle.id);
    //   if (_index != -1) {
    //     this._plist[_index] = vehicle;
    //   } else {
    //     this._plist.unshift(vehicle)
    //   }

    //   this.plist = [...this._plist];

    // }

  }

  async deleteVehicle(item) {
    await this.vehicleService.deleteVehicle(item.id);
    this.getVehicleList();
  }

  async setVehicleToPass(item) {
    this.selected_item_id = item.id;
    // await this.vehicleService.setDefaultVehicle(item.id);

    if (this.from_new_pass == true) {
      // check if need to dismiss modally
      this.closeModal({ data: item });
    }


  }

  async presentPopover($event, item) {

    $event.stopPropagation();
    const res = await this.popover.present(ContactpopoverPageComponent, $event,
    {
      pid: item,
      flag: 'VE'
    });

    const data = res.data;
    console.log(data);

    if (data == null) {
      return;
    }
    switch (data.param) {
      case 'S':
        this.setVehicleToPass(item);
        break;
      case 'E':
        this.editVehicle(item);
        break;
      case 'D':
        this.deleteVehicle(item);
        break;
    }
  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

}

import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-contact-options',
  templateUrl: './contact-options.component.html',
  styleUrls: ['./contact-options.component.scss'],
})
export class ContactOptionsComponent implements OnInit {

  @Input() fromContacts = true;
  @Input() contacts = [];

  @Output() selectedContact: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {}

  getSelected(item){
    console.log(item);
    this.selectedContact.emit(item);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequestPassPageRoutingModule } from './request-pass-routing.module';

import { RequestPassPage } from './request-pass.page';
import { ChoiceOptionsComponent } from './choice-options/choice-options.component';
import { RequestSuccessComponent } from './request-success/request-success.component';
import { CommentOptionsComponent } from './comment-options/comment-options.component';
import { ContactOptionsComponent } from './contact-options/contact-options.component';
import { WizardComponent } from './wizard/wizard.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RequestPassPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [
    RequestPassPage,
    ChoiceOptionsComponent,
    RequestSuccessComponent,
    CommentOptionsComponent,
    ContactOptionsComponent,
    WizardComponent
  ]
})
export class RequestPassPageModule {}

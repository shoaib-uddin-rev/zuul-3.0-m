import { Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../base-page/base-page';
import { AddContactsPage } from '../contacts/contacts/add/add.page';

@Component({
  selector: 'app-request-pass',
  templateUrl: './request-pass.page.html',
  styleUrls: ['./request-pass.page.scss'],
})
export class RequestPassPage extends BasePage implements OnInit, OnDestroy {

  @ViewChild('slides', { static: true }) slides: IonSlides;
  selected_contacts: any = [];
  comments = '';
  heading1 = 'Pass request has been sent. You will be notified once a pass has been issued.';
  Start = 'Finish';
  fromContacts = false;
  contacts = [];
  step = 0;

  constructor(injector: Injector, public storedcontacts: StoredContactsService) {
    super(injector);

    this.events.subscribe('requestpass:selectfromexicting', this.selectFromExicting.bind(this));
    this.events.subscribe('requestpass:goToNext', this.goToNext.bind(this));
    this.events.subscribe('requestpass:setComments', this.setComments.bind(this));
    this.events.subscribe('requestpass:close', this.goBack.bind(this));

  }
  ngOnDestroy(): void {
    this.events.unsubscribe('requestpass:selectfromexicting');
    this.events.unsubscribe('requestpass:goToNext');
    this.events.unsubscribe('requestpass:setComments');
    this.events.unsubscribe('requestpass:close');
  }

  ngOnInit() { }

  goBack() {
    this.modals.dismiss();
  }

  setComments(comments): Promise<any> {

    return new Promise<void>(resolve => {
      this.comments = comments;
      resolve();
    });

  }



  async selectFromExicting(bool) {
    // this.slides.lockSwipes(false);

    console.log(bool);

    if (bool) {
      // select a contact from list and get back
      // this.slides.slideTo(1, 500);

      // ask API to load users that are in my contact list that can send passes

      const contacts = await this.getResidentsListToRequestPass();
      console.log(contacts);
      if (contacts.length <= 0) {
        this.utility.presentSuccessToast('No Residential Contacts Found');
        // this.utility.hideLoader();
      } else {

        // console.log("peep");
        // let imported_list = await this.storedcontacts.returnContactSelection(true, true, true, contacts);
        // let contacts = await this.storedcontacts.getContactsByArrayOfIds(imported_list);

        // console.log(imported_list, contacts);

        if (contacts.length) {
          this.contacts = [...contacts];
          this.goToPre(1);
        } else {
          this.utility.presentSuccessToast('No Residential Contacts Found');
        }

      }




    } else {
      this.editContact();
    }

    this.slides.lockSwipes(true);
  }

  async selectedContact($event) {
    this.selected_contacts = [];
    this.selected_contacts.push($event);
    this.step = 2;
    this.moveSlide(this.step);
  }

  moveSlide(num) {
    this.slides.lockSwipes(false);
    this.slides.slideTo(num, 500);
    this.slides.lockSwipes(true);
  }

  async goStep($event) {
    const num = await this.slides.getActiveIndex();
    console.log($event);
    if ($event < num) {
      this.step = $event;
      this.moveSlide($event);
    }
  }

  getResidentsListToRequestPass(): Promise<any[]> {
    return new Promise(resolve => {

      this.network.getResidentsListToRequestPass().then(v => {
        console.log(v);
        // let ids = [];
        // v["list"].forEach(element => {
        //   ids.push(element["user_id"]);
        // });
        resolve(v.list);

      }, err => {
        console.error(err);
        resolve([]);
      });

    });
  }


  async editContact() {

    // CreateGroupPage as modal

    const res = await this.modals.present(AddContactsPage, { user_id: false, show_relation: false, isFromRequestPassScreen: true });
    const data = res.data;

    console.log('req-data', data);

    if (data.data != 'A') {
      this.selected_contacts = [data.id];
      // this.contacts.push(data);
      // this.selected_contacts.push(data);
      this.goToPre(2);
    }

  }

  goToPre(num) {
    console.log(num);
    this.step = num;
    this.slides.lockSwipes(false);
    this.slides.slideTo(num, 500);
    this.slides.lockSwipes(true);
  }

  async goToNext(num) {

    if (!this.comments || this.comments == '') {
      return;
    }

    // const user = await this.sqlite.getActiveUser();
    // const id = user['id'];
    // console.log(id);

    const self = this;
    const conts = [];

    this.selected_contacts.forEach(element => {

      //var prett = this.phone_contacts.filter( x => x['its_user'] != null);
      console.log(element);
      const t = element;

      const f = {
        requested_user_id: t.user_id,
        description: this.comments
      };
      // f['sent_user_id'] = id;
      // f['display_name'] = t.display_name;
      // f['display_email'] = t.email;
      // f['description'] = this.comments;
      // f['phone_number'] = t.phone_number;


      conts.push(f);

    });

    // console.log(conts)

    if (!conts[0]) {
      return;
    }

    this.network.sendRequestForAPass(conts[0]).then((response: any) => {

      // console.log(response);
      this.goToPre(3);

    }, err => {
      this.utility.presentFailureToast('Person not allowed to send passes');
      this.finishregister(null);
    });


  }

  finishregister(data) {
    this.modals.dismiss();
  }


}

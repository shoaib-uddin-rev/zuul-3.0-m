import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-comment-options',
  templateUrl: './comment-options.component.html',
  styleUrls: ['./comment-options.component.scss'],
})
export class CommentOptionsComponent extends BasePage implements OnInit {

  comments: any;
  requestSent = false;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  goToNext(num){

    if(this.requestSent == true){
      return;
    }

    this.requestSent = true;

    this.events.publish('requestpass:setComments', this.comments);
    setTimeout( () => {
      this.events.publish('requestpass:goToNext', num);
    }, 500);
  }

}

import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.page.html',
  styleUrls: ['./forget-password.page.scss'],
})
export class ForgetPasswordPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;

  constructor(injector: Injector) {
    super(injector);
    this.setupForm();
  }

  ngOnInit() {}

  setupForm(){

    this.aForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.pattern(/\S+@\S+\.\S+/), Validators.required]) /*, VemailValidator.checkEmail */ ]
    });

  }

  save() {

    const inEmail = !this.aForm.controls.email.valid;
    if(inEmail){
      this.utility.presentFailureToast('Email address is invalid, please enter a valid email address');
      return;
    }
    this.submitAttempt = true;
    const formdata = this.aForm.value;

    this.network.forgetPassword(formdata).then( res => {

      // if(res.bool === true){
        this.utility.presentSuccessToast(res);
        this.modals.dismiss();
      // }else{
      //   this.utility.presentFailureToast(res.message);
      // }

    }, err => { });



  }

  login() {
    this.modals.dismiss();
  }

  redirectToContactUsFOrm(){
    this.utility.openContactFormUrl();
  }

}

import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-parental-logs',
  templateUrl: './parental-logs.page.html',
  styleUrls: ['./parental-logs.page.scss'],
})
export class ParentalLogsPage extends BasePage implements OnInit {
  plist: any[] = [];
  offset = 1;
  public loading = true;
  public doneMarkAllRead = false;

  constructor(injector: Injector) {
    super(injector);
    this.fetchNotifications();
  }

  ngOnInit() {}

  fetchNotifications(showloader = true) {
    return new Promise<void>((resolve) => {
      this.loading = showloader;
      const params = { page: this.offset };

      this.network.getParentalNotifications(params).then(
        (res: any) => {
          if (this.offset == 1) {
            this.plist = res.list;
          } else {
            this.plist = [...this.plist, ...res.list];
          }

          this.offset = this.offset + 1;
          this.loading = false;
          resolve();
        },
        (err) => {}
      );
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ParentalLogsPage');
  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

  markasread(item) {
    this.network.markReadPCNotification({ notification_ids: [item.id] }).then(
      (response: any) => {
        // console.log(response);
      },
      (err) => {}
    );
  }

  markAllAsRead() {
    const ids = [];
    this.plist.forEach((element) => {
      element.status = 1;
      ids.push(element.id);
    });

    this.doneMarkAllRead = true;
    this.network.markReadPCNotification({ notification_ids: ids, all: true }).then(
      (response: any) => {
        // console.log(response);
      },
      (err) => {}
    );
  }

  async doRefresh(refresher = null) {
    this.offset = 0;
    this.plist = [];
    this.fetchNotifications();
    refresher.complete();
  }

  loadMore($event) {
    this.fetchNotifications(false).then((v) => {
      $event.target.complete();
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentalLogsPageRoutingModule } from './parental-logs-routing.module';

import { ParentalLogsPage } from './parental-logs.page';
import { EmptyviewComponentModule } from 'src/app/components/emptyview/emptyview.component.module';
import { ParentalNotificationItemComponentModule } from 'src/app/components/parental-notification-item/parental-notification-item.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentalLogsPageRoutingModule,
    EmptyviewComponentModule,
    ParentalNotificationItemComponentModule
  ],
  declarations: [ParentalLogsPage]
})
export class ParentalLogsPageModule {}
